<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<!DOCTYPE html>
<html>
 <head>
  <!-- <title>工艺信息</title> -->
  <script type="text/javascript">
  //初始化下标
	function resetTrNum(tableId) {
		$tbody = $("#"+tableId+"");
		$tbody.find('>tr').each(function(i){
			$(':input, select', this).each(function(){
				var $this = $(this), name = $this.attr('name'), val = $this.val();
				if(name!=null){
					if (name.indexOf("#index#") >= 0){
						$this.attr("name",name.replace('#index#',i));
					}else{
						var s = name.indexOf("[");
						var e = name.indexOf("]");
						var new_name = name.substring(s+1,e);
						$this.attr("name",name.replace(new_name,i));
					}
				}
			});
			$(this).find('div[name=\'xh\']').html(i+1);
		});
	}
 </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" tiptype="1" action="craftController.do?save">
			<input id="id" name="id" type="hidden" value="${craftPage.id }">
			<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
			<td align="right"><label class="Validform_label">工艺编号:</label></td>
			<td class="value">
				<input nullmsg="请填写craftno" errormsg="craftno格式不对" class="inputxt" id="craftno" name="craftno" 
									   value="${craftPage.craftno}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">机种名称:</label></td>
			<td class="value">
				<input nullmsg="请填写craftna" errormsg="craftna格式不对" class="inputxt" id="craftname" name="craftname" 
									   value="${craftPage.craftname}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">零件号:</label></td>
			<td class="value">
				<input nullmsg="请填写materia" errormsg="materia格式不对" class="inputxt" id="materialId" name="materialId" 
									   value="${craftPage.materialId}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">数据状态:</label></td>
			<td class="value">
				<input nullmsg="请填写datasta" errormsg="datasta格式不对" class="inputxt" id="datastatus" name="datastatus" 
									   value="${craftPage.datastatus}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">备注:</label></td>
			<td class="value">
				<input nullmsg="请填写memo" errormsg="memo格式不对" class="inputxt" id="memo" name="memo" 
									   value="${craftPage.memo}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			</table>
			<div style="width: auto;height: 200px;">
				<%-- 增加一个div，用于调节页面大小，否则默认太小 --%>
				<div style="width:690px;height:1px;"></div>
				<t:tabs id="tt" iframe="false" tabPosition="top" fit="false">
				 <t:tab href="craftController.do?processList&id=${craftPage.id}" icon="icon-search" title="工序信息" id="process"></t:tab>
				</t:tabs>
			</div>
			</t:formvalid>
			<!-- 添加 明细 模版 -->
		<table style="display:none">
		<tbody id="add_process_table_template">
			<tr>
			 <td align="center"><div style="width: 25px;" name="xh"></div></td>
			 <td align="center"><input style="width:20px;" type="checkbox" name="ck"/></td>
				  <td align="left"><input name="processList[#index#].craftprocessno" maxlength="50" type="text" style="width:120px;"></td>
				  <td align="left"><input name="processList[#index#].craftprocessname" maxlength="50" type="text" style="width:120px;"></td>
				  <td align="left"><input name="processList[#index#].equipmenttypeid" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="processList[#index#].setuptime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="processList[#index#].processtime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="processList[#index#].memo" maxlength="200" type="text" style="width:120px;"></td>
			</tr>
		 </tbody>
		</table>
 </body>