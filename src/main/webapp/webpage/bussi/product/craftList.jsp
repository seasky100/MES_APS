<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="craftList" fitColumns="true" actionUrl="craftController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="true"></t:dgCol>
   <t:dgCol title="工艺编号" field="craftno" ></t:dgCol>
   <t:dgCol title="机种名称" field="craftname" ></t:dgCol>
   <t:dgCol title="crafttarget" field="crafttarget" hidden="true"></t:dgCol>
   <t:dgCol title="nextcraftid" field="nextcraftid" hidden="true"></t:dgCol>
   <t:dgCol title="precraftid" field="precraftid" hidden="true"></t:dgCol>
   <t:dgCol title="零件号" field="materialId" ></t:dgCol>
   <t:dgCol title="cost" field="cost" hidden="true"></t:dgCol>
   <t:dgCol title="数据状态" field="datastatus" ></t:dgCol>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="craftController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="craftController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="craftController.do?addorupdate" funname="update"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>