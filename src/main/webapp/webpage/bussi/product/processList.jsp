<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<script type="text/javascript">
	$('#addProcessBtn').linkbutton({   
	    iconCls: 'icon-add'  
	});  
	$('#delProcessBtn').linkbutton({   
	    iconCls: 'icon-remove'  
	}); 
	$('#addProcessBtn').bind('click', function(){   
 		 var tr =  $("#add_process_table_template tr").clone();
	 	 $("#add_process_table").append(tr);
	 	 resetTrNum('add_process_table');
    });  
	$('#delProcessBtn').bind('click', function(){   
      	$("#add_process_table").find("input:checked").parent().parent().remove();   
        resetTrNum('add_process_table'); 
    }); 
    $(document).ready(function(){
    	$(".datagrid-toolbar").parent().css("width","auto");
    });
</script>
<div style="padding: 3px; height: 25px;width:auto;" class="datagrid-toolbar">
	<a id="addProcessBtn" href="#">添加</a> <a id="delProcessBtn" href="#">删除</a> 
</div>
<div style="width: auto;height: 300px;overflow-y:auto;overflow-x:scroll;">
<table border="0" cellpadding="2" cellspacing="0" id="process_table">
	<tr bgcolor="#E6E6E6">
		<td align="center" bgcolor="#EEEEEE">序号</td>
		<td align="center" bgcolor="#EEEEEE">操作</td>
				  <td align="left" bgcolor="#EEEEEE">工序号</td>
				  <td align="left" bgcolor="#EEEEEE">工序名称</td>
				  <td align="left" bgcolor="#EEEEEE">加工区</td>
				  <td align="left" bgcolor="#EEEEEE">换线时间（分钟）</td>
				  <td align="left" bgcolor="#EEEEEE">标准产能（PCS/H）</td>
				  <td align="left" bgcolor="#EEEEEE">备注</td>
	</tr>
	<tbody id="add_process_table">	
	<c:if test="${fn:length(processList)  <= 0 }">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">1</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck"/></td>
				  <td align="left"><input name="processList[0].craftprocessno" maxlength="50" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="processList[0].craftprocessname" maxlength="50" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="processList[0].equipmenttypeid" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="processList[0].setuptime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="processList[0].processtime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="processList[0].memo" maxlength="200" type="text" style="width:120px;" ></td>
   			</tr>
	</c:if>
	<c:if test="${fn:length(processList)  > 0 }">
		<c:forEach items="${processList}" var="poVal" varStatus="stuts">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">${stuts.index+1 }</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck" /></td>
				<input name="processList[${stuts.index }].id"  value="${poVal.id }" type="hidden" >
				   <td align="left"><input name="processList[${stuts.index }].craftprocessno" maxlength="50" value="${poVal.craftprocessno }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="processList[${stuts.index }].craftprocessname" maxlength="50" value="${poVal.craftprocessname }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="processList[${stuts.index }].equipmenttypeid" maxlength="" value="${poVal.equipmenttypeid }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="processList[${stuts.index }].setuptime" maxlength="" value="${poVal.setuptime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="processList[${stuts.index }].processtime" maxlength="" value="${poVal.processtime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="processList[${stuts.index }].memo" maxlength="200" value="${poVal.memo }" type="text" style="width:120px;"></td>
   			</tr>
		</c:forEach>
	</c:if>	
	</tbody>
</table>
</div>