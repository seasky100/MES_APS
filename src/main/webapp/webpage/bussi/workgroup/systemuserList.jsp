<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<script type="text/javascript">
	$('#addsystemusersBtn').linkbutton({   
	    iconCls: 'icon-add'  
	});  
	$('#delsystemusersBtn').linkbutton({   
	    iconCls: 'icon-remove'  
	}); 
	$('#addsystemusersBtn').bind('click', function(){   
 		 var tr =  $("#add_systemusers_table_template tr").clone();
	 	 $("#add_systemusers_table").append(tr);
	 	 resetTrNum('add_systemusers_table');
    });  
	$('#delsystemusersBtn').bind('click', function(){   
      	$("#add_systemusers_table").find("input:checked").parent().parent().remove();   
        resetTrNum('add_systemusers_table'); 
    }); 
    $(document).ready(function(){
    	$(".datagrid-toolbar").parent().css("width","auto");
    });
</script>
<div style="padding: 3px; height: 25px;width:auto;" class="datagrid-toolbar">
	<a id="addsystemusersBtn" href="#">添加</a> <a id="delsystemusersBtn" href="#">删除</a> 
</div>
<div style="width: auto;height: 300px;overflow-y:auto;overflow-x:scroll;">
<table border="0" cellpadding="2" cellspacing="0" id="systemusers_table">
	<tr bgcolor="#E6E6E6">
		<td align="center" bgcolor="#EEEEEE">序号</td>
		<td align="center" bgcolor="#EEEEEE">操作</td>
				  <td align="left" bgcolor="#EEEEEE">姓名</td>
				  <td align="left" bgcolor="#EEEEEE">性别</td>
				  <td align="left" bgcolor="#EEEEEE">固定电话</td>
				  <td align="left" bgcolor="#EEEEEE">手机</td>
				  <td align="left" bgcolor="#EEEEEE">地址</td>
				  <td align="left" bgcolor="#EEEEEE">备注</td>
	</tr>
	<tbody id="add_systemusers_table">	
	<c:if test="${fn:length(sysUserList)  <= 0 }">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">1</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck"/></td>
				  <td align="left"><input name="sysUserList[0].username" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="sysUserList[0].gendar" maxlength="12" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="sysUserList[0].phone" maxlength="10" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="sysUserList[0].mobile" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="sysUserList[0].address" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="sysUserList[0].memo" maxlength="33" type="text" style="width:120px;" ></td>
   			</tr>
	</c:if>
	<c:if test="${fn:length(sysUserList)  > 0 }">
		<c:forEach items="${sysUserList}" var="poVal" varStatus="stuts">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">${stuts.index+1 }</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck" /></td>
				<input name="sysUserList[${stuts.index }].id"  value="${poVal.id }" type="hidden" >
				   <td align="left"><input name="sysUserList[${stuts.index }].username" maxlength="" value="${poVal.username }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="sysUserList[${stuts.index }].gendar" maxlength="12" value="${poVal.gendar }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="sysUserList[${stuts.index }].phone" maxlength="10" value="${poVal.phone }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="sysUserList[${stuts.index }].mobile" maxlength="" value="${poVal.mobile }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="sysUserList[${stuts.index }].address" maxlength="" value="${poVal.address }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="sysUserList[${stuts.index }].memo" maxlength="33" value="${poVal.memo }" type="text" style="width:120px;"></td>
   			</tr>
		</c:forEach>
	</c:if>	
	</tbody>
</table>
</div>