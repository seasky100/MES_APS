<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
 <!--  <title>员工信息</title> -->
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="systemuserController.do?save">
			<input id="id" name="id" type="hidden" value="${systemuserPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							loginname:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="loginname" name="loginname" 
							   value="${systemuserPage.loginname}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							username:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="username" name="username" 
							   value="${systemuserPage.username}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							password:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="password" name="password" 
							   value="${systemuserPage.password}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							datastatus:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="datastatus" name="datastatus" 
							   value="${systemuserPage.datastatus}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							enabled:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="enabled" name="enabled" 
							   value="${systemuserPage.enabled}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							numlogin:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="numlogin" name="numlogin" 
							   value="${systemuserPage.numlogin}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							gendar:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="gendar" name="gendar" 
							   value="${systemuserPage.gendar}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							email:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="email" name="email" 
							   value="${systemuserPage.email}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							phone:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="phone" name="phone" 
							   value="${systemuserPage.phone}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							mobile:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="mobile" name="mobile" 
							   value="${systemuserPage.mobile}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							affiliation:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="affiliation" name="affiliation" 
							   value="${systemuserPage.affiliation}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							address:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="address" name="address" 
							   value="${systemuserPage.address}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							memo:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="memo" name="memo" 
							   value="${systemuserPage.memo}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							workgoupid:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="workgoupid" name="workgoupid" 
							   value="${systemuserPage.workgoupid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
		</t:formvalid>
 </body>