<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<!DOCTYPE html>
<html>
 <head>
  <!-- <title>班组信息</title> -->
  <script type="text/javascript">
  //初始化下标
	function resetTrNum(tableId) {
		$tbody = $("#"+tableId+"");
		$tbody.find('>tr').each(function(i){
			$(':input, select', this).each(function(){
				var $this = $(this), name = $this.attr('name'), val = $this.val();
				if(name!=null){
					if (name.indexOf("#index#") >= 0){
						$this.attr("name",name.replace('#index#',i));
					}else{
						var s = name.indexOf("[");
						var e = name.indexOf("]");
						var new_name = name.substring(s+1,e);
						$this.attr("name",name.replace(new_name,i));
					}
				}
			});
			$(this).find('div[name=\'xh\']').html(i+1);
		});
	}
 </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="workgroupController.do?save">
			<input id="id" name="id" type="hidden" value="${workgroupPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							编号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="workgroupno" name="workgroupno" 
							   value="${workgroupPage.workgroupno}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							带线班长:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="workgroupname" name="workgroupname" 
							   value="${workgroupPage.workgroupname}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							车间经理:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="manager" name="manager" 
							   value="${workgroupPage.manager}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							备注:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="memo" name="memo" 
							   value="${workgroupPage.memo}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
			<div style="width: auto;height: 200px;">
				<%-- 增加一个div，用于调节页面大小，否则默认太小 --%>
				<div style="width:690px;height:1px;"></div>
				<t:tabs id="tt" iframe="false" tabPosition="top" fit="false">
				 <t:tab href="workgroupController.do?sysUserList&id=${workgroupPage.id}" icon="icon-search" title="用户明细" id="systemusers"></t:tab>
				</t:tabs>
			</div>
		</t:formvalid>
		<table style="display:none">
		<tbody id="add_systemusers_table_template">
			<tr>
			 <td align="center"><div style="width: 25px;" name="xh"></div></td>
			 <td align="center"><input style="width:20px;" type="checkbox" name="ck"/></td>
				  <td align="left"><input name="sysUserList[#index#].username" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="sysUserList[#index#].gendar" maxlength="12" type="text" style="width:120px;"></td>
				  <td align="left"><input name="sysUserList[#index#].phone" maxlength="10" type="text" style="width:120px;"></td>
				  <td align="left"><input name="sysUserList[#index#].mobile" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="sysUserList[#index#].address" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="sysUserList[#index#].memo" maxlength="33" type="text" style="width:120px;"></td>
			</tr>
		 </tbody>
		</table>
 </body>