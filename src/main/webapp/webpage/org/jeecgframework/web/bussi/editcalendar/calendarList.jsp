<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="calendarList" actionUrl="calendarController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="日历编号" field="calendarno"  hidden="false"></t:dgCol>
   <t:dgCol title="日历名称" field="calendarname" ></t:dgCol>
   <t:dgCol title="起始日期" field="startdate" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="结束日期" field="enddate" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="数据状态" field="datastatus"  hidden="false"></t:dgCol>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="calendarController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="calendarController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="calendarController.do?addorupdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="calendarController.do?addorupdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>