<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="jobprocessList"  actionUrl="jobprocessController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="任务编号" field="jobprocessno" ></t:dgCol>
   <%-- <t:dgCol title="jobprocessname" field="jobprocessname" ></t:dgCol>
   <t:dgCol title="craftid" field="craftid" ></t:dgCol>
   <t:dgCol title="craftprocessid" field="craftprocessid" ></t:dgCol> --%>
   <t:dgCol title="数量" field="quantity" ></t:dgCol>
   <t:dgCol title="工序号" field="ordernum" ></t:dgCol>
   <t:dgCol title="计划开始时间" field="plannedstarttime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="计划结束时间" field="plannedendtime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="车间" field="plannedequipmentsetid" ></t:dgCol>
   <t:dgCol title="产线" field="plannedequipmentid" ></t:dgCol>
   <%-- <t:dgCol title="actualstarttime" field="actualstarttime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="actualendtime" field="actualendtime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="actualequipmentsetid" field="actualequipmentsetid" ></t:dgCol>
   <t:dgCol title="actualequipmentid" field="actualequipmentid" ></t:dgCol> --%>
   <t:dgCol title="销售订单编号" field="orderid" ></t:dgCol>
   <%-- <t:dgCol title="productionorderid" field="productionorderid" ></t:dgCol>
   <t:dgCol title="lotid" field="lotid" ></t:dgCol>
   <t:dgCol title="scheduleid" field="scheduleid" ></t:dgCol> --%>
   <t:dgCol title="机种名称" field="materialid" ></t:dgCol>
   <%-- <t:dgCol title="setuptime" field="setuptime" ></t:dgCol>
   <t:dgCol title="turnovertime" field="turnovertime" ></t:dgCol> --%>
   <t:dgCol title="是否冻结" field="isfrozen" ></t:dgCol>
   <%-- <t:dgCol title="isoutsourcing" field="isoutsourcing" ></t:dgCol> --%>
   <t:dgCol title="状态" field="state" ></t:dgCol>
   <%-- <t:dgCol title="datastatus" field="datastatus" ></t:dgCol> --%>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="jobprocessController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="jobprocessController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="jobprocessController.do?addorupdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="jobprocessController.do?addorupdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>