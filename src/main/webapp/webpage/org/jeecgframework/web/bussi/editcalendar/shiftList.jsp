<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<script type="text/javascript">
	$('#addShiftBtn').linkbutton({   
	    iconCls: 'icon-add'  
	});  
	$('#delShiftBtn').linkbutton({   
	    iconCls: 'icon-remove'  
	}); 
	$('#addShiftBtn').bind('click', function(){   
 		 var tr =  $("#add_shift_table_template tr").clone();
	 	 $("#add_shift_table").append(tr);
	 	 resetTrNum('add_shift_table');
    });  
	$('#delShiftBtn').bind('click', function(){   
      	$("#add_shift_table").find("input:checked").parent().parent().remove();   
        resetTrNum('add_shift_table'); 
    }); 
    $(document).ready(function(){
    	$(".datagrid-toolbar").parent().css("width","auto");
    });
</script>
<div style="padding: 3px; height: 25px;width:auto;" class="datagrid-toolbar">
	<a id="addShiftBtn" href="#">添加</a> <a id="delShiftBtn" href="#">删除</a> 
</div>
<div style="width: auto;height: 300px;overflow-y:auto;overflow-x:scroll;">
<table border="0" cellpadding="2" cellspacing="0" id="shift_table">
	<tr bgcolor="#E6E6E6">
		<td align="center" bgcolor="#EEEEEE">序号</td>
		<td align="center" bgcolor="#EEEEEE">操作</td>
				  <td align="left" bgcolor="#EEEEEE">名称</td>
				  <td align="left" bgcolor="#EEEEEE">上班时间</td>
				  <td align="left" bgcolor="#EEEEEE">下班时间</td>
				  <td align="left" bgcolor="#EEEEEE">备注</td>
	</tr>
	<tbody id="add_shift_table">	
	<c:if test="${fn:length(shiftList)  <= 0 }">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">1</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck"/></td>
				  <td align="left"><input name="shiftList[0].shiftname" maxlength="50" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="shiftList[0].starttime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="shiftList[0].endtime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="shiftList[0].memo" maxlength="200" type="text" style="width:120px;" ></td>
   			</tr>
	</c:if>
	<c:if test="${fn:length(shiftList)  > 0 }">
		<c:forEach items="${shiftList}" var="poVal" varStatus="stuts">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">${stuts.index+1 }</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck" /></td>
				<input name="shiftList[${stuts.index }].id"  value="${poVal.id }" type="hidden" >
				   <td align="left"><input name="shiftList[${stuts.index }].shiftname" maxlength="50" value="${poVal.shiftname }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="shiftList[${stuts.index }].starttime" maxlength="" value="${poVal.starttime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="shiftList[${stuts.index }].endtime" maxlength="" value="${poVal.endtime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="shiftList[${stuts.index }].memo" maxlength="200" value="${poVal.memo }" type="text" style="width:120px;"></td>
   			</tr>
		</c:forEach>
	</c:if>	
	</tbody>
</table>
</div>