<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="dispatchList" fitColumns="true"  actionUrl="dispatchController.do?datagrid" idField="id" fit="true"
  queryMode="group" checkbox="true" autoLoadData="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="日生产计划编号" field="dispatchno" ></t:dgCol>
   <t:dgCol title="日生产计划名称" field="dispatchname" ></t:dgCol>
   <t:dgCol title="生产管理员" field="dispatcher" ></t:dgCol>
   <t:dgCol title="生成时间" field="savetime" formatter="yyyy-MM-dd hh:mm:ss" query="true" queryMode="group"></t:dgCol>
   <t:dgCol title="alarmdates" field="alarmdates" hidden="false"></t:dgCol>
   <t:dgCol title="确认人" field="confirmer" ></t:dgCol>
   <t:dgCol title="numconfirm" field="numconfirm" hidden="false"></t:dgCol>
   <t:dgCol title="确认时间" field="confirmtime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="confirminfo" field="confirminfo" hidden="false"></t:dgCol>
   <t:dgCol title="审批人" field="checker" ></t:dgCol>
   <t:dgCol title="numcheck" field="numcheck" hidden="false"></t:dgCol>
   <t:dgCol title="审批时间" field="checktime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="checkinfo" field="checkinfo" hidden="false"></t:dgCol>
   <t:dgCol title="状态" field="state" ></t:dgCol>
   <t:dgCol title="datastatus" field="datastatus" hidden="false"></t:dgCol>
   <t:dgCol title="memo" field="memo" hidden="false"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="dispatchController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="dispatchController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="dispatchController.do?addorupdate" funname="update"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
  <script type="text/javascript">
 $(document).ready(function(){
		$("input[name='savetime_begin']").datebox({required:false});
		$("input[name='savetime_end']").datebox({required:false});
	});
 </script>