<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <!-- <title>月计划表查询</title> -->
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="planController.do?save">
			<input id="id" name="id" type="hidden" value="${planPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							计划编号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="planno" name="planno" 
							   value="${planPage.planno}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							计划名称:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="planname" name="planname" 
							   value="${planPage.planname}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							计划人:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="planner" name="planner" 
							   value="${planPage.planner}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							保存时间:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="savetime" name="savetime" 
							     value="<fmt:formatDate value='${planPage.savetime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							提示日期:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="alarmdates" name="alarmdates" 
							   value="${planPage.alarmdates}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							审核人:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="checker" name="checker" 
							   value="${planPage.checker}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							检查号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="numcheck" name="numcheck" 
							   value="${planPage.numcheck}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							检查信息:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="checkinfo" name="checkinfo" 
							   value="${planPage.checkinfo}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							状态:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="state" name="state" 
							   value="${planPage.state}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							算法:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="algorithminfo" name="algorithminfo" 
							   value="${planPage.algorithminfo}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							数据状态:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="datastatus" name="datastatus" 
							   value="${planPage.datastatus}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							备注:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="memo" name="memo" 
							   value="${planPage.memo}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
		</t:formvalid>
 </body>