<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="dynamiceventsList" title="动态事件" actionUrl="dynamiceventsController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="事件编号" field="dynamiceventsno" ></t:dgCol>
   <t:dgCol title="对象" field="dynamiceventsname" ></t:dgCol>
   <t:dgCol title="事件类型" field="dynamiceventstype" ></t:dgCol>
   <t:dgCol title="物品" field="objectid" ></t:dgCol>
   <t:dgCol title="starttime" field="starttime" formatter="yyyy-MM-dd hh:mm:ss" hidden="false"></t:dgCol>
   <t:dgCol title="要求交货时间" field="endtime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="jobprocessid" field="jobprocessid" hidden="false"></t:dgCol>
   <t:dgCol title="数量" field="backlogquantity" hidden="false"></t:dgCol>
   <t:dgCol title="状态" field="state" ></t:dgCol>
   <t:dgCol title="数据状态" field="datastatus" ></t:dgCol>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgDelOpt title="删除" url="dynamiceventsController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="dynamiceventsController.do?addorupdatejobdelay" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="dynamiceventsController.do?addorupdatejobdelay" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="dynamiceventsController.do?addorupdatejobdelay" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>