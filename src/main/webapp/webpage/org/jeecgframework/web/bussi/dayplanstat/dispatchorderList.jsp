<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<script type="text/javascript">
	$('#addDispatchorderBtn').linkbutton({   
	    iconCls: 'icon-add'  
	});  
	$('#delDispatchorderBtn').linkbutton({   
	    iconCls: 'icon-remove'  
	}); 
	$('#addDispatchorderBtn').bind('click', function(){   
 		 var tr =  $("#add_dispatchorder_table_template tr").clone();
	 	 $("#add_dispatchorder_table").append(tr);
	 	 resetTrNum('add_dispatchorder_table');
    });  
	$('#delDispatchorderBtn').bind('click', function(){   
      	$("#add_dispatchorder_table").find("input:checked").parent().parent().remove();   
        resetTrNum('add_dispatchorder_table'); 
    }); 
    $(document).ready(function(){
    	$(".datagrid-toolbar").parent().css("width","auto");
    });
</script>
<div style="padding: 3px; height: 25px;width:auto;" class="datagrid-toolbar">
	<a id="addDispatchorderBtn" href="#">添加</a> <a id="delDispatchorderBtn" href="#">删除</a> 
</div>
<div style="width: auto;height: 300px;overflow-y:auto;overflow-x:scroll;">
<table border="0" cellpadding="2" cellspacing="0" id="dispatchorder_table">
	<tr bgcolor="#E6E6E6">
		<td align="center" bgcolor="#EEEEEE">序号</td>
		<td align="center" bgcolor="#EEEEEE">操作</td>
				  <td align="left" bgcolor="#EEEEEE">工序号</td>
				  <td align="left" bgcolor="#EEEEEE">生产指令单编号 </td>
				  <td align="left" bgcolor="#EEEEEE">车间</td>
				  <td align="left" bgcolor="#EEEEEE">产线</td>
				  <td align="left" bgcolor="#EEEEEE">带线班长</td>
				  <td align="left" bgcolor="#EEEEEE">机种名称</td>
				  <td align="left" bgcolor="#EEEEEE">零件号</td>
				  <td align="left" bgcolor="#EEEEEE">客户编号</td>
				  <td align="left" bgcolor="#EEEEEE">订单编号</td>
				  <td align="left" bgcolor="#EEEEEE">要求加工数量</td>
				  <td align="left" bgcolor="#EEEEEE">制程名称</td>
				  <td align="left" bgcolor="#EEEEEE">计划开始时间</td>
				  <td align="left" bgcolor="#EEEEEE">计划结束时间</td>
				  <td align="left" bgcolor="#EEEEEE">实际开始时间</td>
				  <td align="left" bgcolor="#EEEEEE">实际结束时间</td>
				  <td align="left" bgcolor="#EEEEEE">完成数量</td>
	</tr>
	<tbody id="add_dispatchorder_table">	
	<c:if test="${fn:length(dispatchorderList)  <= 0 }">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">1</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck"/></td>
				  <td align="left"><input name="dispatchorderList[0].ordernum" maxlength="255" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].dispatchorderno" maxlength="255" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].equipmentsetid" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].equipmentid" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].workgroupmanager" maxlength="255" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].materialId" maxlength="255" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].materialId" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].jobprocessid" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].jobprocessid" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].quantity" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].processId" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].plannedstarttime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].plannedendtime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].actualstarttime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].actualendtime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="dispatchorderList[0].numfinished" maxlength="" type="text" style="width:120px;" ></td>
   			</tr>
	</c:if>
	<c:if test="${fn:length(dispatchorderList)  > 0 }">
		<c:forEach items="${dispatchorderList}" var="poVal" varStatus="stuts">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">${stuts.index+1 }</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck" /></td>
				<input name="dispatchorderList[${stuts.index }].id"  value="${poVal.id }" type="hidden" >
				   <td align="left"><input name="dispatchorderList[${stuts.index }].ordernum" maxlength="255" value="${poVal.ordernum }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].dispatchorderno" maxlength="255" value="${poVal.dispatchorderno }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].equipmentsetid" maxlength="" value="${poVal.equipmentsetid }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].equipmentid" maxlength="" value="${poVal.equipmentid }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].workgroupmanager" maxlength="255" value="${poVal.workgroupmanager }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].materialId" maxlength="255" value="${poVal.materialId }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].materialId" maxlength="" value="${poVal.materialId }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].jobprocessid" maxlength="" value="${poVal.jobprocessid }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].jobprocessid" maxlength="" value="${poVal.jobprocessid }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].quantity" maxlength="" value="${poVal.quantity }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].processId" maxlength="" value="${poVal.processId }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].plannedstarttime" maxlength="" value="${poVal.plannedstarttime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].plannedendtime" maxlength="" value="${poVal.plannedendtime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].actualstarttime" maxlength="" value="${poVal.actualstarttime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].actualendtime" maxlength="" value="${poVal.actualendtime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="dispatchorderList[${stuts.index }].numfinished" maxlength="" value="${poVal.numfinished }" type="text" style="width:120px;"></td>
   			</tr>
		</c:forEach>
	</c:if>	
	</tbody>
</table>
</div>