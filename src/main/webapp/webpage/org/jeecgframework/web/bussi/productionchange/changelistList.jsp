<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<script type="text/javascript">
	$('#addChangelistBtn').linkbutton({   
	    iconCls: 'icon-add'  
	});  
	$('#delChangelistBtn').linkbutton({   
	    iconCls: 'icon-remove'  
	}); 
	$('#addChangelistBtn').bind('click', function(){   
 		 var tr =  $("#add_changelist_table_template tr").clone();
	 	 $("#add_changelist_table").append(tr);
	 	 resetTrNum('add_changelist_table');
    });  
	$('#delChangelistBtn').bind('click', function(){   
      	$("#add_changelist_table").find("input:checked").parent().parent().remove();   
        resetTrNum('add_changelist_table'); 
    }); 
    $(document).ready(function(){
    	$(".datagrid-toolbar").parent().css("width","auto");
    });
</script>
<div style="padding: 3px; height: 25px;width:auto;" class="datagrid-toolbar">
	<a id="addChangelistBtn" href="#">添加</a> <a id="delChangelistBtn" href="#">删除</a> 
</div>
<div style="width: auto;height: 300px;overflow-y:auto;overflow-x:scroll;">
<table border="0" cellpadding="2" cellspacing="0" id="changelist_table">
	<tr bgcolor="#E6E6E6">
		<td align="center" bgcolor="#EEEEEE">序号</td>
		<td align="center" bgcolor="#EEEEEE">操作</td>
				  <td align="left" bgcolor="#EEEEEE">变更编号</td>
				  <td align="left" bgcolor="#EEEEEE">变更单据类型</td>
				  <td align="left" bgcolor="#EEEEEE">变更单据编号</td>
				  <td align="left" bgcolor="#EEEEEE">原物料</td>
				  <td align="left" bgcolor="#EEEEEE">变更物料</td>
				  <td align="left" bgcolor="#EEEEEE">原数量</td>
				  <td align="left" bgcolor="#EEEEEE">变更数量</td>
				  <td align="left" bgcolor="#EEEEEE">原开始时间</td>
				  <td align="left" bgcolor="#EEEEEE">变更开始时间</td>
				  <td align="left" bgcolor="#EEEEEE">原结束时间</td>
				  <td align="left" bgcolor="#EEEEEE">变更结束时间</td>
	</tr>
	<tbody id="add_changelist_table">	
	<c:if test="${fn:length(changelistList)  <= 0 }">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">1</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck"/></td>
				  <td align="left"><input name="changelistList[0].changelistno" maxlength="50" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].changedoctype" maxlength="50" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].changedocno" maxlength="100" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].materialid" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].changedmaterialid" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].quantity" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].changedquantity" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].starttime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].changedstarttime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].endtime" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="changelistList[0].changedendtime" maxlength="" type="text" style="width:120px;" ></td>
   			</tr>
	</c:if>
	<c:if test="${fn:length(changelistList)  > 0 }">
		<c:forEach items="${changelistList}" var="poVal" varStatus="stuts">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">${stuts.index+1 }</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck" /></td>
				<input name="changelistList[${stuts.index }].id"  value="${poVal.id }" type="hidden" >
				   <td align="left"><input name="changelistList[${stuts.index }].changelistno" maxlength="50" value="${poVal.changelistno }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].changedoctype" maxlength="50" value="${poVal.changedoctype }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].changedocno" maxlength="100" value="${poVal.changedocno }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].materialid" maxlength="" value="${poVal.materialid }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].changedmaterialid" maxlength="" value="${poVal.changedmaterialid }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].quantity" maxlength="" value="${poVal.quantity }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].changedquantity" maxlength="" value="${poVal.changedquantity }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].starttime" maxlength="" value="${poVal.starttime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].changedstarttime" maxlength="" value="${poVal.changedstarttime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].endtime" maxlength="" value="${poVal.endtime }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="changelistList[${stuts.index }].changedendtime" maxlength="" value="${poVal.changedendtime }" type="text" style="width:120px;"></td>
   			</tr>
		</c:forEach>
	</c:if>	
	</tbody>
</table>
</div>