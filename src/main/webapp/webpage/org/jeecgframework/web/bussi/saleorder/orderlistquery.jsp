<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>销售订单录入转换</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript">
  //初始化下标
	function resetTrNum(tableId) {
		$tbody = $("#"+tableId+"");
		$tbody.find('>tr').each(function(i){
			$(':input, select', this).each(function(){
				var $this = $(this), name = $this.attr('name'), val = $this.val();
				if(name!=null){
					if (name.indexOf("#index#") >= 0){
						$this.attr("name",name.replace('#index#',i));
					}else{
						var s = name.indexOf("[");
						var e = name.indexOf("]");
						var new_name = name.substring(s+1,e);
						$this.attr("name",name.replace(new_name,i));
					}
				}
			});
			$(this).find('div[name=\'xh\']').html(i+1);
		});
	}
 </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" tiptype="1" action="orderlistController.do?save">
			<input id="id" name="id" type="hidden" value="${orderlistPage.id }">
			<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
			<td align="right"><label class="Validform_label">销售订单编号:</label></td>
			<td class="value">
				<input nullmsg="请填写orderno" errormsg="orderno格式不对" class="inputxt" id="orderno" name="orderno" 
									   value="${orderlistPage.orderno}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">客户名称:</label></td>
			<td class="value">
				<input nullmsg="请填写orderna" errormsg="orderna格式不对" class="inputxt" id="ordername" name="ordername" 
									   value="${orderlistPage.ordername}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">订单价格:</label></td>
			<td class="value">
				<input nullmsg="请填写orderpr" errormsg="orderpr格式不对" class="inputxt" id="orderprice" name="orderprice" 
									   value="${orderlistPage.orderprice}" datatype="d">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">订货时间:</label></td>
			<td class="value">
				<input nullmsg="请填写orderda" errormsg="orderda格式不对" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="orderdate" name="orderdate" 
									     value="<fmt:formatDate value='${orderlistPage.orderdate}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right" hidden="true"><label class="Validform_label">systemu:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写systemu" errormsg="systemu格式不对" class="inputxt" id="systemuserId" name="systemuserId" 
									   value="${orderlistPage.systemuserId}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right" hidden="true"><label class="Validform_label">tardine:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写tardine" errormsg="tardine格式不对" class="inputxt" id="tardinesscost" name="tardinesscost" 
									   value="${orderlistPage.tardinesscost}" datatype="d">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right" hidden="true"><label class="Validform_label">invento:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写invento" errormsg="invento格式不对" class="inputxt" id="inventorycost" name="inventorycost" 
									   value="${orderlistPage.inventorycost}" datatype="d">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right" hidden="true"><label class="Validform_label">materia:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写materia" errormsg="materia格式不对" class="inputxt" id="materialid" name="materialid" 
									   value="${orderlistPage.materialid}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right" hidden="true"><label class="Validform_label">quantit:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写quantit" errormsg="quantit格式不对" class="inputxt" id="quantity" name="quantity" 
									   value="${orderlistPage.quantity}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right" hidden="true"><label class="Validform_label">quantit:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写quantit" errormsg="quantit格式不对" class="inputxt" id="quantityu" name="quantityu" 
									   value="${orderlistPage.quantityu}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right" hidden="true"><label class="Validform_label">quantit:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写quantit" errormsg="quantit格式不对" class="inputxt" id="quantityf" name="quantityf" 
									   value="${orderlistPage.quantityf}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			
			<td align="right"><label class="Validform_label">转生产订单时间:</label></td>
			<td class="value">
				<input nullmsg="请填写release" errormsg="release格式不对" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="releasedate" name="releasedate" 
									     value="<fmt:formatDate value='${orderlistPage.releasedate}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">要求完成时间:</label></td>
			<td class="value">
				<input nullmsg="请填写duedate" errormsg="duedate格式不对" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="duedate" name="duedate" 
									     value="<fmt:formatDate value='${orderlistPage.duedate}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			
			<td align="right"><label class="Validform_label">实际完成时间:</label></td>
			<td class="value">
				<input nullmsg="请填写actuald" errormsg="actuald格式不对" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="actualddate" name="actualddate" 
									     value="<fmt:formatDate value='${orderlistPage.actualddate}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">状态:</label></td>
			<td class="value">
				<input nullmsg="请填写state" errormsg="state格式不对" class="inputxt" id="state" name="state" 
									   value="${orderlistPage.state}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right" hidden="true"><label class="Validform_label">priorit:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写priorit" errormsg="priorit格式不对" class="inputxt" id="priority" name="priority" 
									   value="${orderlistPage.priority}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right" hidden="true"><label class="Validform_label">color:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写color" errormsg="color格式不对" class="inputxt" id="color" name="color" 
									   value="${orderlistPage.color}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right" hidden="true"><label class="Validform_label">isplan:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写isplan" errormsg="isplan格式不对" class="inputxt" id="isplan" name="isplan" 
									   value="${orderlistPage.isplan}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			
			</tr>
			<tr>
			<td align="right" hidden="true"><label class="Validform_label">datasta:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写datasta" errormsg="datasta格式不对" class="inputxt" id="datastatus" name="datastatus" 
									   value="${orderlistPage.datastatus}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right" hidden="true"><label class="Validform_label">memo:</label></td>
			<td class="value" hidden="true">
				<input nullmsg="请填写memo" errormsg="memo格式不对" class="inputxt" id="memo" name="memo" 
									   value="${orderlistPage.memo}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			</table>
			<div style="width: auto;height: 200px;">
				<%-- 增加一个div，用于调节页面大小，否则默认太小 --%>
				<div style="width:690px;height:1px;"></div>
				<t:tabs id="tt" iframe="false" tabPosition="top" fit="false">
				 <t:tab href="orderlistController.do?productionorderList" icon="icon-search" title="销售订单查询" id="productionorder"></t:tab>
				 <%-- <t:tab href="orderlistController.do?productionorder2List" icon="icon-search" title="销售订单查询" id="productionorder2"></t:tab> --%>
				</t:tabs>
			</div>
			</t:formvalid>
			<!-- 添加 明细 模版 -->
		<table style="display:none">
		<tbody id="add_productionorder_table_template">
			<tr>
			 <td align="center"><div style="width: 25px;" name="xh"></div></td>
			 <td align="center"><input style="width:20px;" type="checkbox" name="ck"/></td>
				  <td align="left"><input name="productionorderList[#index#].productionorderno" maxlength="50" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].productionordername" maxlength="50" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].isfrozen" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].materialId" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].orderlistId" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].quantity" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].quantityu" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].quantityf" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].color" maxlength="50" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].earliestduedate" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].latestduedate" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].actualdeliverydate" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].priority" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].state" maxlength="20" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].alarmdates" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].datastatus" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="productionorderList[#index#].memo" maxlength="200" type="text" style="width:120px;"></td>
			</tr>
		 </tbody>
		
		</table>
 </body>