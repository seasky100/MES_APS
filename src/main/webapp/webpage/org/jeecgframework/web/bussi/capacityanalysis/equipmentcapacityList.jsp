<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="equipmentcapacityList" title="产能负荷表" actionUrl="equipmentcapacityController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="resourceid" field="resourceid" hidden="false"></t:dgCol>
   <t:dgCol title="产线编号" field="resourceno" ></t:dgCol>
   <t:dgCol title="产线名称额" field="resourcename" ></t:dgCol>
   <t:dgCol title="resourcetype" field="resourcetype" hidden="false"></t:dgCol>
   <t:dgCol title="committedjobprocessid" field="committedjobprocessid" hidden="false"></t:dgCol>
   <t:dgCol title="isavailable" field="isavailable" hidden="false"></t:dgCol>
   <t:dgCol title="开始时间" field="starttime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="结束时间" field="endtime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="availablequantity" field="availablequantity" hidden="false"></t:dgCol>
   <t:dgCol title="数据状态" field="datastatus" ></t:dgCol>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="equipmentcapacityController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="equipmentcapacityController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="equipmentcapacityController.do?addorupdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="equipmentcapacityController.do?addorupdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>