<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="planlotList"  actionUrl="planlotController.do?datagrid" idField="id" fit="true" 
  checkbox="true" autoLoadData="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="生产订单编号" field="lotno" ></t:dgCol>
   <t:dgCol title="机种名称" field="lotname" ></t:dgCol>
   <t:dgCol title="数量" field="lotquantity" ></t:dgCol>
   <t:dgCol title="是否冻结" field="isfrozen" ></t:dgCol>
   <%-- <t:dgCol title="color" field="color" hidden="true"></t:dgCol> --%>
   <t:dgCol title="状态" field="lotstate" ></t:dgCol>
   <t:dgCol title="零件号" field="materialId" ></t:dgCol>
  <%--  <t:dgCol title="craftid" field="craftid" hidden="true"></t:dgCol> --%>
   <t:dgCol title="完成数量" field="numfinished" ></t:dgCol>
   <t:dgCol title="计划开始时间" field="plannedstarttime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="计划完成时间" field="plannedendtime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <%-- <t:dgCol title="actualstarttime" hidden="true" field="actualstarttime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="actualendtime" hidden="true" field="actualendtime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="planid" field="planid" hidden="true"></t:dgCol>
   <t:dgCol title="productionorderid" field="productionorderid" hidden="true"></t:dgCol> --%>
   <t:dgCol title="销售订单编号" field="orderid" ></t:dgCol>
   <t:dgCol title="释放时间" field="releasetime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="交货期" field="duedate" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="优先级" field="priority" ></t:dgCol>
   <%-- <t:dgCol title="datastatus" field="datastatus" hidden="true"></t:dgCol>
   <t:dgCol title="memo" field="memo" hidden="true"></t:dgCol> --%>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="planlotController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="planlotController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="planlotController.do?addorupdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="planlotController.do?addorupdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <!-- <script type="text/javascript">
 $(document).ready(function(){
		$("input[name='plannedstarttime']").datebox({required:false});
		$("input[name='plannedendtime']").datebox({required:false});
	});
 </script> -->