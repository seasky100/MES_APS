<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>动态事件</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="dynamiceventsController.do?save">
			<input id="id" name="id" type="hidden" value="${dynamiceventsPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							事件编号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="dynamiceventsno" name="dynamiceventsno" 
							   value="${dynamiceventsPage.dynamiceventsno}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							对象:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="dynamiceventsname" name="dynamiceventsname" 
							   value="${dynamiceventsPage.dynamiceventsname}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							事件类型:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="dynamiceventstype" name="dynamiceventstype" 
							   value="${dynamiceventsPage.dynamiceventstype}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							订单编号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="objectid" name="objectid" 
							   value="${dynamiceventsPage.objectid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							要求交货时间:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="endtime" name="endtime" 
							     value="<fmt:formatDate value='${dynamiceventsPage.endtime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							状态:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="state" name="state" 
							   value="${dynamiceventsPage.state}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							数据类型:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="datastatus" name="datastatus" 
							   value="${dynamiceventsPage.datastatus}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							备注:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="memo" name="memo" 
							   value="${dynamiceventsPage.memo}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
		</t:formvalid>
 </body>