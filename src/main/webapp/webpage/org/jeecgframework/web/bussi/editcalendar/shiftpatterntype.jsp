<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <!-- <title>班次模式</title> -->
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript">
  //初始化下标
	function resetTrNum(tableId) {
		$tbody = $("#"+tableId+"");
		$tbody.find('>tr').each(function(i){
			$(':input, select', this).each(function(){
				var $this = $(this), name = $this.attr('name'), val = $this.val();
				if(name!=null){
					if (name.indexOf("#index#") >= 0){
						$this.attr("name",name.replace('#index#',i));
					}else{
						var s = name.indexOf("[");
						var e = name.indexOf("]");
						var new_name = name.substring(s+1,e);
						$this.attr("name",name.replace(new_name,i));
					}
				}
			});
			$(this).find('div[name=\'xh\']').html(i+1);
		});
	}
 </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" tiptype="1" action="shiftpatterntypeController.do?save">
			<input id="id" name="id" type="hidden" value="${shiftpatterntypePage.id }">
			<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
			<td align="right"><label class="Validform_label">班别:</label></td>
			<td class="value">
				<input nullmsg="请填写shiftpa" errormsg="shiftpa格式不对" class="inputxt" id="shiftpatterntypename" name="shiftpatterntypename" 
									   value="${shiftpatterntypePage.shiftpatterntypename}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">备注:</label></td>
			<td class="value">
				<input nullmsg="请填写memo" errormsg="memo格式不对" class="inputxt" id="memo" name="memo" 
									   value="${shiftpatterntypePage.memo}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			</table>
			<div style="width: auto;height: 200px;">
				<%-- 增加一个div，用于调节页面大小，否则默认太小 --%>
				<div style="width:690px;height:1px;"></div>
				<t:tabs id="tt" iframe="false" tabPosition="top" fit="false">
				 <t:tab href="shiftpatterntypeController.do?shiftList" icon="icon-search" title="班次时间" id="shift"></t:tab>
				</t:tabs>
			</div>
			</t:formvalid>
			<!-- 添加 明细 模版 -->
		<table style="display:none">
		<tbody id="add_shift_table_template">
			<tr>
			 <td align="center"><div style="width: 25px;" name="xh"></div></td>
			 <td align="center"><input style="width:20px;" type="checkbox" name="ck"/></td>
				  <td align="left"><input name="shiftList[#index#].shiftname" maxlength="50" type="text" style="width:120px;"></td>
				  <td align="left"><input name="shiftList[#index#].starttime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="shiftList[#index#].endtime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="shiftList[#index#].memo" maxlength="200" type="text" style="width:120px;"></td>
			</tr>
		</table>
 </body>