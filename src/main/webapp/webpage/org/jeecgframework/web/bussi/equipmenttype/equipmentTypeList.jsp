<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="equipmentTypeList" actionUrl="equipmentTypeController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="equipmenttypeno" field="equipmenttypeno" hidden="false"></t:dgCol>
   <t:dgCol title="加工区名称" field="equipmenttypename" ></t:dgCol>
   <t:dgCol title="说明信息" field="info" ></t:dgCol>
   <t:dgCol title="datastatus" field="datastatus" hidden="false"></t:dgCol>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="equipmentTypeController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="equipmentTypeController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="equipmentTypeController.do?addorupdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="equipmentTypeController.do?addorupdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>