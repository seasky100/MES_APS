<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <!-- <title>产线信息</title> -->
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="equipmentController.do?save">
			<input id="id" name="id" type="hidden" value="${equipmentPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							名称:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="equipmentname" name="equipmentname" 
							   value="${equipmentPage.equipmentname}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							带线班长:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="manager" name="manager" 
							   value="${equipmentPage.manager}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							归属车间:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="workshop" name="workshop" 
							   value="${equipmentPage.workshop}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							产线类型:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="workshop" name="workshop" 
							   value="${equipmentPage.equipmenttypeid}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							状态:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="state" name="state" 
							   value="${equipmentPage.state}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							总工时/班:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="maxprod" name="maxprod" 
							   value="${equipmentPage.maxprod}" datatype="d">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							对应工厂日历:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="calendarid" name="calendarid" 
							   value="${equipmentPage.calendarid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							备注:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="memo" name="memo" 
							   value="${equipmentPage.memo}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
		</t:formvalid>
 </body>