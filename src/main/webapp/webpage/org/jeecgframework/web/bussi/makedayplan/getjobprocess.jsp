<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <!-- <title></title> -->
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="jobprocessController.do?save">
			<input id="id" name="id" type="hidden" value="${jobprocessPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							任务编号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="jobprocessno" name="jobprocessno" 
							   value="${jobprocessPage.jobprocessno}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
					<td align="right">
						<label class="Validform_label">
							数量:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="quantity" name="quantity" 
							   value="${jobprocessPage.quantity}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<%-- <tr>
					<td align="right" hidden="true">
						<label class="Validform_label">
							jobprocessname:
						</label>
					</td>
					<td class="value" hidden="true">
						<input class="inputxt" id="jobprocessname" name="jobprocessname" 
							   value="${jobprocessPage.jobprocessname}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							craftid:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="craftid" name="craftid" 
							   value="${jobprocessPage.craftid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							craftprocessid:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="craftprocessid" name="craftprocessid" 
							   value="${jobprocessPage.craftprocessid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>
				
				<tr>
					<td align="right">
						<label class="Validform_label">
							工序号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="ordernum" name="ordernum" 
							   value="${jobprocessPage.ordernum}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
					<td align="right">
						<label class="Validform_label">
							计划开始时间:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="plannedstarttime" name="plannedstarttime" 
							     value="<fmt:formatDate value='${jobprocessPage.plannedstarttime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							计划结束时间:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="plannedendtime" name="plannedendtime" 
							     value="<fmt:formatDate value='${jobprocessPage.plannedendtime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
					<td align="right">
						<label class="Validform_label">
							车间:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="plannedequipmentsetid" name="plannedequipmentsetid" 
							   value="${jobprocessPage.plannedequipmentsetid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							产线:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="plannedequipmentid" name="plannedequipmentid" 
							   value="${jobprocessPage.plannedequipmentid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>

					<td align="right">
						<label class="Validform_label">
							销售订单编号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="orderid" name="orderid" 
							   value="${jobprocessPage.orderid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<%-- <tr>
					<td align="right">
						<label class="Validform_label">
							actualstarttime:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="actualstarttime" name="actualstarttime" 
							     value="<fmt:formatDate value='${jobprocessPage.actualstarttime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							actualendtime:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="actualendtime" name="actualendtime" 
							     value="<fmt:formatDate value='${jobprocessPage.actualendtime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							actualequipmentsetid:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="actualequipmentsetid" name="actualequipmentsetid" 
							   value="${jobprocessPage.actualequipmentsetid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							actualequipmentid:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="actualequipmentid" name="actualequipmentid" 
							   value="${jobprocessPage.actualequipmentid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>
				
				<%-- <tr>
					<td align="right">
						<label class="Validform_label">
							productionorderid:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="productionorderid" name="productionorderid" 
							   value="${jobprocessPage.productionorderid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							lotid:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="lotid" name="lotid" 
							   value="${jobprocessPage.lotid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							scheduleid:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="scheduleid" name="scheduleid" 
							   value="${jobprocessPage.scheduleid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>
				<tr>
					<td align="right">
						<label class="Validform_label">
							机种名称:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="materialid" name="materialid" 
							   value="${jobprocessPage.materialid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>

					<td align="right">
						<label class="Validform_label">
							是否冻结:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="isfrozen" name="isfrozen" 
							   value="${jobprocessPage.isfrozen}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<%-- <tr>
					<td align="right">
						<label class="Validform_label">
							setuptime:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="setuptime" name="setuptime" 
							   value="${jobprocessPage.setuptime}" datatype="d">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							turnovertime:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="turnovertime" name="turnovertime" 
							   value="${jobprocessPage.turnovertime}" datatype="d">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>

				<%-- <tr>
					<td align="right">
						<label class="Validform_label">
							isoutsourcing:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="isoutsourcing" name="isoutsourcing" 
							   value="${jobprocessPage.isoutsourcing}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>
				<tr>
					<td align="right">
						<label class="Validform_label">
							状态:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="state" name="state" 
							   value="${jobprocessPage.state}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>

					<td align="right">
						<label class="Validform_label">
							备注:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="memo" name="memo" 
							   value="${jobprocessPage.memo}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<%-- <tr>
					<td align="right">
						<label class="Validform_label">
							datastatus:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="datastatus" name="datastatus" 
							   value="${jobprocessPage.datastatus}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>
				
			</table>
		</t:formvalid>
 </body>