<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <!-- <title>日生产计划</title> -->
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript">
  //初始化下标
	function resetTrNum(tableId) {
		$tbody = $("#"+tableId+"");
		$tbody.find('>tr').each(function(i){
			$(':input, select', this).each(function(){
				var $this = $(this), name = $this.attr('name'), val = $this.val();
				if(name!=null){
					if (name.indexOf("#index#") >= 0){
						$this.attr("name",name.replace('#index#',i));
					}else{
						var s = name.indexOf("[");
						var e = name.indexOf("]");
						var new_name = name.substring(s+1,e);
						$this.attr("name",name.replace(new_name,i));
					}
				}
			});
			$(this).find('div[name=\'xh\']').html(i+1);
		});
	}
 </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" tiptype="1" action="dispatchController.do?save">
			<input id="id" name="id" type="hidden" value="${dispatchPage.id }">
			<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
			<td align="right"><label class="Validform_label">日生产计划编号:</label></td>
			<td class="value">
				<input nullmsg="请填写dispatc" errormsg="dispatc格式不对" class="inputxt" id="dispatchno" name="dispatchno" 
									   value="${dispatchPage.dispatchno}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">日生产计划名称:</label></td>
			<td class="value">
				<input nullmsg="请填写dispatc" errormsg="dispatc格式不对" class="inputxt" id="dispatchname" name="dispatchname" 
									   value="${dispatchPage.dispatchname}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">生产管理员:</label></td>
			<td class="value">
				<input nullmsg="请填写dispatc" errormsg="dispatc格式不对" class="inputxt" id="dispatcher" name="dispatcher" 
									   value="${dispatchPage.dispatcher}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">生成时间:</label></td>
			<td class="value">
				<input nullmsg="请填写savetim" errormsg="savetim格式不对" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="savetime" name="savetime" 
									     value="<fmt:formatDate value='${dispatchPage.savetime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">确认人:</label></td>
			<td class="value">
				<input nullmsg="请填写alarmda" errormsg="alarmda格式不对" class="inputxt" id="alarmdates" name="alarmdates" 
									   value="${dispatchPage.alarmdates}" datatype="n">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">确认时间:</label></td>
			<td class="value">
				<input nullmsg="请填写confirm" errormsg="confirm格式不对" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="confirmtime" name="confirmtime" 
									     value="<fmt:formatDate value='${dispatchPage.confirmtime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">审批人:</label></td>
			<td class="value">
				<input nullmsg="请填写checker" errormsg="checker格式不对" class="inputxt" id="checker" name="checker" 
									   value="${dispatchPage.checker}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">审批时间:</label></td>
			<td class="value">
				<input nullmsg="请填写checkti" errormsg="checkti格式不对" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="checktime" name="checktime" 
									     value="<fmt:formatDate value='${dispatchPage.checktime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">状态:</label></td>
			<td class="value">
				<input nullmsg="请填写state" errormsg="state格式不对" class="inputxt" id="state" name="state" 
									   value="${dispatchPage.state}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			</table>
			<div style="width: auto;height: 200px;">
				<%-- 增加一个div，用于调节页面大小，否则默认太小 --%>
				<div style="width:690px;height:1px;"></div>
				<t:tabs id="tt" iframe="false" tabPosition="top" fit="false">
				 <t:tab href="dispatchController.do?dispatchorderList" icon="icon-search" title="日派工单" id="dispatchorder"></t:tab>
				</t:tabs>
			</div>
			</t:formvalid>
			<!-- 添加 明细 模版 -->
		<table style="display:none">
		<tbody id="add_dispatchorder_table_template">
			<tr>
			 <td align="center"><div style="width: 25px;" name="xh"></div></td>
			 <td align="center"><input style="width:20px;" type="checkbox" name="ck"/></td>
				  <td align="left"><input name="dispatchorderList[#index#].ordernum" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].dispatchorderno" maxlength="255" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].equipmentsetid" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].equipmentid" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].workgroupmanager" maxlength="255" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].materialId" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].materialId" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].jobprocessid" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].jobprocessid" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].quantity" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].processID" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].plannedstarttime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].plannedendtime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].actualstarttime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].actualendtime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="dispatchorderList[#index#].numfinished" maxlength="" type="text" style="width:120px;"></td>
			</tr>
		 </tbody>
		</table>
 </body>