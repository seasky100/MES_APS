<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <!-- <title>生产变计划</title> -->
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript">
  //初始化下标
	function resetTrNum(tableId) {
		$tbody = $("#"+tableId+"");
		$tbody.find('>tr').each(function(i){
			$(':input, select', this).each(function(){
				var $this = $(this), name = $this.attr('name'), val = $this.val();
				if(name!=null){
					if (name.indexOf("#index#") >= 0){
						$this.attr("name",name.replace('#index#',i));
					}else{
						var s = name.indexOf("[");
						var e = name.indexOf("]");
						var new_name = name.substring(s+1,e);
						$this.attr("name",name.replace(new_name,i));
					}
				}
			});
			$(this).find('div[name=\'xh\']').html(i+1);
		});
	}
 </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" tiptype="1" action="productionchangeController.do?save">
			<input id="id" name="id" type="hidden" value="${productionchangePage.id }">
			<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
			<td align="right"><label class="Validform_label">生产订单编号:</label></td>
			<td class="value">
				<input nullmsg="请填写product" errormsg="product格式不对" class="inputxt" id="productionchangeno" name="productionchangeno" 
									   value="${productionchangePage.productionchangeno}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">计划员:</label></td>
			<td class="value">
				<input nullmsg="请填写planner" errormsg="planner格式不对" class="inputxt" id="planner" name="planner" 
									   value="${productionchangePage.planner}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">动态调度操作:</label></td>
			<td class="value">
				<input nullmsg="请填写dynamic" errormsg="dynamic格式不对" class="inputxt" id="dynamicscheduleoperation" name="dynamicscheduleoperation" 
									   value="${productionchangePage.dynamicscheduleoperation}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">动态调度目标:</label></td>
			<td class="value">
				<input nullmsg="请填写dynamic" errormsg="dynamic格式不对" class="inputxt" id="dynamicscheduleobjective" name="dynamicscheduleobjective" 
									   value="${productionchangePage.dynamicscheduleobjective}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">申请时间:</label></td>
			<td class="value">
				<input nullmsg="请填写applyti" errormsg="applyti格式不对" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="applytime" name="applytime" 
									     value="<fmt:formatDate value='${productionchangePage.applytime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">变更说明:</label></td>
			<td class="value">
				<input nullmsg="请填写descrip" errormsg="descrip格式不对" class="inputxt" id="description" name="description" 
									   value="${productionchangePage.description}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">变更时间:</label></td>
			<td class="value">
				<input nullmsg="请填写changet" errormsg="changet格式不对" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="changetime" name="changetime" 
									     value="<fmt:formatDate value='${productionchangePage.changetime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			<td align="right"><label class="Validform_label">状态:</label></td>
			<td class="value">
				<input nullmsg="请填写state" errormsg="state格式不对" class="inputxt" id="state" name="state" 
									   value="${productionchangePage.state}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			<tr>
			<td align="right"><label class="Validform_label">备注:</label></td>
			<td class="value">
				<input nullmsg="请填写memo" errormsg="memo格式不对" class="inputxt" id="memo" name="memo" 
									   value="${productionchangePage.memo}" datatype="*">
								<span class="Validform_checktip"></span>
			</td>
			</tr>
			</table>
			<div style="width: auto;height: 200px;">
				<%-- 增加一个div，用于调节页面大小，否则默认太小 --%>
				<div style="width:690px;height:1px;"></div>
				<t:tabs id="tt" iframe="false" tabPosition="top" fit="false">
				 <t:tab href="productionchangeController.do?changelistList" icon="icon-search" title="变更单" id="changelist"></t:tab>
				</t:tabs>
			</div>
			</t:formvalid>
			<!-- 添加 明细 模版 -->
		<table style="display:none">
		<tbody id="add_changelist_table_template">
			<tr>
			 <td align="center"><div style="width: 25px;" name="xh"></div></td>
			 <td align="center"><input style="width:20px;" type="checkbox" name="ck"/></td>
				  <td align="left"><input name="changelistList[#index#].changelistno" maxlength="50" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].changelistname" maxlength="50" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].productionchangeId" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].changedoctype" maxlength="50" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].changedocno" maxlength="100" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].materialid" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].changedmaterialid" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].quantity" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].changedquantity" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].starttime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].changedstarttime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].endtime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].changedendtime" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].datastatus" maxlength="" type="text" style="width:120px;"></td>
				  <td align="left"><input name="changelistList[#index#].memo" maxlength="200" type="text" style="width:120px;"></td>
			</tr>
		 </tbody>
		</table>
 </body>