<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="planList" actionUrl="planController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="计划编号" field="planno" ></t:dgCol>
   <t:dgCol title="计划名称" field="planname" ></t:dgCol>
   <t:dgCol title="计划人" field="planner" ></t:dgCol>
   <t:dgCol title="保存时间" field="savetime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="提示日期" field="alarmdates" ></t:dgCol>
   <t:dgCol title="审核人" field="checker" ></t:dgCol>
   <t:dgCol title="检查号" field="numcheck" ></t:dgCol>
   <t:dgCol title="检查信息" field="checkinfo" ></t:dgCol>
   <t:dgCol title="状态" field="state" ></t:dgCol>
   <t:dgCol title="算法" field="algorithminfo" ></t:dgCol>
   <t:dgCol title="数据状态" field="datastatus" ></t:dgCol>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="planController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="planController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="planController.do?addorupdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="planController.do?addorupdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>