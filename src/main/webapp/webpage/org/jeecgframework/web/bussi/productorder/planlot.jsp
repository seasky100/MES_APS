<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>生产订单管理</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body style="overflow-y: hidden" scroll="no">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="planlotController.do?save">
			<input id="id" name="id" type="hidden" value="${planlotPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							生产订单编号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="lotno" name="lotno" 
							   value="${planlotPage.lotno}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				
					<td align="right">
						<label class="Validform_label">
							机种名称:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="lotname" name="lotname" 
							   value="${planlotPage.lotname}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							数量:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="lotquantity" name="lotquantity" 
							   value="${planlotPage.lotquantity}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				
					<td align="right">
						<label class="Validform_label">
							是否冻结:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="isfrozen" name="isfrozen" 
							   value="${planlotPage.isfrozen}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<%-- <tr>
					<td align="right" hidden="true">
						<label class="Validform_label">
							color:
						</label>
					</td>
					<td class="value" hidden="true">
						<input class="inputxt" id="color" name="color" 
							   value="${planlotPage.color}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>
				<tr>
					<td align="right">
						<label class="Validform_label">
							状态:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="lotstate" name="lotstate" 
							   value="${planlotPage.lotstate}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>

					<td align="right">
						<label class="Validform_label">
							零件号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="materialId" name="materialId" 
							   value="${planlotPage.materialId}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<%-- <tr>
					<td align="right" hidden="true">
						<label class="Validform_label">
							craftid:
						</label>
					</td>
					<td class="value" hidden="true">
						<input class="inputxt" id="craftid" name="craftid" 
							   value="${planlotPage.craftid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>
				<tr>
					<td align="right">
						<label class="Validform_label">
							完成数量:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="numfinished" name="numfinished" 
							   value="${planlotPage.numfinished}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>

					<td align="right">
						<label class="Validform_label">
							计划开始时间:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="plannedstarttime" name="plannedstarttime" 
							     value="<fmt:formatDate value='${planlotPage.plannedstarttime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							计划完成时间:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="plannedendtime" name="plannedendtime" 
							     value="<fmt:formatDate value='${planlotPage.plannedendtime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>

					<td align="right">
						<label class="Validform_label">
							销售订单编号:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="orderid" name="orderid" 
							   value="${planlotPage.orderid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<%-- <tr>
					<td align="right" hidden="true">
						<label class="Validform_label">
							actualstarttime:
						</label>
					</td>
					<td class="value" hidden="true">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="actualstarttime" name="actualstarttime" 
							     value="<fmt:formatDate value='${planlotPage.actualstarttime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right" hidden="true">
						<label class="Validform_label">
							actualendtime:
						</label>
					</td>
					<td class="value" hidden="true">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="actualendtime" name="actualendtime" 
							     value="<fmt:formatDate value='${planlotPage.actualendtime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right" hidden="true">
						<label class="Validform_label">
							planid:
						</label>
					</td>
					<td class="value" hidden="true">
						<input class="inputxt" id="planid" name="planid" 
							   value="${planlotPage.planid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right" hidden="true">
						<label class="Validform_label">
							productionorderid:
						</label>
					</td >
					<td class="value" hidden="true">
						<input class="inputxt" id="productionorderid" name="productionorderid" 
							   value="${planlotPage.productionorderid}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>
				
				<tr>
					<td align="right">
						<label class="Validform_label">
							释放时间:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="releasetime" name="releasetime" 
							     value="<fmt:formatDate value='${planlotPage.releasetime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>

					<td align="right">
						<label class="Validform_label">
							交货期:
						</label>
					</td>
					<td class="value">
						<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="duedate" name="duedate" 
							     value="<fmt:formatDate value='${planlotPage.duedate}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							优先级:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="priority" name="priority" 
							   value="${planlotPage.priority}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<%-- <tr>
					<td align="right" hidden="true">
						<label class="Validform_label">
							datastatus:
						</label>
					</td>
					<td class="value" hidden="true">
						<input class="inputxt" id="datastatus" name="datastatus" 
							   value="${planlotPage.datastatus}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right" hidden="true">
						<label class="Validform_label">
							memo:
						</label>
					</td>
					<td class="value" hidden="true">
						<input class="inputxt" id="memo" name="memo" 
							   value="${planlotPage.memo}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr> --%>
			</table>
		</t:formvalid>
 </body>