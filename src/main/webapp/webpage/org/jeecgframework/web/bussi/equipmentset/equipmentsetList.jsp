<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="equipmentsetList" actionUrl="equipmentsetController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="equipmentsetno" field="equipmentsetno" hidden="false"></t:dgCol>
   <t:dgCol title="车间名称" field="equipmentsetname" ></t:dgCol>
   <t:dgCol title="车间经理" field="manager" ></t:dgCol>
   <t:dgCol title="deptname" field="deptname" hidden="false"></t:dgCol>
   <t:dgCol title="加工区" field="equipmenttypeid" ></t:dgCol>
   <t:dgCol title="状态" field="state" ></t:dgCol>
   <t:dgCol title="maxprod" field="maxprod" hidden="false"></t:dgCol>
   <t:dgCol title="calendarid" field="calendarid" hidden="false"></t:dgCol>
   <t:dgCol title="datastatus" field="datastatus" hidden="false"></t:dgCol>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="equipmentsetController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="equipmentsetController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="equipmentsetController.do?addorupdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="equipmentsetController.do?addorupdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>