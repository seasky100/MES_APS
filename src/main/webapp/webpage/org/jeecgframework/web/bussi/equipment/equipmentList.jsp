<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="equipmentList" actionUrl="equipmentController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="equipmentno" field="equipmentno" hidden="false"></t:dgCol>
   <t:dgCol title="产线名称" field="equipmentname" ></t:dgCol>
   <t:dgCol title="带线班长" field="manager" ></t:dgCol>
   <t:dgCol title="归属车间" field="workshop" ></t:dgCol>
   <t:dgCol title="station" field="station" hidden="false"></t:dgCol>
   <t:dgCol title="加工区" field="equipmenttypeid" ></t:dgCol>
   <t:dgCol title="状态" field="state" ></t:dgCol>
   <t:dgCol title="总工时/班" field="maxprod"></t:dgCol>
   <t:dgCol title="producer" field="producer" hidden="false"></t:dgCol>
   <t:dgCol title="price" field="price" hidden="false"></t:dgCol>
   <t:dgCol title="proddate" field="proddate" formatter="yyyy-MM-dd hh:mm:ss" hidden="false"></t:dgCol>
   <t:dgCol title="purchasedate" field="purchasedate" formatter="yyyy-MM-dd hh:mm:ss" hidden="false"></t:dgCol>
   <t:dgCol title="总工时/班" field="timeinuse"  hidden="false"></t:dgCol>
   <t:dgCol title="对应工厂日历" field="calendarid" ></t:dgCol>
   <t:dgCol title="datastatus" field="datastatus" hidden="false"></t:dgCol>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="equipmentController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="equipmentController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="equipmentController.do?addorupdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="equipmentController.do?addorupdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>