<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="productionchangeList" fitColumns="true"  actionUrl="productionchangeController.do?datagrid" idField="id" fit="true">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="生产变更单编号" field="productionchangeno" ></t:dgCol>
   <t:dgCol title="productionchangename" field="productionchangename" hidden="false"></t:dgCol>
   <t:dgCol title="计划员" field="planner" ></t:dgCol>
   <t:dgCol title="动态调度操作" field="dynamicscheduleoperation" ></t:dgCol>
   <t:dgCol title="动态调度目标" field="dynamicscheduleobjective" ></t:dgCol>
   <t:dgCol title="申请时间" field="applytime" formatter="yyyy-MM-dd hh:mm:ss" ></t:dgCol>
   <t:dgCol title="变更说明" field="description" ></t:dgCol>
   <t:dgCol title="变更时间" field="changetime" formatter="yyyy-MM-dd hh:mm:ss"></t:dgCol>
   <t:dgCol title="状态" field="state" ></t:dgCol>
   <t:dgCol title="confirmer" field="confirmer" hidden="false"></t:dgCol>
   <t:dgCol title="numconfirm" field="numconfirm" hidden="false"></t:dgCol>
   <t:dgCol title="confirmtime" field="confirmtime" formatter="yyyy-MM-dd hh:mm:ss" hidden="false"></t:dgCol>
   <t:dgCol title="confirminfo" field="confirminfo" hidden="false"></t:dgCol>
   <t:dgCol title="checker" field="checker" hidden="false"></t:dgCol>
   <t:dgCol title="numcheck" field="numcheck" hidden="false"></t:dgCol>
   <t:dgCol title="checktime" field="checktime" formatter="yyyy-MM-dd hh:mm:ss" hidden="false"></t:dgCol>
   <t:dgCol title="checkinfo" field="checkinfo" hidden="false"></t:dgCol>
   <t:dgCol title="datastatus" field="datastatus" hidden="false"></t:dgCol>
   <t:dgCol title="备注" field="memo" ></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="productionchangeController.do?del&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="productionchangeController.do?addorupdate" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="productionchangeController.do?addorupdate" funname="update"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>