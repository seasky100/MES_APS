<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<script type="text/javascript">
	$('#addProductionorderBtn').linkbutton({   
	    iconCls: 'icon-add'  
	});  
	$('#delProductionorderBtn').linkbutton({   
	    iconCls: 'icon-remove'  
	}); 
	$('#addProductionorderBtn').bind('click', function(){   
 		 var tr =  $("#add_productionorder_table_template tr").clone();
	 	 $("#add_productionorder_table").append(tr);
	 	 resetTrNum('add_productionorder_table');
    });  
	$('#delProductionorderBtn').bind('click', function(){   
      	$("#add_productionorder_table").find("input:checked").parent().parent().remove();   
        resetTrNum('add_productionorder_table'); 
    }); 
    $(document).ready(function(){
    	$(".datagrid-toolbar").parent().css("width","auto");
    });
</script>
<div style="padding: 3px; height: 25px;width:auto;" class="datagrid-toolbar">
	<a id="addProductionorderBtn" href="#">添加</a> <a id="delProductionorderBtn" href="#">删除</a> 
</div>
<div style="width: auto;height: 300px;overflow-y:auto;overflow-x:scroll;">
<table border="0" cellpadding="2" cellspacing="0" id="productionorder_table">
	<tr bgcolor="#E6E6E6">
		<td align="center" bgcolor="#EEEEEE">序号</td>
		<td align="center" bgcolor="#EEEEEE">操作</td>
				  <td align="left" bgcolor="#EEEEEE">编号</td>
				  <td align="left" bgcolor="#EEEEEE">机种名称</td>
				  <td align="left" bgcolor="#EEEEEE" hidden="true">isfroze</td>
				  <td align="left" bgcolor="#EEEEEE">零件号</td>
				  <td align="left" bgcolor="#EEEEEE" hidden="true">orderli</td>
				  <td align="left" bgcolor="#EEEEEE">数量</td>
				  <td align="left" bgcolor="#EEEEEE" hidden="true">quantit</td>
				  <td align="left" bgcolor="#EEEEEE" hidden="true">quantit</td>
				  <td align="left" bgcolor="#EEEEEE" hidden="true">color</td>
				  <td align="left" bgcolor="#EEEEEE" hidden="true">earlies</td>
				  <td align="left" bgcolor="#EEEEEE">要求交货期</td>
				  <td align="left" bgcolor="#EEEEEE">齐料日期</td>
				  <td align="left" bgcolor="#EEEEEE">优先级</td>
				  <td align="left" bgcolor="#EEEEEE">状态</td>
				  <td align="left" bgcolor="#EEEEEE">预警提前期</td>
				  <td align="left" bgcolor="#EEEEEE" hidden="true">datasta</td>
				  <td align="left" bgcolor="#EEEEEE" hidden="true">memo</td>
	</tr>
	<tbody id="add_productionorder_table">	
	<c:if test="${fn:length(productionorderList)  <= 0 }">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">1</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck"/></td>
				  <td align="left"><input name="productionorderList[0].productionorderno" maxlength="50" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="productionorderList[0].productionordername" maxlength="50" type="text" style="width:120px;" ></td>
				  <td align="left" hidden="true"><input name="productionorderList[0].isfrozen" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="productionorderList[0].materialId" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left" hidden="true"><input name="productionorderList[0].orderlistId" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="productionorderList[0].quantity" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left" hidden="true"><input name="productionorderList[0].quantityu" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left" hidden="true"><input name="productionorderList[0].quantityf" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left" hidden="true"><input name="productionorderList[0].color" maxlength="50" type="text" style="width:120px;" ></td>
				  <td align="left" hidden="true"><input name="productionorderList[0].earliestduedate" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="productionorderList[0].latestduedate" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="productionorderList[0].actualdeliverydate" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="productionorderList[0].priority" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="productionorderList[0].state" maxlength="20" type="text" style="width:120px;" ></td>
				  <td align="left"><input name="productionorderList[0].alarmdates" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left" hidden="true"><input name="productionorderList[0].datastatus" maxlength="" type="text" style="width:120px;" ></td>
				  <td align="left" hidden="true"><input name="productionorderList[0].memo" maxlength="200" type="text" style="width:120px;" ></td>
   			</tr>
	</c:if>
	<c:if test="${fn:length(productionorderList)  > 0 }">
		<c:forEach items="${productionorderList}" var="poVal" varStatus="stuts">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">${stuts.index+1 }</div></td>
				<td align="center"><input style="width:20px;"  type="checkbox" name="ck" /></td>
				<input name="productionorderList[${stuts.index }].id"  value="${poVal.id }" type="hidden" >
				   <td align="left"><input name="productionorderList[${stuts.index }].productionorderno" maxlength="50" value="${poVal.productionorderno }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="productionorderList[${stuts.index }].productionordername" maxlength="50" value="${poVal.productionordername }" type="text" style="width:120px;"></td>
				   <td align="left" hidden="true"><input name="productionorderList[${stuts.index }].isfrozen" maxlength="" value="${poVal.isfrozen }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="productionorderList[${stuts.index }].materialId" maxlength="" value="${poVal.materialId }" type="text" style="width:120px;"></td>
				   <td align="left" hidden="true"><input name="productionorderList[${stuts.index }].orderlistId" maxlength="" value="${poVal.orderlistId }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="productionorderList[${stuts.index }].quantity" maxlength="" value="${poVal.quantity }" type="text" style="width:120px;"></td>
				   <td align="left" hidden="true"><input name="productionorderList[${stuts.index }].quantityu" maxlength="" value="${poVal.quantityu }" type="text" style="width:120px;"></td>
				   <td align="left" hidden="true"><input name="productionorderList[${stuts.index }].quantityf" maxlength="" value="${poVal.quantityf }" type="text" style="width:120px;"></td>
				   <td align="left" hidden="true"><input name="productionorderList[${stuts.index }].color" maxlength="50" value="${poVal.color }" type="text" style="width:120px;"></td>
				   <td align="left" hidden="true"><input name="productionorderList[${stuts.index }].earliestduedate" maxlength="" value="${poVal.earliestduedate }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="productionorderList[${stuts.index }].latestduedate" maxlength="" value="${poVal.latestduedate }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="productionorderList[${stuts.index }].actualdeliverydate" maxlength="" value="${poVal.actualdeliverydate }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="productionorderList[${stuts.index }].priority" maxlength="" value="${poVal.priority }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="productionorderList[${stuts.index }].state" maxlength="20" value="${poVal.state }" type="text" style="width:120px;"></td>
				   <td align="left"><input name="productionorderList[${stuts.index }].alarmdates" maxlength="" value="${poVal.alarmdates }" type="text" style="width:120px;"></td>
				   <td align="left" hidden="true"><input name="productionorderList[${stuts.index }].datastatus" maxlength="" value="${poVal.datastatus }" type="text" style="width:120px;"></td>
				   <td align="left" hidden="true"><input name="productionorderList[${stuts.index }].memo" maxlength="200" value="${poVal.memo }" type="text" style="width:120px;"></td>
   			</tr>
		</c:forEach>
	</c:if>
	</tbody>
</table>
</div>