package org.jeecgframework.web.bussi.service.impl.workgroup;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.jeecgframework.web.bussi.entity.product.CraftEntity;
import org.jeecgframework.web.bussi.entity.product.ProcessEntity;
import org.jeecgframework.web.bussi.entity.workgroup.SystemuserEntity;
import org.jeecgframework.web.bussi.entity.workgroup.WorkgroupEntity;
import org.jeecgframework.web.bussi.entity.workgroup.WorkgroupWorkerEntity;
import org.jeecgframework.web.bussi.service.workgroup.WorkgroupServiceI;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.jeecgframework.core.util.MyBeanUtils;

@Service("workgroupService")
@Transactional
public class WorkgroupServiceImpl extends CommonServiceImpl implements WorkgroupServiceI {
	
	public void addMain(WorkgroupEntity workgroup,
	        List<SystemuserEntity> sysUserList){
			//保存主信息
			this.save(workgroup);
		
			/**保存-工序信息*/
			for(SystemuserEntity systemuser:sysUserList){
				//外键设置
				this.save(systemuser);
				WorkgroupWorkerEntity wwEntity = new WorkgroupWorkerEntity();
				wwEntity.setDatastatus(1);
				wwEntity.setSystemuserid(systemuser.getId());
				wwEntity.setWorkgroupid(workgroup.getId());
				this.save(wwEntity);
			}
	}

	
	public void updateMain(WorkgroupEntity workgroup,
	        List<SystemuserEntity> sysUserList) {
		//保存订单主信息
		this.saveOrUpdate(workgroup);
		
		
		//===================================================================================
		//获取参数
		Object id0 = workgroup.getId();
		//===================================================================================
		//1.查询出数据库的明细数据-工序信息
	    String hql0 = "from SystemuserEntity where id in (select ww.systemuserid from WorkgroupWorkerEntity ww where ww.workgroupid = ?)";
	    List<SystemuserEntity> sysUserOldList = this.findHql(hql0,id0);
		//2.筛选更新明细数据-工序信息
		for(SystemuserEntity oldE:sysUserOldList){
			boolean isUpdate = false;
				for(SystemuserEntity sendE:sysUserList){
					//需要更新的明细数据-工序信息
					if(oldE.getId().equals(sendE.getId())){
		    			try {
							MyBeanUtils.copyBeanNotNull2Bean(sendE,oldE);
							this.saveOrUpdate(oldE);
						} catch (Exception e) {
							e.printStackTrace();
							throw new BusinessException(e.getMessage());
						}
						isUpdate= true;
		    			break;
		    		}
		    	}
	    		if(!isUpdate){
		    		//如果数据库存在的明细，前台没有传递过来则是删除-工序信息
		    		super.delete(oldE);
	    		}
	    		
			}
		//3.持久化新增的数据-工序信息
		for(SystemuserEntity systemuser:sysUserList){
			if(systemuser.getId()==null){
				this.save(systemuser);
			}
		}
		
	}

	
	public void delMain(WorkgroupEntity workgroup) {
		//删除主表信息
		this.delete(workgroup);
		
		//===================================================================================
		//获取参数
		Object id0 = workgroup.getId();
		//===================================================================================
		//删除-工序信息
	    String hql0 = "from SystemuserEntity where id in (select ww.systemuserid from WorkgroupWorkerEntity ww where ww.workgroupid = ?)";
	    List<SystemuserEntity> sysOldUserList = this.findHql(hql0,id0);
		this.deleteAllEntitie(sysOldUserList);
	}
}