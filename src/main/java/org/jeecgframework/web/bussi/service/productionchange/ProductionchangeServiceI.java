package org.jeecgframework.web.bussi.service.productionchange;

import java.util.List;
import org.jeecgframework.core.common.service.CommonService;
import org.jeecgframework.web.bussi.entity.productionchange.ProductionchangeEntity;
import org.jeecgframework.web.bussi.entity.productionchange.ChangelistEntity;

public interface ProductionchangeServiceI extends CommonService{

	/**
	 * 添加一对多
	 * 
	 */
	public void addMain(ProductionchangeEntity productionchange,
	        List<ChangelistEntity> changelistList) ;
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(ProductionchangeEntity productionchange,
	        List<ChangelistEntity> changelistList);
	public void delMain (ProductionchangeEntity productionchange);
}
