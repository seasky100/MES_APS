package org.jeecgframework.web.bussi.service.impl.makemonthplan;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.makemonthplan.PlanServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("planService")
@Transactional
public class PlanServiceImpl extends CommonServiceImpl implements PlanServiceI {
	
}