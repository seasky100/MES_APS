package org.jeecgframework.web.bussi.service.impl.dayplanstat;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import org.jeecgframework.web.bussi.service.dayplanstat.DispatchServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.web.bussi.entity.dayplanstat.DispatchEntity;
import org.jeecgframework.web.bussi.entity.dayplanstat.DispatchorderEntity;
@Service("dispatchService")
@Transactional
public class DispatchServiceImpl extends CommonServiceImpl implements DispatchServiceI {

	
	public void addMain(DispatchEntity dispatch,
	        List<DispatchorderEntity> dispatchorderList){
			//保存主信息
			this.save(dispatch);
		
			/**保存-日派工单*/
			for(DispatchorderEntity dispatchorder:dispatchorderList){
				this.save(dispatchorder);
			}
	}

	
	public void updateMain(DispatchEntity dispatch,
	        List<DispatchorderEntity> dispatchorderList) {
		//保存订单主信息
		this.saveOrUpdate(dispatch);
		
		
		//===================================================================================
		//获取参数
		Object id = dispatch.getId();
		//===================================================================================
		//1.查询出数据库的明细数据-日派工单
	    String hql0 = "from DispatchorderEntity where 1 = 1 and dispatch_Id = ?";
	    List<DispatchorderEntity> dispatchorderOldList = this.findHql(hql0,id);
		//2.筛选更新明细数据-日派工单
		for(DispatchorderEntity oldE:dispatchorderOldList){
			boolean isUpdate = false;
				for(DispatchorderEntity sendE:dispatchorderList){
					//需要更新的明细数据-日派工单
					if(oldE.getId().equals(sendE.getId())){
		    			try {
							MyBeanUtils.copyBeanNotNull2Bean(sendE,oldE);
							this.saveOrUpdate(oldE);
						} catch (Exception e) {
							e.printStackTrace();
							throw new BusinessException(e.getMessage());
						}
						isUpdate= true;
		    			break;
		    		}
		    	}
	    		if(!isUpdate){
		    		//如果数据库存在的明细，前台没有传递过来则是删除-日派工单
		    		super.delete(oldE);
	    		}
	    		
			}
		//3.持久化新增的数据-日派工单
		for(DispatchorderEntity dispatchorder:dispatchorderList){
			if(dispatchorder.getId()==null){
				//外键设置
				this.save(dispatchorder);
			}
		}
		
	}

	
	public void delMain(DispatchEntity dispatch) {
		//删除主表信息
		this.delete(dispatch);
		
		//===================================================================================
		//获取参数
		Object id = dispatch.getId();
		//===================================================================================
		//删除-日派工单
	    String hql0 = "from DispatchorderEntity where 1 = 1 and dispatch_Id = ?";
	    List<DispatchorderEntity> dispatchorderOldList = this.findHql(hql0,id);
		this.deleteAllEntitie(dispatchorderOldList);
	}
	
}