package org.jeecgframework.web.bussi.service.impl.equipment;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.equipment.EquipmentServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("equipmentService")
@Transactional
public class EquipmentServiceImpl extends CommonServiceImpl implements EquipmentServiceI {
	
}