package org.jeecgframework.web.bussi.service.impl.editcalendar;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.editcalendar.CalendarServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("calendarService")
@Transactional
public class CalendarServiceImpl extends CommonServiceImpl implements CalendarServiceI {
	
}