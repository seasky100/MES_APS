package org.jeecgframework.web.bussi.service.product;

import java.util.List;
import org.jeecgframework.core.common.service.CommonService;
import org.jeecgframework.web.bussi.entity.product.CraftEntity;
import org.jeecgframework.web.bussi.entity.product.ProcessEntity;

public interface CraftServiceI extends CommonService{

	/**
	 * 添加一对多
	 * 
	 */
	public void addMain(CraftEntity craft,
	        List<ProcessEntity> processList) ;
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(CraftEntity craft,
	        List<ProcessEntity> processList);
	public void delMain (CraftEntity craft);
}
