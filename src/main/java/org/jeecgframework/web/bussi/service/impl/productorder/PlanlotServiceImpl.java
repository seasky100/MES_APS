package org.jeecgframework.web.bussi.service.impl.productorder;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.productorder.PlanlotServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("planlotService")
@Transactional
public class PlanlotServiceImpl extends CommonServiceImpl implements PlanlotServiceI {
	
}