package org.jeecgframework.web.bussi.service.impl.changemanage;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.changemanage.DynamiceventsServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("dynamiceventsService")
@Transactional
public class DynamiceventsServiceImpl extends CommonServiceImpl implements DynamiceventsServiceI {
	
}