package org.jeecgframework.web.bussi.service.saleorder;

import java.util.List;
import org.jeecgframework.core.common.service.CommonService;
import org.jeecgframework.web.bussi.entity.saleorder.OrderlistEntity;
import org.jeecgframework.web.bussi.entity.saleorder.ProductionorderEntity;

public interface OrderlistServiceI extends CommonService{

	/**
	 * 添加一对多
	 * 
	 */
	public void addMain(OrderlistEntity orderlist,
	        List<ProductionorderEntity> productionorderList) ;
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(OrderlistEntity orderlist,
	        List<ProductionorderEntity> productionorderList);
	public void delMain (OrderlistEntity orderlist);
}
