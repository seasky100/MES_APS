package org.jeecgframework.web.bussi.service.impl.equipmenttype;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.equipmenttype.EquipmentTypeServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("equipmentTypeService")
@Transactional
public class EquipmentTypeServiceImpl extends CommonServiceImpl implements EquipmentTypeServiceI {
	
}