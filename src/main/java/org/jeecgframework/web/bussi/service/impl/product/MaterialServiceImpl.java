package org.jeecgframework.web.bussi.service.impl.product;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.product.MaterialServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("materialService")
@Transactional
public class MaterialServiceImpl extends CommonServiceImpl implements MaterialServiceI {
	
}