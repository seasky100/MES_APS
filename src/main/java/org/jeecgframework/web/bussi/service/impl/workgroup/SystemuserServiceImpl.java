package org.jeecgframework.web.bussi.service.impl.workgroup;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.workgroup.SystemuserServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("systemuserService")
@Transactional
public class SystemuserServiceImpl extends CommonServiceImpl implements SystemuserServiceI {
	
}