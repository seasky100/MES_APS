package org.jeecgframework.web.bussi.service.impl.product;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import org.jeecgframework.web.bussi.service.product.CraftServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.web.bussi.entity.product.CraftEntity;
import org.jeecgframework.web.bussi.entity.product.ProcessEntity;
@Service("craftService")
@Transactional
public class CraftServiceImpl extends CommonServiceImpl implements CraftServiceI {

	
	public void addMain(CraftEntity craft,
	        List<ProcessEntity> processList){
			//保存主信息
			this.save(craft);
		
			/**保存-工序信息*/
			for(ProcessEntity process:processList){
				//外键设置
				process.setCraftId(craft.getId());
				this.save(process);
			}
	}

	
	public void updateMain(CraftEntity craft,
	        List<ProcessEntity> processList) {
		//保存订单主信息
		this.saveOrUpdate(craft);
		
		
		//===================================================================================
		//获取参数
		Object id0 = craft.getId();
		//===================================================================================
		//1.查询出数据库的明细数据-工序信息
	    String hql0 = "from ProcessEntity where 1 = 1 AND craftId = ? ";
	    List<ProcessEntity> processOldList = this.findHql(hql0,id0);
		//2.筛选更新明细数据-工序信息
		for(ProcessEntity oldE:processOldList){
			boolean isUpdate = false;
				for(ProcessEntity sendE:processList){
					//需要更新的明细数据-工序信息
					if(oldE.getId().equals(sendE.getId())){
		    			try {
							MyBeanUtils.copyBeanNotNull2Bean(sendE,oldE);
							this.saveOrUpdate(oldE);
						} catch (Exception e) {
							e.printStackTrace();
							throw new BusinessException(e.getMessage());
						}
						isUpdate= true;
		    			break;
		    		}
		    	}
	    		if(!isUpdate){
		    		//如果数据库存在的明细，前台没有传递过来则是删除-工序信息
		    		super.delete(oldE);
	    		}
	    		
			}
		//3.持久化新增的数据-工序信息
		for(ProcessEntity process:processList){
			if(process.getId()==null){
				//外键设置
				process.setCraftId(craft.getId());
				//process.setId(craft.getId());
				this.save(process);
			}
		}
		
	}

	
	public void delMain(CraftEntity craft) {
		//删除主表信息
		this.delete(craft);
		
		//===================================================================================
		//获取参数
		Object id0 = craft.getId();
		//===================================================================================
		//删除-工序信息
	    String hql0 = "from ProcessEntity where 1 = 1 AND craftId = ? ";
	    List<ProcessEntity> processOldList = this.findHql(hql0,id0);
		this.deleteAllEntitie(processOldList);
	}
	
}