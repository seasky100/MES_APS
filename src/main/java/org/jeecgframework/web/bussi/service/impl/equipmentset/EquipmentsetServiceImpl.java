package org.jeecgframework.web.bussi.service.impl.equipmentset;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.equipmentset.EquipmentsetServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("equipmentsetService")
@Transactional
public class EquipmentsetServiceImpl extends CommonServiceImpl implements EquipmentsetServiceI {
	
}