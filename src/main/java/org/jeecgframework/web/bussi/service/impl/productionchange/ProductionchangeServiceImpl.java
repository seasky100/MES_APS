package org.jeecgframework.web.bussi.service.impl.productionchange;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import org.jeecgframework.web.bussi.service.productionchange.ProductionchangeServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.web.bussi.entity.productionchange.ProductionchangeEntity;
import org.jeecgframework.web.bussi.entity.productionchange.ChangelistEntity;
@Service("productionchangeService")
@Transactional
public class ProductionchangeServiceImpl extends CommonServiceImpl implements ProductionchangeServiceI {

	
	public void addMain(ProductionchangeEntity productionchange,
	        List<ChangelistEntity> changelistList){
			//保存主信息
			this.save(productionchange);
		
			/**保存-变更单*/
			for(ChangelistEntity changelist:changelistList){
				this.save(changelist);
			}
	}

	
	public void updateMain(ProductionchangeEntity productionchange,
	        List<ChangelistEntity> changelistList) {
		//保存订单主信息
		this.saveOrUpdate(productionchange);
		
		
		//===================================================================================
		//获取参数
		Object id = productionchange.getId();
		//===================================================================================
		//1.查询出数据库的明细数据-变更单
	    String hql0 = "from ChangelistEntity where 1 = 1 and ProductionChange_ID = ?";
	    List<ChangelistEntity> changelistOldList = this.findHql(hql0,id);
		//2.筛选更新明细数据-变更单
		for(ChangelistEntity oldE:changelistOldList){
			boolean isUpdate = false;
				for(ChangelistEntity sendE:changelistList){
					//需要更新的明细数据-变更单
					if(oldE.getId().equals(sendE.getId())){
		    			try {
							MyBeanUtils.copyBeanNotNull2Bean(sendE,oldE);
							this.saveOrUpdate(oldE);
						} catch (Exception e) {
							e.printStackTrace();
							throw new BusinessException(e.getMessage());
						}
						isUpdate= true;
		    			break;
		    		}
		    	}
	    		if(!isUpdate){
		    		//如果数据库存在的明细，前台没有传递过来则是删除-变更单
		    		super.delete(oldE);
	    		}
	    		
			}
		//3.持久化新增的数据-变更单
		for(ChangelistEntity changelist:changelistList){
			if(changelist.getId()==null){
				//外键设置
				this.save(changelist);
			}
		}
	}

	
	public void delMain(ProductionchangeEntity productionchange) {
		//删除主表信息
		this.delete(productionchange);
		
		//===================================================================================
		//获取参数
		Object id = productionchange.getId();
		//===================================================================================
		//删除-变更单
	    String hql0 = "from ChangelistEntity where 1 = 1 and ProductionChange_ID = ?";
	    List<ChangelistEntity> changelistOldList = this.findHql(hql0,id);
		this.deleteAllEntitie(changelistOldList);
	}
	
}