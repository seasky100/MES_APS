package org.jeecgframework.web.bussi.service.impl.editcalendar;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import org.jeecgframework.web.bussi.service.editcalendar.ShiftpatterntypeServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.web.bussi.entity.editcalendar.ShiftpatterntypeEntity;
import org.jeecgframework.web.bussi.entity.editcalendar.ShiftEntity;
@Service("shiftpatterntypeService")
@Transactional
public class ShiftpatterntypeServiceImpl extends CommonServiceImpl implements ShiftpatterntypeServiceI {

	
	public void addMain(ShiftpatterntypeEntity shiftpatterntype,
	        List<ShiftEntity> shiftList){
			//保存主信息
			this.save(shiftpatterntype);
		
			/**保存-班次时间*/
			for(ShiftEntity shift:shiftList){
				this.save(shift);
			}
	}

	
	public void updateMain(ShiftpatterntypeEntity shiftpatterntype,
	        List<ShiftEntity> shiftList) {
		//保存订单主信息
		this.saveOrUpdate(shiftpatterntype);
		
		
		//===================================================================================
		//获取参数
		Object id = shiftpatterntype.getId();
		//===================================================================================
		//1.查询出数据库的明细数据-班次时间
	    String hql0 = "from ShiftEntity where 1 = 1 and ShiftPatternType_ID = ?";
	    List<ShiftEntity> shiftOldList = this.findHql(hql0,id);
		//2.筛选更新明细数据-班次时间
		for(ShiftEntity oldE:shiftOldList){
			boolean isUpdate = false;
				for(ShiftEntity sendE:shiftList){
					//需要更新的明细数据-班次时间
					if(oldE.getId().equals(sendE.getId())){
		    			try {
							MyBeanUtils.copyBeanNotNull2Bean(sendE,oldE);
							this.saveOrUpdate(oldE);
						} catch (Exception e) {
							e.printStackTrace();
							throw new BusinessException(e.getMessage());
						}
						isUpdate= true;
		    			break;
		    		}
		    	}
	    		if(!isUpdate){
		    		//如果数据库存在的明细，前台没有传递过来则是删除-班次时间
		    		super.delete(oldE);
	    		}
	    		
			}
		//3.持久化新增的数据-班次时间
		for(ShiftEntity shift:shiftList){
			if(shift.getId()==null){
				//外键设置
				this.save(shift);
			}
		}
		
	}

	
	public void delMain(ShiftpatterntypeEntity shiftpatterntype) {
		//删除主表信息
		this.delete(shiftpatterntype);
		
		//===================================================================================
		//获取参数
		Object id = shiftpatterntype.getId();
		//===================================================================================
		//删除-班次时间
	    String hql0 = "from ShiftEntity where 1 = 1 and ShiftPatternType_ID = ?";
	    List<ShiftEntity> shiftOldList = this.findHql(hql0,id);
		this.deleteAllEntitie(shiftOldList);
	}
	
}