package org.jeecgframework.web.bussi.service.impl.editcalendar;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.editcalendar.ShiftServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("shiftService")
@Transactional
public class ShiftServiceImpl extends CommonServiceImpl implements ShiftServiceI {
	
}