package org.jeecgframework.web.bussi.service.editcalendar;

import java.util.List;
import org.jeecgframework.core.common.service.CommonService;
import org.jeecgframework.web.bussi.entity.editcalendar.ShiftpatterntypeEntity;
import org.jeecgframework.web.bussi.entity.editcalendar.ShiftEntity;

public interface ShiftpatterntypeServiceI extends CommonService{

	/**
	 * 添加一对多
	 * 
	 */
	public void addMain(ShiftpatterntypeEntity shiftpatterntype,
	        List<ShiftEntity> shiftList) ;
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(ShiftpatterntypeEntity shiftpatterntype,
	        List<ShiftEntity> shiftList);
	public void delMain (ShiftpatterntypeEntity shiftpatterntype);
}
