package org.jeecgframework.web.bussi.service.impl.makemonthplan;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.makemonthplan.JobprocessServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("jobprocessService")
@Transactional
public class JobprocessServiceImpl extends CommonServiceImpl implements JobprocessServiceI {
	
}