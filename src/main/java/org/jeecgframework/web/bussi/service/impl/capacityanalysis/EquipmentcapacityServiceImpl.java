package org.jeecgframework.web.bussi.service.impl.capacityanalysis;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.jeecgframework.web.bussi.service.capacityanalysis.EquipmentcapacityServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;

@Service("equipmentcapacityService")
@Transactional
public class EquipmentcapacityServiceImpl extends CommonServiceImpl implements EquipmentcapacityServiceI {
	
}