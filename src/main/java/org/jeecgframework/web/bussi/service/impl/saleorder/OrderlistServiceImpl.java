package org.jeecgframework.web.bussi.service.impl.saleorder;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import org.jeecgframework.web.bussi.service.saleorder.OrderlistServiceI;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.web.bussi.entity.saleorder.OrderlistEntity;
import org.jeecgframework.web.bussi.entity.saleorder.ProductionorderEntity;
@Service("orderlistService")
@Transactional
public class OrderlistServiceImpl extends CommonServiceImpl implements OrderlistServiceI {

	
	public void addMain(OrderlistEntity orderlist,
	        List<ProductionorderEntity> productionorderList){
			//保存主信息
			this.save(orderlist);
		
			/**保存-销售订单查询*/
			for(ProductionorderEntity productionorder:productionorderList){
				this.save(productionorder);
			}
	}

	
	public void updateMain(OrderlistEntity orderlist,
	        List<ProductionorderEntity> productionorderList) {
		//保存订单主信息
		this.saveOrUpdate(orderlist);
		
		//===================================================================================
		//获取参数
		//===================================================================================
		//1.查询出数据库的明细数据-销售订单查询
	    String hql0 = "from ProductionorderEntity where 1 = 1";
	    List<ProductionorderEntity> productionorderOldList = this.findHql(hql0);

	    //2.筛选更新明细数据-销售订单查询
		for(ProductionorderEntity oldE:productionorderOldList){
			boolean isUpdate = false;
				for(ProductionorderEntity sendE:productionorderList){
					//需要更新的明细数据-销售订单查询
					if(oldE.getId().equals(sendE.getId())){
		    			try {
							MyBeanUtils.copyBeanNotNull2Bean(sendE,oldE);
							this.saveOrUpdate(oldE);
						} catch (Exception e) {
							e.printStackTrace();
							throw new BusinessException(e.getMessage());
						}
						isUpdate= true;
		    			break;
		    		}
		    	}
	    		if(!isUpdate){
		    		//如果数据库存在的明细，前台没有传递过来则是删除-销售订单查询
		    		super.delete(oldE);
	    		}
	    		
			}
		//3.持久化新增的数据-销售订单查询
		for(ProductionorderEntity productionorder:productionorderList){
			if(productionorder.getId()==null){
				//外键设置
				this.save(productionorder);
			}
		}
	}

	
	public void delMain(OrderlistEntity orderlist) {
		//删除主表信息
		this.delete(orderlist);
		
		//===================================================================================
		//获取参数
		//===================================================================================
		//删除-销售订单查询
	    String hql0 = "from ProductionorderEntity where 1 = 1";
	    List<ProductionorderEntity> productionorderOldList = this.findHql(hql0);
		this.deleteAllEntitie(productionorderOldList);
}
}