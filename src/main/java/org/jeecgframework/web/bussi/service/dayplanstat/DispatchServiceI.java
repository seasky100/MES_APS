package org.jeecgframework.web.bussi.service.dayplanstat;

import java.util.List;
import org.jeecgframework.core.common.service.CommonService;
import org.jeecgframework.web.bussi.entity.dayplanstat.DispatchEntity;
import org.jeecgframework.web.bussi.entity.dayplanstat.DispatchorderEntity;

public interface DispatchServiceI extends CommonService{

	/**
	 * 添加一对多
	 * 
	 */
	public void addMain(DispatchEntity dispatch,
	        List<DispatchorderEntity> dispatchorderList) ;
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(DispatchEntity dispatch,
	        List<DispatchorderEntity> dispatchorderList);
	public void delMain (DispatchEntity dispatch);
}
