package org.jeecgframework.web.bussi.page.productionchange;

import java.util.List;
import java.util.ArrayList;
import org.jeecgframework.web.bussi.entity.productionchange.ChangelistEntity;

/**   
 * @Title: Entity
 * @Description: 生产变计划
 * @author zhangdaihao
 * @date 2015-08-01 13:08:27
 * @version V1.0   
 *
 */
@SuppressWarnings("serial")
public class ProductionchangePage implements java.io.Serializable {
	/**保存-变更单*/
	private List<ChangelistEntity> changelistList = new ArrayList<ChangelistEntity>();
	public List<ChangelistEntity> getChangelistList() {
		return changelistList;
	}
	public void setChangelistList(List<ChangelistEntity> changelistList) {
		this.changelistList = changelistList;
	}
}
