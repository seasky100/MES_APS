package org.jeecgframework.web.bussi.page.product;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

import org.jeecgframework.web.bussi.entity.product.ProcessEntity;

/**   
 * @Title: Entity
 * @Description: 工艺信息
 * @author zhangdaihao
 * @date 2015-06-07 17:26:48
 * @version V1.0   
 *
 */
@SuppressWarnings("serial")
public class CraftPage implements java.io.Serializable {
	/**保存-工序信息*/
	private List<ProcessEntity> processList = new ArrayList<ProcessEntity>();
	public List<ProcessEntity> getProcessList() {
		return processList;
	}
	public void setProcessList(List<ProcessEntity> processList) {
		this.processList = processList;
	}
}
