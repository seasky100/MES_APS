package org.jeecgframework.web.bussi.page.ordermain;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import org.jeecgframework.web.bussi.entity.workgroup.SystemuserEntity;

/**   
 * @Title: Entity
 * @Description: 班组信息主数据
 * @author zhangdaihao
 * @date 2015-05-31 20:56:12
 * @version V1.0   
 *
 */
@SuppressWarnings("serial")
public class WorkgroupPage implements java.io.Serializable {
	/**保存-班组用户信息*/
	private List<SystemuserEntity> systemUserList = new ArrayList<SystemuserEntity>();
	public List<SystemuserEntity> getSystemUserList() {
		return systemUserList;
	}

	public void setSystemUserList(List<SystemuserEntity> systemUserList) {
		this.systemUserList = systemUserList;
	}
}
