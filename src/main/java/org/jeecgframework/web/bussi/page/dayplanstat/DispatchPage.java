package org.jeecgframework.web.bussi.page.dayplanstat;

import java.util.List;
import java.util.ArrayList;

import org.jeecgframework.web.bussi.entity.dayplanstat.DispatchorderEntity;

/**   
 * @Title: Entity
 * @Description: 日生产计划
 * @author zhangdaihao
 * @date 2015-07-25 20:56:48
 * @version V1.0   
 *
 */
@SuppressWarnings("serial")
public class DispatchPage implements java.io.Serializable {
	/**保存-日派工单*/
	private List<DispatchorderEntity> dispatchorderList = new ArrayList<DispatchorderEntity>();
	public List<DispatchorderEntity> getDispatchorderList() {
		return dispatchorderList;
	}
	public void setDispatchorderList(List<DispatchorderEntity> dispatchorderList) {
		this.dispatchorderList = dispatchorderList;
	}
}
