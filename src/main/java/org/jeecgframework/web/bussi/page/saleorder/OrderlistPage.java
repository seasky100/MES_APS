package org.jeecgframework.web.bussi.page.saleorder;

import java.util.List;
import java.util.ArrayList;
import org.jeecgframework.web.bussi.entity.saleorder.ProductionorderEntity;

/**   
 * @Title: Entity
 * @Description: 销售订单录入转换
 * @author zhangdaihao
 * @date 2015-07-25 17:37:20
 * @version V1.0   
 *
 */
@SuppressWarnings("serial")
public class OrderlistPage implements java.io.Serializable {
	/**保存-销售订单查询*/
	private List<ProductionorderEntity> productionorderList = new ArrayList<ProductionorderEntity>();
	public List<ProductionorderEntity> getProductionorderList() {
		return productionorderList;
	}
	public void setProductionorderList(List<ProductionorderEntity> productionorderList) {
		this.productionorderList = productionorderList;
	}
}
