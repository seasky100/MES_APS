package org.jeecgframework.web.bussi.page.editcalendar;

import java.util.List;
import java.util.ArrayList;

import org.jeecgframework.web.bussi.entity.editcalendar.ShiftEntity;

/**   
 * @Title: Entity
 * @Description: 班次模式主数据
 * @author zhangdaihao
 * @date 2015-06-07 16:01:56
 * @version V1.0   
 *
 */
@SuppressWarnings("serial")
public class ShiftpatterntypePage implements java.io.Serializable {
	/**保存-班次时间*/
	private List<ShiftEntity> shiftList = new ArrayList<ShiftEntity>();

	public List<ShiftEntity> getShiftList() {
		return shiftList;
	}

	public void setShiftList(List<ShiftEntity> shiftList) {
		this.shiftList = shiftList;
	}
}
