package org.jeecgframework.web.bussi.controller.product;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.product.CraftEntity;
import org.jeecgframework.web.bussi.page.product.CraftPage;
import org.jeecgframework.web.bussi.service.product.CraftServiceI;
import org.jeecgframework.web.bussi.entity.product.ProcessEntity;
/**   
 * @Title: Controller
 * @Description: 工艺信息
 * @author zhangdaihao
 * @date 2015-06-07 17:26:47
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/craftController")
public class CraftController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CraftController.class);

	@Autowired
	private CraftServiceI craftService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 工艺信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "craft")
	public ModelAndView craft(HttpServletRequest request) {
		return new ModelAndView("bussi/product/craftList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(CraftEntity craft,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(CraftEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, craft);
		this.craftService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除工艺信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(CraftEntity craft, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		craft = systemService.getEntity(CraftEntity.class, craft.getId());
		message = "删除成功";
		craftService.delete(craft);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加工艺信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(CraftEntity craft,CraftPage craftPage, HttpServletRequest request) {
		List<ProcessEntity> processList =  craftPage.getProcessList();
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(craft.getId())) {
			message = "更新成功";
			craftService.updateMain(craft, processList);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} else {
			message = "添加成功";
			craftService.addMain(craft, processList);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 工艺信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(CraftEntity craft, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(craft.getId())) {
			craft = craftService.getEntity(CraftEntity.class, craft.getId());
			req.setAttribute("craftPage", craft);
		}
		return new ModelAndView("bussi/product/craft");
	}
	
	
	/**
	 * 加载明细列表[工序信息]
	 * 
	 * @return
	 */
	@RequestMapping(params = "processList")
	public ModelAndView processList(CraftEntity craft, HttpServletRequest req) {
	
		//===================================================================================
		//获取参数
		Object id0 = craft.getId();
		//===================================================================================
		//查询-工序信息
	    String hql0 = "from ProcessEntity where 1 = 1 AND craftId = ? ";
		try{
		    List<ProcessEntity> processEntityList = systemService.findHql(hql0,id0);
			req.setAttribute("processList", processEntityList);
		}catch(Exception e){
			logger.info(e.getMessage());
		}
		return new ModelAndView("bussi/product/processList");
	}
	
}
