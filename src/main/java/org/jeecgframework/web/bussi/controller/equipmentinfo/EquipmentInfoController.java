package org.jeecgframework.web.bussi.controller.equipmentinfo;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 设备信息查询
 * @author zhangdaihao
 * @date 2015-07-26 16:31:15
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/equipmentInfoController")
public class EquipmentInfoController extends BaseController {


	/**
	 * 设备信息 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "equipmentInfo")
	public ModelAndView plan(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/equipmentinfo/equipmentInfoList");
	}
}
