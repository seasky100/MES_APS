package org.jeecgframework.web.bussi.controller.editcalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.editcalendar.CalendarEntity;
import org.jeecgframework.web.bussi.service.editcalendar.CalendarServiceI;

/**   
 * @Title: Controller
 * @Description: 工厂日历创建
 * @author zhangdaihao
 * @date 2015-07-25 17:12:27
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/calendarController")
public class CalendarController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CalendarController.class);

	@Autowired
	private CalendarServiceI calendarService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 工厂日历创建列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "calendar")
	public ModelAndView calendar(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/editcalendar/calendarList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(CalendarEntity calendar,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(CalendarEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, calendar, request.getParameterMap());
		this.calendarService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除工厂日历创建
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(CalendarEntity calendar, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		calendar = systemService.getEntity(CalendarEntity.class, calendar.getId());
		message = "工厂日历创建删除成功";
		calendarService.delete(calendar);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加工厂日历创建
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(CalendarEntity calendar, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(calendar.getId())) {
			message = "工厂日历创建更新成功";
			CalendarEntity t = calendarService.get(CalendarEntity.class, calendar.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(calendar, t);
				calendarService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "工厂日历创建更新失败";
			}
		} else {
			message = "工厂日历创建添加成功";
			calendarService.save(calendar);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 工厂日历创建列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(CalendarEntity calendar, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(calendar.getId())) {
			calendar = calendarService.getEntity(CalendarEntity.class, calendar.getId());
			req.setAttribute("calendarPage", calendar);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/editcalendar/calendar");
	}
}
