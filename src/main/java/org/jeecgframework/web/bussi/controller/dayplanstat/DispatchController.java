package org.jeecgframework.web.bussi.controller.dayplanstat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.dayplanstat.DispatchEntity;
import org.jeecgframework.web.bussi.page.dayplanstat.DispatchPage;
import org.jeecgframework.web.bussi.service.dayplanstat.DispatchServiceI;
import org.jeecgframework.web.bussi.entity.dayplanstat.DispatchorderEntity;
/**   
 * @Title: Controller
 * @Description: 日生产计划
 * @author zhangdaihao
 * @date 2015-07-25 20:56:47
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/dispatchController")
public class DispatchController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DispatchController.class);

	@Autowired
	private DispatchServiceI dispatchService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 日生产计划执行统计列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "dispatch2")
	public ModelAndView dispatch2(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/dispatch2List");
	}
	/**
	 * 日生产计划列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "dispatch")
	public ModelAndView dispatch(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/dayplanstat/dispatchList");
	}
	/**
	 * 当日日生产计划 列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "daydispatch")
	public ModelAndView daydispatch(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/acquiredayplan/daydispatchList");
	}
	/**
	 * 待审批日生产计划 列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "dispatchpending")
	public ModelAndView dispatchpending(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/checkdayplan/dispatchpendingList");
	}
	/**
	 * 所有日生产计划查询 列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "alldispatch")
	public ModelAndView alldispatch(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/checkdayplan/alldispatchList");
	}
	/**
	 * 日生产计划查询列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "enquirydispatch")
	public ModelAndView enquirydispatch(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/enquiryplan/enquirydispatchList");
	}
	/**
	 * 日生产计划生成列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "generatedayplan")
	public ModelAndView generatedayplan(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/makedayplan/generatedayplanList");
	}
	
	/**
	 * 当日生产计划
	 * 
	 * @return
	 */
	@RequestMapping(params = "dispatchday")
	public ModelAndView dispatchday(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/dayplanAcqire/dispatchdayList");
	}
	
	/**
	 * 日生产计划列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdateday")
	public ModelAndView addorupdateday(DispatchEntity dispatch, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dispatch.getId())) {
			dispatch = dispatchService.getEntity(DispatchEntity.class, dispatch.getId());
			req.setAttribute("dispatchPage", dispatch);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/dayplanAcqire/dispatchday");
	}
	
	/**
	 * 所有日生产计划
	 * 
	 * @return
	 */
	@RequestMapping(params = "dispatchall")
	public ModelAndView dispatchall(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/dayplanAcqire/dispatchallList");
	}
	
	/**
	 * 日生产计划列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdateall")
	public ModelAndView addorupdateall(DispatchEntity dispatch, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dispatch.getId())) {
			dispatch = dispatchService.getEntity(DispatchEntity.class, dispatch.getId());
			req.setAttribute("dispatchPage", dispatch);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/dayplanAcqire/dispatchall");
	}
	
	
	
	/**
	 * 当日生产计划
	 * 
	 * @return
	 */
	@RequestMapping(params = "filldispatchday")
	public ModelAndView filldispatchday(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/filldispatch/dispatchdayList");
	}
	
	/**
	 * 日生产计划列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdateday")
	public ModelAndView filladdorupdateday(DispatchEntity dispatch, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dispatch.getId())) {
			dispatch = dispatchService.getEntity(DispatchEntity.class, dispatch.getId());
			req.setAttribute("dispatchPage", dispatch);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/filldispatch/dispatchday");
	}
	
	/**
	 * 所有日生产计划
	 * 
	 * @return
	 */
	@RequestMapping(params = "dispatchall")
	public ModelAndView filldispatchall(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/filldispatch/dispatchallList");
	}
	
	/**
	 * 日生产计划列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdateall")
	public ModelAndView filladdorupdateall(DispatchEntity dispatch, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dispatch.getId())) {
			dispatch = dispatchService.getEntity(DispatchEntity.class, dispatch.getId());
			req.setAttribute("dispatchPage", dispatch);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/filldispatch/dispatchall");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(DispatchEntity dispatch,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(DispatchEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, dispatch);
		this.dispatchService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除日生产计划
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(DispatchEntity dispatch, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		dispatch = systemService.getEntity(DispatchEntity.class, dispatch.getId());
		message = "删除成功";
		dispatchService.delete(dispatch);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加日生产计划
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(DispatchEntity dispatch,DispatchPage dispatchPage, HttpServletRequest request) {
		List<DispatchorderEntity> dispatchorderList =  dispatchPage.getDispatchorderList();
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(dispatch.getId())) {
			message = "更新成功";
			dispatchService.updateMain(dispatch, dispatchorderList);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} else {
			message = "添加成功";
			dispatchService.addMain(dispatch, dispatchorderList);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 日生产计划列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(DispatchEntity dispatch, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dispatch.getId())) {
			dispatch = dispatchService.getEntity(DispatchEntity.class, dispatch.getId());
			req.setAttribute("dispatchPage", dispatch);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/dayplanstat/dispatch");
	}
	
	
	/**
	 * 加载明细列表[日派工单]
	 * 
	 * @return
	 */
	@RequestMapping(params = "dispatchorderList")
	public ModelAndView dispatchorderList(DispatchEntity dispatch, HttpServletRequest req) {
	
		//===================================================================================
		//获取参数
		Object id = dispatch.getId();
		//===================================================================================
		//查询-日派工单
	    String hql0 = "from DispatchorderEntity where 1 = 1 and dispatch_ID = ?";
		try{
		    List<DispatchorderEntity> dispatchorderEntityList = systemService.findHql(hql0,id);
			req.setAttribute("dispatchorderList", dispatchorderEntityList);
		}catch(Exception e){
			logger.info(e.getMessage());
		}
		return new ModelAndView("org/jeecgframework/web/bussi/dayplanstat/dispatchorderList");
	}
	
	
}
