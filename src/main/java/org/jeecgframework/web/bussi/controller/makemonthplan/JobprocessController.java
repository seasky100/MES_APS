package org.jeecgframework.web.bussi.controller.makemonthplan;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.makemonthplan.JobprocessEntity;
import org.jeecgframework.web.bussi.service.makemonthplan.JobprocessServiceI;

/**   
 * @Title: Controller
 * @Description: 排产结果
 * @author zhangdaihao
 * @date 2015-07-26 16:34:17
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/jobprocessController")
public class JobprocessController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobprocessController.class);

	@Autowired
	private JobprocessServiceI jobprocessService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 排产结果列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "jobprocess")
	public ModelAndView jobprocess(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/makemonthplan/jobprocessList");
	}
	/**
	 * 滚动生产计划统计 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "countjobprocess")
	public ModelAndView countjobprocess(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/planscheduleview/countjobprocessList");
	}
	/**
	 * 冻结/解冻 已有任务安排列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "jobprocesslock")
	public ModelAndView jobprocesslock(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/makemonthplan/jobprocesslockList");
	}
	/**
	 * 日生产指令单获取 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "getjobprocess")
	public ModelAndView getjobprocess(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/makedayplan/getjobprocessList");
	}
	/**
	 * 滚动生产计划查询  页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "enquiryjobprocess")
	public ModelAndView enquiryjobprocess(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/enquiryplan/enquiryjobprocessList");
	}
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(JobprocessEntity jobprocess,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(JobprocessEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, jobprocess, request.getParameterMap());
		this.jobprocessService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除排产结果
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(JobprocessEntity jobprocess, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		jobprocess = systemService.getEntity(JobprocessEntity.class, jobprocess.getId());
		message = "排产结果删除成功";
		jobprocessService.delete(jobprocess);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加排产结果
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(JobprocessEntity jobprocess, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(jobprocess.getId())) {
			message = "排产结果更新成功";
			JobprocessEntity t = jobprocessService.get(JobprocessEntity.class, jobprocess.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(jobprocess, t);
				jobprocessService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "排产结果更新失败";
			}
		} else {
			message = "排产结果添加成功";
			jobprocessService.save(jobprocess);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 排产结果列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(JobprocessEntity jobprocess, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(jobprocess.getId())) {
			jobprocess = jobprocessService.getEntity(JobprocessEntity.class, jobprocess.getId());
			req.setAttribute("jobprocessPage", jobprocess);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/makemonthplan/jobprocess");
	}
}
