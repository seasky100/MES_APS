package org.jeecgframework.web.bussi.controller.changemanage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.changemanage.DynamiceventsEntity;
import org.jeecgframework.web.bussi.service.changemanage.DynamiceventsServiceI;

/**   
 * @Title: Controller
 * @Description: 动态事件
 * @author zhangdaihao
 * @date 2015-08-01 13:25:21
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/dynamiceventsController")
public class DynamiceventsController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DynamiceventsController.class);

	@Autowired
	private DynamiceventsServiceI dynamiceventsService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 动态事件列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "dynamicevents")
	public ModelAndView dynamicevents(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/changemanage/dynamiceventsList");
	}
	
	/**
	 * 紧急订单
	 * 
	 * @return
	 */
	@RequestMapping(params = "swiftorder")
	public ModelAndView swiftOrder(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/editdynamicevents/swiftOrderList");
	}
	
	/**
	 * 紧急订单页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdateswiftorder")
	public ModelAndView addorupdateswiftorder(DynamiceventsEntity dynamicevents, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dynamicevents.getId())) {
			dynamicevents = dynamiceventsService.getEntity(DynamiceventsEntity.class, dynamicevents.getId());
			req.setAttribute("dynamiceventsPage", dynamicevents);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/editdynamicevents/swiftOrder");
	}
	
	/**
	 * 任务拖期
	 * 
	 * @return
	 */
	@RequestMapping(params = "jobdelay")
	public ModelAndView jobDelay(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/editdynamicevents/jobDelayList");
	}
	
	/**
	 * 任务拖期页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdatejobdelay")
	public ModelAndView addorupdatejobdelay(DynamiceventsEntity dynamicevents, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dynamicevents.getId())) {
			dynamicevents = dynamiceventsService.getEntity(DynamiceventsEntity.class, dynamicevents.getId());
			req.setAttribute("dynamiceventsPage", dynamicevents);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/editdynamicevents/jobDelay");
	}
	
	/**
	 * 动态事件查询
	 * 
	 * @return
	 */
	@RequestMapping(params = "query")
	public ModelAndView queryEvents(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/editdynamicevents/queryEventsList");
	}
	
	/**
	 * 动态事件查询页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdatequery")
	public ModelAndView addorupdatequery(DynamiceventsEntity dynamicevents, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dynamicevents.getId())) {
			dynamicevents = dynamiceventsService.getEntity(DynamiceventsEntity.class, dynamicevents.getId());
			req.setAttribute("dynamiceventsPage", dynamicevents);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/editdynamicevents/queryEvents");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(DynamiceventsEntity dynamicevents,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(DynamiceventsEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, dynamicevents, request.getParameterMap());
		this.dynamiceventsService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除动态事件
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(DynamiceventsEntity dynamicevents, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		dynamicevents = systemService.getEntity(DynamiceventsEntity.class, dynamicevents.getId());
		message = "动态事件删除成功";
		dynamiceventsService.delete(dynamicevents);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加动态事件
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(DynamiceventsEntity dynamicevents, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(dynamicevents.getId())) {
			message = "动态事件更新成功";
			DynamiceventsEntity t = dynamiceventsService.get(DynamiceventsEntity.class, dynamicevents.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(dynamicevents, t);
				dynamiceventsService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "动态事件更新失败";
			}
		} else {
			message = "动态事件添加成功";
			dynamiceventsService.save(dynamicevents);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 动态事件列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(DynamiceventsEntity dynamicevents, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dynamicevents.getId())) {
			dynamicevents = dynamiceventsService.getEntity(DynamiceventsEntity.class, dynamicevents.getId());
			req.setAttribute("dynamiceventsPage", dynamicevents);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/changemanage/dynamicevents");
	}
}
