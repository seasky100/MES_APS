package org.jeecgframework.web.bussi.controller.monthplanstat;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 月生产计划执行统计
 * @author zhangdaihao
 * @date 2015-07-25 20:36:00
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/monthPlanStatController")
public class MonthPlanStatController extends BaseController {


	/**
	 * 与生产计划统计 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "monthplanstat")
	public ModelAndView equipmentcapacity(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/monthplanstat/monthplanstat");
	}
}
