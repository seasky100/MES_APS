package org.jeecgframework.web.bussi.controller.editcalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.web.bussi.entity.editcalendar.ShiftpatterntypeEntity;
import org.jeecgframework.web.bussi.page.editcalendar.ShiftpatterntypePage;
import org.jeecgframework.web.bussi.service.editcalendar.ShiftpatterntypeServiceI;
import org.jeecgframework.web.bussi.entity.editcalendar.ShiftEntity;
/**   
 * @Title: Controller
 * @Description: 班次模式
 * @author zhangdaihao
 * @date 2015-07-25 17:24:49
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/shiftpatterntypeController")
public class ShiftpatterntypeController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ShiftpatterntypeController.class);

	@Autowired
	private ShiftpatterntypeServiceI shiftpatterntypeService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 班次模式列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "shiftpatterntype")
	public ModelAndView shiftpatterntype(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/editcalendar/shiftpatterntypeList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(ShiftpatterntypeEntity shiftpatterntype,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ShiftpatterntypeEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, shiftpatterntype);
		this.shiftpatterntypeService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除班次模式
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(ShiftpatterntypeEntity shiftpatterntype, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		shiftpatterntype = systemService.getEntity(ShiftpatterntypeEntity.class, shiftpatterntype.getId());
		message = "删除成功";
		shiftpatterntypeService.delete(shiftpatterntype);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加班次模式
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(ShiftpatterntypeEntity shiftpatterntype,ShiftpatterntypePage shiftpatterntypePage, HttpServletRequest request) {
		List<ShiftEntity> shiftList =  shiftpatterntypePage.getShiftList();
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(shiftpatterntype.getId())) {
			message = "更新成功";
			shiftpatterntypeService.updateMain(shiftpatterntype, shiftList);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} else {
			message = "添加成功";
			shiftpatterntypeService.addMain(shiftpatterntype, shiftList);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 班次模式列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(ShiftpatterntypeEntity shiftpatterntype, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(shiftpatterntype.getId())) {
			shiftpatterntype = shiftpatterntypeService.getEntity(ShiftpatterntypeEntity.class, shiftpatterntype.getId());
			req.setAttribute("shiftpatterntypePage", shiftpatterntype);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/editcalendar/shiftpatterntype");
	}
	
	
	/**
	 * 加载明细列表[班次时间]
	 * 
	 * @return
	 */
	@RequestMapping(params = "shiftList")
	public ModelAndView shiftList(ShiftpatterntypeEntity shiftpatterntype, HttpServletRequest req) {
	
		//===================================================================================
		//获取参数
		Object id = shiftpatterntype.getId();
		//===================================================================================
		//查询-班次时间
	    String hql0 = "from ShiftEntity where 1 = 1 and ShiftPatternType_ID = ?";
		try{
		    List<ShiftEntity> shiftEntityList = systemService.findHql(hql0,id);
			req.setAttribute("shiftList", shiftEntityList);
		}catch(Exception e){
			logger.info(e.getMessage());
		}
		return new ModelAndView("org/jeecgframework/web/bussi/editcalendar/shiftList");
	}
	
}
