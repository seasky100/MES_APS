package org.jeecgframework.web.bussi.controller.confirmproductchange;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 生产变更单确认
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/confirmproductchangeController")
public class confirmproductchangeController extends BaseController {
	/**
	 * 生产变更单确认 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "confirmproductchange")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/confirmproductchange/confirmproductchange");
	}
}