package org.jeecgframework.web.bussi.controller.editdynamicevents;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 动态事件管理页面
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/editdynamiceventsController")
public class EditdynamiceventsController extends BaseController {
	/**
	 * 动态事件管理跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "editdynamicevents")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/editdynamicevents/editdynamicevents");
	}
}