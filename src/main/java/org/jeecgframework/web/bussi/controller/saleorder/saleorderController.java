package org.jeecgframework.web.bussi.controller.saleorder;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 销售订单录入与转换
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/saleorderController")
public class saleorderController extends BaseController {
	/**
	 * 销售订单录入与转换页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "saleorder")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/saleorder/saleorder");
	}
}