package org.jeecgframework.web.bussi.controller.productorder;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 生产订单管理
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/productorderController")
public class productorderController extends BaseController {
	/**
	 * 生产订单管理 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "productorder")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/productorder/productorder");
	}
}