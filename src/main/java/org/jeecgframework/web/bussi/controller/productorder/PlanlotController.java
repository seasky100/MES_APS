package org.jeecgframework.web.bussi.controller.productorder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.productorder.PlanlotEntity;
import org.jeecgframework.web.bussi.service.productorder.PlanlotServiceI;

/**   
 * @Title: Controller
 * @Description: 生产订单管理
 * @author zhangdaihao
 * @date 2015-07-26 15:05:05
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/planlotController")
public class PlanlotController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PlanlotController.class);

	@Autowired
	private PlanlotServiceI planlotService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 生产订单管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "planlot")
	public ModelAndView planlot(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/productorder/planlotList");
	}
	/**
	 * 月生产计划制定 列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "setplanlot")
	public ModelAndView setplanlot(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/makemonthplan/setplanlotList");
	}
	/**
	 * 生产订单查询列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "planlotquery")
	public ModelAndView planlotquery(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/productorder/planlotqueryList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(PlanlotEntity planlot,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(PlanlotEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, planlot);
		this.planlotService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除生产订单管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(PlanlotEntity planlot, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		planlot = systemService.getEntity(PlanlotEntity.class, planlot.getId());
		message = "生产订单管理删除成功";
		planlotService.delete(planlot);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加生产订单管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(PlanlotEntity planlot, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(planlot.getId())) {
			message = "生产订单管理更新成功";
			PlanlotEntity t = planlotService.get(PlanlotEntity.class, planlot.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(planlot, t);
				planlotService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "生产订单管理更新失败";
			}
		} else {
			message = "生产订单管理添加成功";
			planlotService.save(planlot);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 生产订单管理列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(PlanlotEntity planlot, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(planlot.getId())) {
			planlot = planlotService.getEntity(PlanlotEntity.class, planlot.getId());
			req.setAttribute("planlotPage", planlot);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/productorder/planlot");
	}

	public static Logger getLogger() {
		return logger;
	}
}
