package org.jeecgframework.web.bussi.controller.product;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.product.MaterialEntity;
import org.jeecgframework.web.bussi.service.product.MaterialServiceI;

/**   
 * @Title: Controller
 * @Description: 物品信息
 * @author zhangdaihao
 * @date 2015-06-07 17:04:46
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/materialController")
public class MaterialController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MaterialController.class);

	@Autowired
	private MaterialServiceI materialService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 物品信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "material")
	public ModelAndView material(HttpServletRequest request) {
		return new ModelAndView("bussi/product/materialList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(MaterialEntity material,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(MaterialEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, material, request.getParameterMap());
		this.materialService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除物品信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(MaterialEntity material, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		material = systemService.getEntity(MaterialEntity.class, material.getId());
		message = "物品信息删除成功";
		materialService.delete(material);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加物品信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(MaterialEntity material, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(material.getId())) {
			message = "物品信息更新成功";
			MaterialEntity t = materialService.get(MaterialEntity.class, material.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(material, t);
				materialService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "物品信息更新失败";
			}
		} else {
			message = "物品信息添加成功";
			materialService.save(material);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 物品信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(MaterialEntity material, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(material.getId())) {
			material = materialService.getEntity(MaterialEntity.class, material.getId());
			req.setAttribute("materialPage", material);
		}
		return new ModelAndView("bussi/product/material");
	}
}
