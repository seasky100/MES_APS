package org.jeecgframework.web.bussi.controller.planscheduleview;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 生产执行结果统计
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/planscheduleviewController")
public class planscheduleviewController extends BaseController {
	/**
	 * 生产执行结果统计 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "planscheduleview")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/planscheduleview/planscheduleview");
	}
}