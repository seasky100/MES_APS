package org.jeecgframework.web.bussi.controller.workgroup;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.web.bussi.entity.workgroup.SystemuserEntity;
import org.jeecgframework.web.bussi.entity.workgroup.WorkgroupEntity;
import org.jeecgframework.web.bussi.service.workgroup.WorkgroupServiceI;

/**   
 * @Title: Controller
 * @Description: 班组信息
 * @author zhangdaihao
 * @date 2015-05-31 20:47:28
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/workgroupController")
public class WorkgroupController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WorkgroupController.class);

	@Autowired
	private WorkgroupServiceI workgroupService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 班组信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "workgroup")
	public ModelAndView workgroup(HttpServletRequest request) {
		return new ModelAndView("bussi/workgroup/workgroupList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(WorkgroupEntity workgroup,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(WorkgroupEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, workgroup, request.getParameterMap());
		this.workgroupService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除班组信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(WorkgroupEntity workgroup, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		workgroup = systemService.getEntity(WorkgroupEntity.class, workgroup.getId());
		message = "班组信息删除成功";
		workgroupService.delete(workgroup);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加班组信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(WorkgroupEntity workgroup, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(workgroup.getId())) {
			message = "班组信息更新成功";
			WorkgroupEntity t = workgroupService.get(WorkgroupEntity.class, workgroup.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(workgroup, t);
				workgroupService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "班组信息更新失败";
			}
		} else {
			message = "班组信息添加成功";
			workgroupService.save(workgroup);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 班组信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(WorkgroupEntity workgroup, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(workgroup.getId())) {
			workgroup = workgroupService.getEntity(WorkgroupEntity.class, workgroup.getId());
			req.setAttribute("workgroupPage", workgroup);
		}
		return new ModelAndView("bussi/workgroup/workgroup");
	}
	
	/**
	 * 加载明细列表[班组用户信息]
	 * 
	 * @return
	 */
	@RequestMapping(params = "sysUserList")
	public ModelAndView sysUserList(WorkgroupEntity workgroup, HttpServletRequest req) {
	
		//===================================================================================
		//获取参数
		Object id0 = workgroup.getId();
		//===================================================================================
		//查询-订单客户明细
		//String hql0 = "from SystemuserEntity where 1 = 1 AND id = ?  AND goOrderCode = ? ";
		String hql0 = "from SystemuserEntity where id in (select ww.systemuserid from WorkgroupWorkerEntity ww where ww.workgroupid = ?)";
		try{
		    List<SystemuserEntity> SystemuserEntityList = systemService.findHql(hql0, id0);
			req.setAttribute("sysUserList", SystemuserEntityList);
		}catch(Exception e){
			logger.info(e.getMessage());
		}
		return new ModelAndView("bussi/workgroup/systemuserList");
	}
}
