package org.jeecgframework.web.bussi.controller.workgroup;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.web.bussi.entity.workgroup.SystemuserEntity;
import org.jeecgframework.web.bussi.entity.workgroup.SystemuserEntity;
import org.jeecgframework.web.bussi.service.workgroup.SystemuserServiceI;

/**   
 * @Title: Controller
 * @Description: 员工信息
 * @author zhangdaihao
 * @date 2015-05-31 20:50:01
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/systemuserController")
public class SystemuserController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SystemuserController.class);

	@Autowired
	private SystemuserServiceI systemuserService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 员工信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "systemuser")
	public ModelAndView systemuser(HttpServletRequest request) {
		return new ModelAndView("bussi/workgroup/systemuserList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(SystemuserEntity systemuser,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(SystemuserEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, systemuser, request.getParameterMap());
		this.systemuserService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除员工信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(SystemuserEntity systemuser, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		systemuser = systemService.getEntity(SystemuserEntity.class, systemuser.getId());
		message = "员工信息删除成功";
		systemuserService.delete(systemuser);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加员工信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(SystemuserEntity systemuser, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(systemuser.getId())) {
			message = "员工信息更新成功";
			SystemuserEntity t = systemuserService.get(SystemuserEntity.class, systemuser.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(systemuser, t);
				systemuserService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "员工信息更新失败";
			}
		} else {
			message = "员工信息添加成功";
			systemuserService.save(systemuser);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 员工信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(SystemuserEntity systemuser, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(systemuser.getId())) {
			systemuser = systemuserService.getEntity(SystemuserEntity.class, systemuser.getId());
			req.setAttribute("systemuserPage", systemuser);
		}
		return new ModelAndView("bussi/workgroup/systemuser");
	}
}
