package org.jeecgframework.web.bussi.controller.productionchange;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.web.bussi.entity.productionchange.ProductionchangeEntity;
import org.jeecgframework.web.bussi.page.productionchange.ProductionchangePage;
import org.jeecgframework.web.bussi.service.productionchange.ProductionchangeServiceI;
import org.jeecgframework.web.bussi.entity.productionchange.ChangelistEntity;
/**   
 * @Title: Controller
 * @Description: 生产变计划
 * @author zhangdaihao
 * @date 2015-08-01 13:08:27
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/productionchangeController")
public class ProductionchangeController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductionchangeController.class);

	@Autowired
	private ProductionchangeServiceI productionchangeService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 生产变计划列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "productionchange")
	public ModelAndView productionchange(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/productionchange/productionchangeList");
	}
	/**
	 * 待审批生产变更单 列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "productionchangepending")
	public ModelAndView productionchangepending(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/checkproductchange/productionchangependingList");
	}
	/**
	 * 所有生产变更单查询 列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "allproductionchange")
	public ModelAndView allproductionchange(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/checkproductchange/allproductionchangeList");
	}
	/**
	 * 生产变计划列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "changemanage")
	public ModelAndView changemanage(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/changemanage/productionchangeList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(ProductionchangeEntity productionchange,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ProductionchangeEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, productionchange);
		this.productionchangeService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除生产变计划
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(ProductionchangeEntity productionchange, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		productionchange = systemService.getEntity(ProductionchangeEntity.class, productionchange.getId());
		message = "删除成功";
		productionchangeService.delete(productionchange);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加生产变计划
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(ProductionchangeEntity productionchange,ProductionchangePage productionchangePage, HttpServletRequest request) {
		List<ChangelistEntity> changelistList =  productionchangePage.getChangelistList();
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(productionchange.getId())) {
			message = "更新成功";
			productionchangeService.updateMain(productionchange, changelistList);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} else {
			message = "添加成功";
			productionchangeService.addMain(productionchange, changelistList);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 生产变计划列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(ProductionchangeEntity productionchange, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(productionchange.getId())) {
			productionchange = productionchangeService.getEntity(ProductionchangeEntity.class, productionchange.getId());
			req.setAttribute("productionchangePage", productionchange);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/productionchange/productionchange");
	}
	
	
	/**
	 * 加载明细列表[变更单]
	 * 
	 * @return
	 */
	@RequestMapping(params = "changelistList")
	public ModelAndView changelistList(ProductionchangeEntity productionchange, HttpServletRequest req) {
	
		//===================================================================================
		//获取参数
		Object id = productionchange.getId();
		//===================================================================================
		//查询-变更单
	    String hql0 = "from ChangelistEntity where 1 = 1 and Productionchange_ID = ?";
		try{
		    List<ChangelistEntity> changelistEntityList = systemService.findHql(hql0,id);
			req.setAttribute("changelistList", changelistEntityList);
		}catch(Exception e){
			logger.info(e.getMessage());
		}
		return new ModelAndView("org/jeecgframework/web/bussi/productionchange/changelistList");
	}
	
	
}
