package org.jeecgframework.web.bussi.controller.equipmenttype;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.equipmenttype.EquipmentTypeEntity;
import org.jeecgframework.web.bussi.service.equipmenttype.EquipmentTypeServiceI;

/**   
 * @Title: Controller
 * @Description: 加工区信息
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/equipmentTypeController")
public class EquipmentTypeController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EquipmentTypeController.class);

	@Autowired
	private EquipmentTypeServiceI equipmentTypeService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 加工区信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "equipmentType")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/equipmenttype/equipmentTypeList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(EquipmentTypeEntity equipmentType,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(EquipmentTypeEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, equipmentType, request.getParameterMap());
		this.equipmentTypeService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除加工区信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(EquipmentTypeEntity equipmentType, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		equipmentType = systemService.getEntity(EquipmentTypeEntity.class, equipmentType.getId());
		message = "加工区信息删除成功";
		equipmentTypeService.delete(equipmentType);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加加工区信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(EquipmentTypeEntity equipmentType, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(equipmentType.getId())) {
			message = "加工区信息更新成功";
			EquipmentTypeEntity t = equipmentTypeService.get(EquipmentTypeEntity.class, equipmentType.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(equipmentType, t);
				equipmentTypeService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "加工区信息更新失败";
			}
		} else {
			message = "加工区信息添加成功";
			equipmentTypeService.save(equipmentType);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 加工区信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(EquipmentTypeEntity equipmentType, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(equipmentType.getId())) {
			equipmentType = equipmentTypeService.getEntity(EquipmentTypeEntity.class, equipmentType.getId());
			req.setAttribute("equipmentTypePage", equipmentType);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/equipmenttype/equipmentType");
	}
}
