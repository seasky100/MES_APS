package org.jeecgframework.web.bussi.controller.equipmentset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.equipmentset.EquipmentsetEntity;
import org.jeecgframework.web.bussi.service.equipmentset.EquipmentsetServiceI;

/**   
 * @Title: Controller
 * @Description: 车间信息
 * @author zhangdaihao
 * @date 2015-07-25 15:50:02
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/equipmentsetController")
public class EquipmentsetController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EquipmentsetController.class);

	@Autowired
	private EquipmentsetServiceI equipmentsetService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 车间信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "equipmentset")
	public ModelAndView equipmentset(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/equipmentset/equipmentsetList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(EquipmentsetEntity equipmentset,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(EquipmentsetEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, equipmentset, request.getParameterMap());
		this.equipmentsetService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除车间信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(EquipmentsetEntity equipmentset, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		equipmentset = systemService.getEntity(EquipmentsetEntity.class, equipmentset.getId());
		message = "车间信息删除成功";
		equipmentsetService.delete(equipmentset);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加车间信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(EquipmentsetEntity equipmentset, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(equipmentset.getId())) {
			message = "车间信息更新成功";
			EquipmentsetEntity t = equipmentsetService.get(EquipmentsetEntity.class, equipmentset.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(equipmentset, t);
				equipmentsetService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "车间信息更新失败";
			}
		} else {
			message = "车间信息添加成功";
			equipmentsetService.save(equipmentset);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 车间信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(EquipmentsetEntity equipmentset, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(equipmentset.getId())) {
			equipmentset = equipmentsetService.getEntity(EquipmentsetEntity.class, equipmentset.getId());
			req.setAttribute("equipmentsetPage", equipmentset);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/equipmentset/equipmentset");
	}
}
