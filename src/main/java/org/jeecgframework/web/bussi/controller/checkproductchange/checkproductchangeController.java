package org.jeecgframework.web.bussi.controller.checkproductchange;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 月生产计划审批
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/checkproductchangeController")
public class checkproductchangeController extends BaseController {
	/**
	 * 月生产计划审批 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "checkproductchange")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/checkproductchange/checkproductchange");
	}
}