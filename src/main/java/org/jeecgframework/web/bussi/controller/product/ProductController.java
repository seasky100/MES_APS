package org.jeecgframework.web.bussi.controller.product;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 物品信息
 * @author zhangdaihao
 * @date 2015-05-18 22:02:12
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/productController")
public class ProductController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductController.class);

	/**
	 * 物品信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "product")
	public ModelAndView person(HttpServletRequest request) {
		return new ModelAndView("bussi/product/productList");
	}
}
