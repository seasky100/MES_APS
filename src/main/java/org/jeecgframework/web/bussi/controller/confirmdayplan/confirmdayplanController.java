package org.jeecgframework.web.bussi.controller.confirmdayplan;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 日生产计划确认
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/confirmdayplanController")
public class confirmdayplanController extends BaseController {
	/**
	 * 日生产计划确认 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "confirmdayplan")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/confirmdayplan/confirmdayplan");
	}
}