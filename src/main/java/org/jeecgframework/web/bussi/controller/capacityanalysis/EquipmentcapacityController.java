package org.jeecgframework.web.bussi.controller.capacityanalysis;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.capacityanalysis.EquipmentcapacityEntity;
import org.jeecgframework.web.bussi.service.capacityanalysis.EquipmentcapacityServiceI;

/**   
 * @Title: Controller
 * @Description: 产能负荷表
 * @author zhangdaihao
 * @date 2015-07-25 20:36:00
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/equipmentcapacityController")
public class EquipmentcapacityController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EquipmentcapacityController.class);

	@Autowired
	private EquipmentcapacityServiceI equipmentcapacityService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 产能负荷表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "equipmentcapacity")
	public ModelAndView equipmentcapacity(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/capacityanalysis/equipmentcapacityList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(EquipmentcapacityEntity equipmentcapacity,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(EquipmentcapacityEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, equipmentcapacity, request.getParameterMap());
		this.equipmentcapacityService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除产能负荷表
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(EquipmentcapacityEntity equipmentcapacity, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		equipmentcapacity = systemService.getEntity(EquipmentcapacityEntity.class, equipmentcapacity.getId());
		message = "产能负荷表删除成功";
		equipmentcapacityService.delete(equipmentcapacity);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加产能负荷表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(EquipmentcapacityEntity equipmentcapacity, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(equipmentcapacity.getId())) {
			message = "产能负荷表更新成功";
			EquipmentcapacityEntity t = equipmentcapacityService.get(EquipmentcapacityEntity.class, equipmentcapacity.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(equipmentcapacity, t);
				equipmentcapacityService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "产能负荷表更新失败";
			}
		} else {
			message = "产能负荷表添加成功";
			equipmentcapacityService.save(equipmentcapacity);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 产能负荷表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(EquipmentcapacityEntity equipmentcapacity, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(equipmentcapacity.getId())) {
			equipmentcapacity = equipmentcapacityService.getEntity(EquipmentcapacityEntity.class, equipmentcapacity.getId());
			req.setAttribute("equipmentcapacityPage", equipmentcapacity);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/capacityanalysis/equipmentcapacity");
	}
}
