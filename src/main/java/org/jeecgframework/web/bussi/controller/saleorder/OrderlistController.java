package org.jeecgframework.web.bussi.controller.saleorder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.service.SystemService;

import org.jeecgframework.web.bussi.entity.saleorder.OrderlistEntity;
import org.jeecgframework.web.bussi.page.saleorder.OrderlistPage;
import org.jeecgframework.web.bussi.service.saleorder.OrderlistServiceI;
import org.jeecgframework.web.bussi.entity.saleorder.ProductionorderEntity;
/**   
 * @Title: Controller
 * @Description: 销售订单录入转换
 * @author zhangdaihao
 * @date 2015-07-25 17:37:19
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/orderlistController")
public class OrderlistController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderlistController.class);

	@Autowired
	private OrderlistServiceI orderlistService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 销售订单录入转换列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "orderlist")
	public ModelAndView orderlist(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/saleorder/orderlistList");
	}

	/**
	 * 销售订单导入列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "orderlistimport")
	public ModelAndView orderlistimport(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/saleorder/orderlistimportList");
	}
	/**
	 * 新增销售订单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorderlist")
	public ModelAndView addorderlist(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/saleorder/addorderlistList");
	}
	/**
	 * 销售订单查询列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "orderlistquery")
	public ModelAndView orderlistquery(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/saleorder/orderlistqueryList");
	}
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(OrderlistEntity orderlist,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(OrderlistEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderlist);
		this.orderlistService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除销售订单录入转换
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(OrderlistEntity orderlist, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		orderlist = systemService.getEntity(OrderlistEntity.class, orderlist.getId());
		message = "删除成功";
		orderlistService.delete(orderlist);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加销售订单录入转换
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(OrderlistEntity orderlist,OrderlistPage orderlistPage, HttpServletRequest request) {
		List<ProductionorderEntity> productionorderList =  orderlistPage.getProductionorderList();
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(orderlist.getId())) {
			message = "更新成功";
			orderlistService.updateMain(orderlist, productionorderList);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} else {
			message = "添加成功";
			orderlistService.addMain(orderlist, productionorderList);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 销售订单录入转换列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(OrderlistEntity orderlist, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(orderlist.getId())) {
			orderlist = orderlistService.getEntity(OrderlistEntity.class, orderlist.getId());
			req.setAttribute("orderlistPage", orderlist);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/saleorder/orderlist");
	}
	
	
	/**
	 * 加载明细列表[销售订单查询]
	 * 
	 * @return
	 */
	@RequestMapping(params = "productionorderList")
	public ModelAndView productionorderList(OrderlistEntity orderlist, HttpServletRequest req) {
	
		//===================================================================================
		//获取参数
		//===================================================================================
		//查询-销售订单查询
	    String hql0 = "from ProductionorderEntity where 1 = 1";
		try{
		    List<ProductionorderEntity> productionorderEntityList = systemService.findHql(hql0);
			req.setAttribute("productionorderList", productionorderEntityList);
		}catch(Exception e){
			logger.info(e.getMessage());
		}
		return new ModelAndView("org/jeecgframework/web/bussi/saleorder/productionorderList");
	}
	
}
