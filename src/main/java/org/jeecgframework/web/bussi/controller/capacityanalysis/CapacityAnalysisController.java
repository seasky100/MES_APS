package org.jeecgframework.web.bussi.controller.capacityanalysis;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 工厂日历页面
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/capacityAnalysisController")
public class CapacityAnalysisController extends BaseController {
	/**
	 * 工厂日历信息页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "capacityanalysis")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/capacityanalysis/capacityAnalysis");
	}
}