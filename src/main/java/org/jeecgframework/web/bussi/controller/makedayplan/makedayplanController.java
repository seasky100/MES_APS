package org.jeecgframework.web.bussi.controller.makedayplan;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.jeecgframework.core.common.controller.BaseController;

/**   
 * @Title: Controller
 * @Description: 日生产计划制定
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/makedayplanController")
public class makedayplanController extends BaseController {
	/**
	 * 月生产计划制定页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "makedayplan")
	public ModelAndView equipmentType(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/makedayplan/makedayplan");
	}
}