package org.jeecgframework.web.bussi.controller.makemonthplan;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.makemonthplan.PlanEntity;
import org.jeecgframework.web.bussi.service.makemonthplan.PlanServiceI;

/**   
 * @Title: Controller
 * @Description: 月计划表查询
 * @author zhangdaihao
 * @date 2015-07-26 16:31:15
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/planController")
public class PlanController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PlanController.class);

	@Autowired
	private PlanServiceI planService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 月计划表查询列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "plan")
	public ModelAndView plan(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/makemonthplan/planList");
	}
	/**
	 * 所有月生产计划 查询 列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "allplan")
	public ModelAndView allplan(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/checkmonthplan/allplanList");
	}
	/**
	 * 待审批月生产计划 查询 列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "planpending")
	public ModelAndView planpending(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/checkmonthplan/planpendingList");
	}
	/**
	 * 月生产计划查询列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "enquirymonthplan")
	public ModelAndView enquirymonthplan(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/enquiryplan/enquirymonthplanList");
	}
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(PlanEntity plan,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(PlanEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, plan, request.getParameterMap());
		this.planService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除月计划表查询
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(PlanEntity plan, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		plan = systemService.getEntity(PlanEntity.class, plan.getId());
		message = "月计划表查询删除成功";
		planService.delete(plan);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加月计划表查询
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(PlanEntity plan, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(plan.getId())) {
			message = "月计划表查询更新成功";
			PlanEntity t = planService.get(PlanEntity.class, plan.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(plan, t);
				planService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "月计划表查询更新失败";
			}
		} else {
			message = "月计划表查询添加成功";
			planService.save(plan);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 月计划表查询列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(PlanEntity plan, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(plan.getId())) {
			plan = planService.getEntity(PlanEntity.class, plan.getId());
			req.setAttribute("planPage", plan);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/makemonthplan/plan");
	}
}
