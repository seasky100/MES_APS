package org.jeecgframework.web.bussi.controller.editcalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.pojo.base.TSDepart;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import org.jeecgframework.web.bussi.entity.editcalendar.ShiftEntity;
import org.jeecgframework.web.bussi.service.editcalendar.ShiftServiceI;

/**   
 * @Title: Controller
 * @Description: 班次时间
 * @author zhangdaihao
 * @date 2015-07-25 17:15:44
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/shiftController")
public class ShiftController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ShiftController.class);

	@Autowired
	private ShiftServiceI shiftService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 班次时间列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "shift")
	public ModelAndView shift(HttpServletRequest request) {
		return new ModelAndView("org/jeecgframework/web/bussi/editcalendar/shiftList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(ShiftEntity shift,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ShiftEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, shift, request.getParameterMap());
		this.shiftService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除班次时间
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(ShiftEntity shift, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		shift = systemService.getEntity(ShiftEntity.class, shift.getId());
		message = "班次时间删除成功";
		shiftService.delete(shift);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加班次时间
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(ShiftEntity shift, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(shift.getId())) {
			message = "班次时间更新成功";
			ShiftEntity t = shiftService.get(ShiftEntity.class, shift.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(shift, t);
				shiftService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "班次时间更新失败";
			}
		} else {
			message = "班次时间添加成功";
			shiftService.save(shift);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 班次时间列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(ShiftEntity shift, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(shift.getId())) {
			shift = shiftService.getEntity(ShiftEntity.class, shift.getId());
			req.setAttribute("shiftPage", shift);
		}
		return new ModelAndView("org/jeecgframework/web/bussi/editcalendar/shift");
	}
}
