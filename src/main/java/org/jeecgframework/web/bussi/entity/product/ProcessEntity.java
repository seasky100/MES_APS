package org.jeecgframework.web.bussi.entity.product;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 工序信息
 * @author zhangdaihao
 * @date 2015-06-07 17:26:46
 * @version V1.0   
 *
 */
@Entity
@Table(name = "process", schema = "")
@SuppressWarnings("serial")
public class ProcessEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**craftprocessno*/
	private java.lang.String craftprocessno;
	/**craftprocessname*/
	private java.lang.String craftprocessname;
	/**craftId*/
	private java.lang.String craftId;
	/**equipmenttypeid*/
	private java.lang.Integer equipmenttypeid;
	/**equipmentid*/
	private java.lang.Integer equipmentid;
	/**equipmentno*/
	private java.lang.String equipmentno;
	/**ordernum*/
	private java.lang.Integer ordernum;
	/**setuptime*/
	private java.lang.Float setuptime;
	/**processtime*/
	private java.lang.Float processtime;
	/**transtime*/
	private java.lang.Float transtime;
	/**cost*/
	private java.lang.Float cost;
	/**needcheck*/
	private java.lang.Integer needcheck;
	/**lossfactor*/
	private java.lang.Float lossfactor;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=32,scale=0)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  craftprocessno
	 */
	@Column(name ="CRAFTPROCESSNO",nullable=false,length=50)
	public java.lang.String getCraftprocessno(){
		return this.craftprocessno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  craftprocessno
	 */
	public void setCraftprocessno(java.lang.String craftprocessno){
		this.craftprocessno = craftprocessno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  craftprocessname
	 */
	@Column(name ="CRAFTPROCESSNAME",nullable=false,length=50)
	public java.lang.String getCraftprocessname(){
		return this.craftprocessname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  craftprocessname
	 */
	public void setCraftprocessname(java.lang.String craftprocessname){
		this.craftprocessname = craftprocessname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  craftId
	 */
	@Column(name ="CRAFT_ID",nullable=false,precision=10,scale=0)
	public java.lang.String getCraftId(){
		return this.craftId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  craftId
	 */
	public void setCraftId(java.lang.String craftId){
		this.craftId = craftId;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  equipmenttypeid
	 */
	@Column(name ="EQUIPMENTTYPEID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getEquipmenttypeid(){
		return this.equipmenttypeid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  equipmenttypeid
	 */
	public void setEquipmenttypeid(java.lang.Integer equipmenttypeid){
		this.equipmenttypeid = equipmenttypeid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  equipmentid
	 */
	@Column(name ="EQUIPMENTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getEquipmentid(){
		return this.equipmentid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  equipmentid
	 */
	public void setEquipmentid(java.lang.Integer equipmentid){
		this.equipmentid = equipmentid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  equipmentno
	 */
	@Column(name ="EQUIPMENTNO",nullable=false,length=100)
	public java.lang.String getEquipmentno(){
		return this.equipmentno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  equipmentno
	 */
	public void setEquipmentno(java.lang.String equipmentno){
		this.equipmentno = equipmentno;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  ordernum
	 */
	@Column(name ="ORDERNUM",nullable=false,precision=10,scale=0)
	public java.lang.Integer getOrdernum(){
		return this.ordernum;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  ordernum
	 */
	public void setOrdernum(java.lang.Integer ordernum){
		this.ordernum = ordernum;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  setuptime
	 */
	@Column(name ="SETUPTIME",nullable=false,precision=12)
	public java.lang.Float getSetuptime(){
		return this.setuptime;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  setuptime
	 */
	public void setSetuptime(java.lang.Float setuptime){
		this.setuptime = setuptime;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  processtime
	 */
	@Column(name ="PROCESSTIME",nullable=false,precision=12)
	public java.lang.Float getProcesstime(){
		return this.processtime;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  processtime
	 */
	public void setProcesstime(java.lang.Float processtime){
		this.processtime = processtime;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  transtime
	 */
	@Column(name ="TRANSTIME",nullable=false,precision=12)
	public java.lang.Float getTranstime(){
		return this.transtime;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  transtime
	 */
	public void setTranstime(java.lang.Float transtime){
		this.transtime = transtime;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  cost
	 */
	@Column(name ="COST",nullable=false,precision=12)
	public java.lang.Float getCost(){
		return this.cost;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  cost
	 */
	public void setCost(java.lang.Float cost){
		this.cost = cost;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  needcheck
	 */
	@Column(name ="NEEDCHECK",nullable=false,precision=3,scale=0)
	public java.lang.Integer getNeedcheck(){
		return this.needcheck;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  needcheck
	 */
	public void setNeedcheck(java.lang.Integer needcheck){
		this.needcheck = needcheck;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  lossfactor
	 */
	@Column(name ="LOSSFACTOR",nullable=false,precision=12)
	public java.lang.Float getLossfactor(){
		return this.lossfactor;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  lossfactor
	 */
	public void setLossfactor(java.lang.Float lossfactor){
		this.lossfactor = lossfactor;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
