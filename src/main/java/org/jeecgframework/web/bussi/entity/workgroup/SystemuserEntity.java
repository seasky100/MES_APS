package org.jeecgframework.web.bussi.entity.workgroup;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 员工信息
 * @author zhangdaihao
 * @date 2015-05-31 20:50:02
 * @version V1.0   
 *
 */
@Entity
@Table(name = "systemuser", schema = "")
@SuppressWarnings("serial")
public class SystemuserEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**loginname*/
	private java.lang.String loginname;
	/**username*/
	private java.lang.String username;
	/**password*/
	private java.lang.String password;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**enabled*/
	private java.lang.Integer enabled;
	/**numlogin*/
	private java.lang.Integer numlogin;
	/**gendar*/
	private java.lang.Integer gendar;
	/**email*/
	private java.lang.String email;
	/**phone*/
	private java.lang.String phone;
	/**mobile*/
	private java.lang.String mobile;
	/**affiliation*/
	private java.lang.String affiliation;
	/**address*/
	private java.lang.String address;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  loginname
	 */
	@Column(name ="LOGINNAME",nullable=false,length=50)
	public java.lang.String getLoginname(){
		return this.loginname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  loginname
	 */
	public void setLoginname(java.lang.String loginname){
		this.loginname = loginname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  username
	 */
	@Column(name ="USERNAME",nullable=false,length=50)
	public java.lang.String getUsername(){
		return this.username;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  username
	 */
	public void setUsername(java.lang.String username){
		this.username = username;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  password
	 */
	@Column(name ="PASSWORD",nullable=false,length=100)
	public java.lang.String getPassword(){
		return this.password;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  password
	 */
	public void setPassword(java.lang.String password){
		this.password = password;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  enabled
	 */
	@Column(name ="ENABLED",nullable=false,precision=3,scale=0)
	public java.lang.Integer getEnabled(){
		return this.enabled;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  enabled
	 */
	public void setEnabled(java.lang.Integer enabled){
		this.enabled = enabled;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  numlogin
	 */
	@Column(name ="NUMLOGIN",nullable=false,precision=10,scale=0)
	public java.lang.Integer getNumlogin(){
		return this.numlogin;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  numlogin
	 */
	public void setNumlogin(java.lang.Integer numlogin){
		this.numlogin = numlogin;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  gendar
	 */
	@Column(name ="GENDAR",nullable=false,precision=3,scale=0)
	public java.lang.Integer getGendar(){
		return this.gendar;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  gendar
	 */
	public void setGendar(java.lang.Integer gendar){
		this.gendar = gendar;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  email
	 */
	@Column(name ="EMAIL",nullable=false,length=100)
	public java.lang.String getEmail(){
		return this.email;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  email
	 */
	public void setEmail(java.lang.String email){
		this.email = email;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  phone
	 */
	@Column(name ="PHONE",nullable=false,length=20)
	public java.lang.String getPhone(){
		return this.phone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  phone
	 */
	public void setPhone(java.lang.String phone){
		this.phone = phone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  mobile
	 */
	@Column(name ="MOBILE",nullable=false,length=20)
	public java.lang.String getMobile(){
		return this.mobile;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  mobile
	 */
	public void setMobile(java.lang.String mobile){
		this.mobile = mobile;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  affiliation
	 */
	@Column(name ="AFFILIATION",nullable=false,length=200)
	public java.lang.String getAffiliation(){
		return this.affiliation;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  affiliation
	 */
	public void setAffiliation(java.lang.String affiliation){
		this.affiliation = affiliation;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  address
	 */
	@Column(name ="ADDRESS",nullable=false,length=200)
	public java.lang.String getAddress(){
		return this.address;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  address
	 */
	public void setAddress(java.lang.String address){
		this.address = address;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
