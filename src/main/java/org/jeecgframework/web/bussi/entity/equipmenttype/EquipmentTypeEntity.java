package org.jeecgframework.web.bussi.entity.equipmenttype;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 加工区信息
 * @author zhangdaihao
 * @date 2015-07-25 15:48:48
 * @version V1.0   
 *
 */
@Entity
@Table(name = "equipmenttype", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class EquipmentTypeEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**equipmenttypeno*/
	private java.lang.String equipmenttypeno;
	/**equipmenttypename*/
	private java.lang.String equipmenttypename;
	/**info*/
	private java.lang.String info;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  equipmenttypeno
	 */
	@Column(name ="EQUIPMENTTYPENO",nullable=false,length=100)
	public java.lang.String getEquipmenttypeno(){
		return this.equipmenttypeno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  equipmenttypeno
	 */
	public void setEquipmenttypeno(java.lang.String equipmenttypeno){
		this.equipmenttypeno = equipmenttypeno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  equipmenttypename
	 */
	@Column(name ="EQUIPMENTTYPENAME",nullable=false,length=100)
	public java.lang.String getEquipmenttypename(){
		return this.equipmenttypename;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  equipmenttypename
	 */
	public void setEquipmenttypename(java.lang.String equipmenttypename){
		this.equipmenttypename = equipmenttypename;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  info
	 */
	@Column(name ="INFO",nullable=false,length=100)
	public java.lang.String getInfo(){
		return this.info;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  info
	 */
	public void setInfo(java.lang.String info){
		this.info = info;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=500)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
