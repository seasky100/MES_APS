package org.jeecgframework.web.bussi.entity.saleorder;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 销售订单查询
 * @author zhangdaihao
 * @date 2015-07-25 17:37:16
 * @version V1.0   
 *
 */
@Entity
@Table(name = "productionorder", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ProductionorderEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**productionorderno*/
	private java.lang.String productionorderno;
	/**productionordername*/
	private java.lang.String productionordername;
	/**isfrozen*/
	private java.lang.Integer isfrozen;
	/**materialId*/
	private java.lang.Integer materialId;
	/**orderlistId*/
	private java.lang.Integer orderlistId;
	/**quantity*/
	private java.lang.Integer quantity;
	/**quantityu*/
	private java.lang.Integer quantityu;
	/**quantityf*/
	private java.lang.Integer quantityf;
	/**color*/
	private java.lang.String color;
	/**earliestduedate*/
	private java.util.Date earliestduedate;
	/**latestduedate*/
	private java.util.Date latestduedate;
	/**actualdeliverydate*/
	private java.util.Date actualdeliverydate;
	/**priority*/
	private java.lang.Integer priority;
	/**state*/
	private java.lang.String state;
	/**alarmdates*/
	private java.lang.Integer alarmdates;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  productionorderno
	 */
	@Column(name ="PRODUCTIONORDERNO",nullable=false,length=50)
	public java.lang.String getProductionorderno(){
		return this.productionorderno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  productionorderno
	 */
	public void setProductionorderno(java.lang.String productionorderno){
		this.productionorderno = productionorderno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  productionordername
	 */
	@Column(name ="PRODUCTIONORDERNAME",nullable=false,length=50)
	public java.lang.String getProductionordername(){
		return this.productionordername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  productionordername
	 */
	public void setProductionordername(java.lang.String productionordername){
		this.productionordername = productionordername;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  isfrozen
	 */
	@Column(name ="ISFROZEN",nullable=false,precision=3,scale=0)
	public java.lang.Integer getIsfrozen(){
		return this.isfrozen;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  isfrozen
	 */
	public void setIsfrozen(java.lang.Integer isfrozen){
		this.isfrozen = isfrozen;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  materialId
	 */
	@Column(name ="MATERIAL_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getMaterialId(){
		return this.materialId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  materialId
	 */
	public void setMaterialId(java.lang.Integer materialId){
		this.materialId = materialId;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  orderlistId
	 */
	@Column(name ="ORDERLIST_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getOrderlistId(){
		return this.orderlistId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  orderlistId
	 */
	public void setOrderlistId(java.lang.Integer orderlistId){
		this.orderlistId = orderlistId;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  quantity
	 */
	@Column(name ="QUANTITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getQuantity(){
		return this.quantity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  quantity
	 */
	public void setQuantity(java.lang.Integer quantity){
		this.quantity = quantity;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  quantityu
	 */
	@Column(name ="QUANTITYU",nullable=false,precision=10,scale=0)
	public java.lang.Integer getQuantityu(){
		return this.quantityu;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  quantityu
	 */
	public void setQuantityu(java.lang.Integer quantityu){
		this.quantityu = quantityu;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  quantityf
	 */
	@Column(name ="QUANTITYF",nullable=false,precision=10,scale=0)
	public java.lang.Integer getQuantityf(){
		return this.quantityf;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  quantityf
	 */
	public void setQuantityf(java.lang.Integer quantityf){
		this.quantityf = quantityf;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  color
	 */
	@Column(name ="COLOR",nullable=false,length=50)
	public java.lang.String getColor(){
		return this.color;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  color
	 */
	public void setColor(java.lang.String color){
		this.color = color;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  earliestduedate
	 */
	@Column(name ="EARLIESTDUEDATE",nullable=false)
	public java.util.Date getEarliestduedate(){
		return this.earliestduedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  earliestduedate
	 */
	public void setEarliestduedate(java.util.Date earliestduedate){
		this.earliestduedate = earliestduedate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  latestduedate
	 */
	@Column(name ="LATESTDUEDATE",nullable=false)
	public java.util.Date getLatestduedate(){
		return this.latestduedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  latestduedate
	 */
	public void setLatestduedate(java.util.Date latestduedate){
		this.latestduedate = latestduedate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  actualdeliverydate
	 */
	@Column(name ="ACTUALDELIVERYDATE",nullable=false)
	public java.util.Date getActualdeliverydate(){
		return this.actualdeliverydate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  actualdeliverydate
	 */
	public void setActualdeliverydate(java.util.Date actualdeliverydate){
		this.actualdeliverydate = actualdeliverydate;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  priority
	 */
	@Column(name ="PRIORITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getPriority(){
		return this.priority;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  priority
	 */
	public void setPriority(java.lang.Integer priority){
		this.priority = priority;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  state
	 */
	@Column(name ="STATE",nullable=false,length=20)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  state
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  alarmdates
	 */
	@Column(name ="ALARMDATES",nullable=false,precision=10,scale=0)
	public java.lang.Integer getAlarmdates(){
		return this.alarmdates;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  alarmdates
	 */
	public void setAlarmdates(java.lang.Integer alarmdates){
		this.alarmdates = alarmdates;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
