package org.jeecgframework.web.bussi.entity.workgroup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 员工关联信息
 * @author zhangdaihao
 * @date 2015-05-31 20:50:02
 * @version V1.0   
 *
 */
@Entity
@Table(name = "workgroupWorker", schema = "")
@SuppressWarnings("serial")
public class WorkgroupWorkerEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**workgroupid*/
	private java.lang.String workgroupid;
	/**systemuserid*/
	private java.lang.String systemuserid;
	/**datastatus*/
	private java.lang.Integer datastatus;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  workgroupid
	 */
	@Column(name ="WorkGroup_ID",nullable=false,length=10)
	public java.lang.String getWorkgroupid() {
		return workgroupid;
	}
	
	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  workgroupid
	 */
	public void setWorkgroupid(java.lang.String workgroupid) {
		this.workgroupid = workgroupid;
	}
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  systemuserid
	 */
	@Column(name ="SystemUser_ID",nullable=false,length=10)
	public java.lang.String getSystemuserid() {
		return systemuserid;
	}
	
	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer systemuserid
	 */
	public void setSystemuserid(java.lang.String systemuserid) {
		this.systemuserid = systemuserid;
	}
	
	/**
	  *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DataStatus",nullable=false,length=1)
	public java.lang.Integer getDatastatus() {
		return datastatus;
	}
	
	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus) {
		this.datastatus = datastatus;
	}
	
	
}
