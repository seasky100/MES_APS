package org.jeecgframework.web.bussi.entity.product;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 物品信息
 * @author zhangdaihao
 * @date 2015-06-07 17:04:46
 * @version V1.0   
 *
 */
@Entity
@Table(name = "material", schema = "")
@SuppressWarnings("serial")
public class MaterialEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**materialno*/
	private java.lang.String materialno;
	/**materialname*/
	private java.lang.String materialname;
	/**parentmaterialid*/
	private java.lang.Integer parentmaterialid;
	/**materialtypeid*/
	private java.lang.Integer materialtypeid;
	/**productfamilyid*/
	private java.lang.Integer productfamilyid;
	/**purchasecost*/
	private java.lang.Float purchasecost;
	/**productioncost*/
	private java.lang.Float productioncost;
	/**sellcost*/
	private java.lang.Float sellcost;
	/**maxdailysupply*/
	private java.lang.Integer maxdailysupply;
	/**inventorynum*/
	private java.lang.Integer inventorynum;
	/**manufacturingleadtime*/
	private java.lang.Integer manufacturingleadtime;
	/**purchaseleadtime*/
	private java.lang.Integer purchaseleadtime;
	/**isapsconstraint*/
	private java.lang.Integer isapsconstraint;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  materialno
	 */
	@Column(name ="MATERIALNO",nullable=true,length=50)
	public java.lang.String getMaterialno(){
		return this.materialno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  materialno
	 */
	public void setMaterialno(java.lang.String materialno){
		this.materialno = materialno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  materialname
	 */
	@Column(name ="MATERIALNAME",nullable=true,length=50)
	public java.lang.String getMaterialname(){
		return this.materialname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  materialname
	 */
	public void setMaterialname(java.lang.String materialname){
		this.materialname = materialname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  parentmaterialid
	 */
	@Column(name ="PARENTMATERIALID",nullable=true,precision=10,scale=0)
	public java.lang.Integer getParentmaterialid(){
		return this.parentmaterialid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  parentmaterialid
	 */
	public void setParentmaterialid(java.lang.Integer parentmaterialid){
		this.parentmaterialid = parentmaterialid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  materialtypeid
	 */
	@Column(name ="MATERIALTYPEID",nullable=true,precision=10,scale=0)
	public java.lang.Integer getMaterialtypeid(){
		return this.materialtypeid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  materialtypeid
	 */
	public void setMaterialtypeid(java.lang.Integer materialtypeid){
		this.materialtypeid = materialtypeid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  productfamilyid
	 */
	@Column(name ="PRODUCTFAMILYID",nullable=true,precision=10,scale=0)
	public java.lang.Integer getProductfamilyid(){
		return this.productfamilyid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  productfamilyid
	 */
	public void setProductfamilyid(java.lang.Integer productfamilyid){
		this.productfamilyid = productfamilyid;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  purchasecost
	 */
	@Column(name ="PURCHASECOST",nullable=true,precision=12)
	public java.lang.Float getPurchasecost(){
		return this.purchasecost;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  purchasecost
	 */
	public void setPurchasecost(java.lang.Float purchasecost){
		this.purchasecost = purchasecost;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  productioncost
	 */
	@Column(name ="PRODUCTIONCOST",nullable=true,precision=12)
	public java.lang.Float getProductioncost(){
		return this.productioncost;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  productioncost
	 */
	public void setProductioncost(java.lang.Float productioncost){
		this.productioncost = productioncost;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  sellcost
	 */
	@Column(name ="SELLCOST",nullable=true,precision=12)
	public java.lang.Float getSellcost(){
		return this.sellcost;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  sellcost
	 */
	public void setSellcost(java.lang.Float sellcost){
		this.sellcost = sellcost;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  maxdailysupply
	 */
	@Column(name ="MAXDAILYSUPPLY",nullable=true,precision=10,scale=0)
	public java.lang.Integer getMaxdailysupply(){
		return this.maxdailysupply;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  maxdailysupply
	 */
	public void setMaxdailysupply(java.lang.Integer maxdailysupply){
		this.maxdailysupply = maxdailysupply;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  inventorynum
	 */
	@Column(name ="INVENTORYNUM",nullable=true,precision=10,scale=0)
	public java.lang.Integer getInventorynum(){
		return this.inventorynum;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  inventorynum
	 */
	public void setInventorynum(java.lang.Integer inventorynum){
		this.inventorynum = inventorynum;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  manufacturingleadtime
	 */
	@Column(name ="MANUFACTURINGLEADTIME",nullable=true,precision=10,scale=0)
	public java.lang.Integer getManufacturingleadtime(){
		return this.manufacturingleadtime;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  manufacturingleadtime
	 */
	public void setManufacturingleadtime(java.lang.Integer manufacturingleadtime){
		this.manufacturingleadtime = manufacturingleadtime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  purchaseleadtime
	 */
	@Column(name ="PURCHASELEADTIME",nullable=true,precision=10,scale=0)
	public java.lang.Integer getPurchaseleadtime(){
		return this.purchaseleadtime;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  purchaseleadtime
	 */
	public void setPurchaseleadtime(java.lang.Integer purchaseleadtime){
		this.purchaseleadtime = purchaseleadtime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  isapsconstraint
	 */
	@Column(name ="ISAPSCONSTRAINT",nullable=true,precision=3,scale=0)
	public java.lang.Integer getIsapsconstraint(){
		return this.isapsconstraint;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  isapsconstraint
	 */
	public void setIsapsconstraint(java.lang.Integer isapsconstraint){
		this.isapsconstraint = isapsconstraint;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=true,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=true,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
