package org.jeecgframework.web.bussi.entity.productorder;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 生产订单管理
 * @author zhangdaihao
 * @date 2015-07-26 15:05:05
 * @version V1.0   
 *
 */
@Entity
@Table(name = "planlot", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class PlanlotEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**lotno*/
	private java.lang.String lotno;
	/**lotname*/
	private java.lang.String lotname;
	/**lotquantity*/
	private java.lang.Integer lotquantity;
	/**isfrozen*/
	private java.lang.Integer isfrozen;
	/**color*/
	private java.lang.String color;
	/**lotstate*/
	private java.lang.String lotstate;
	/**materialId*/
	private java.lang.Integer materialId;
	/**craftid*/
	private java.lang.Integer craftid;
	/**numfinished*/
	private java.lang.Integer numfinished;
	/**plannedstarttime*/
	private java.util.Date plannedstarttime;
	/**plannedendtime*/
	private java.util.Date plannedendtime;
	/**actualstarttime*/
	private java.util.Date actualstarttime;
	/**actualendtime*/
	private java.util.Date actualendtime;
	/**planid*/
	private java.lang.Integer planid;
	/**productionorderid*/
	private java.lang.Integer productionorderid;
	/**orderid*/
	private java.lang.Integer orderid;
	/**releasetime*/
	private java.util.Date releasetime;
	/**duedate*/
	private java.util.Date duedate;
	/**priority*/
	private java.lang.Integer priority;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  lotno
	 */
	@Column(name ="LOTNO",nullable=false,length=50)
	public java.lang.String getLotno(){
		return this.lotno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  lotno
	 */
	public void setLotno(java.lang.String lotno){
		this.lotno = lotno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  lotname
	 */
	@Column(name ="LOTNAME",nullable=false,length=50)
	public java.lang.String getLotname(){
		return this.lotname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  lotname
	 */
	public void setLotname(java.lang.String lotname){
		this.lotname = lotname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  lotquantity
	 */
	@Column(name ="LOTQUANTITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getLotquantity(){
		return this.lotquantity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  lotquantity
	 */
	public void setLotquantity(java.lang.Integer lotquantity){
		this.lotquantity = lotquantity;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  isfrozen
	 */
	@Column(name ="ISFROZEN",nullable=false,precision=3,scale=0)
	public java.lang.Integer getIsfrozen(){
		return this.isfrozen;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  isfrozen
	 */
	public void setIsfrozen(java.lang.Integer isfrozen){
		this.isfrozen = isfrozen;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  color
	 */
	@Column(name ="COLOR",nullable=false,length=50)
	public java.lang.String getColor(){
		return this.color;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  color
	 */
	public void setColor(java.lang.String color){
		this.color = color;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  lotstate
	 */
	@Column(name ="LOTSTATE",nullable=false,length=20)
	public java.lang.String getLotstate(){
		return this.lotstate;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  lotstate
	 */
	public void setLotstate(java.lang.String lotstate){
		this.lotstate = lotstate;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  materialId
	 */
	@Column(name ="MATERIAL_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getMaterialId(){
		return this.materialId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  materialId
	 */
	public void setMaterialId(java.lang.Integer materialId){
		this.materialId = materialId;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  craftid
	 */
	@Column(name ="CRAFTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getCraftid(){
		return this.craftid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  craftid
	 */
	public void setCraftid(java.lang.Integer craftid){
		this.craftid = craftid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  numfinished
	 */
	@Column(name ="NUMFINISHED",nullable=false,precision=10,scale=0)
	public java.lang.Integer getNumfinished(){
		return this.numfinished;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  numfinished
	 */
	public void setNumfinished(java.lang.Integer numfinished){
		this.numfinished = numfinished;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  plannedstarttime
	 */
	@Column(name ="PLANNEDSTARTTIME",nullable=false)
	public java.util.Date getPlannedstarttime(){
		return this.plannedstarttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  plannedstarttime
	 */
	public void setPlannedstarttime(java.util.Date plannedstarttime){
		this.plannedstarttime = plannedstarttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  plannedendtime
	 */
	@Column(name ="PLANNEDENDTIME",nullable=false)
	public java.util.Date getPlannedendtime(){
		return this.plannedendtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  plannedendtime
	 */
	public void setPlannedendtime(java.util.Date plannedendtime){
		this.plannedendtime = plannedendtime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  actualstarttime
	 */
	@Column(name ="ACTUALSTARTTIME",nullable=false)
	public java.util.Date getActualstarttime(){
		return this.actualstarttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  actualstarttime
	 */
	public void setActualstarttime(java.util.Date actualstarttime){
		this.actualstarttime = actualstarttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  actualendtime
	 */
	@Column(name ="ACTUALENDTIME",nullable=false)
	public java.util.Date getActualendtime(){
		return this.actualendtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  actualendtime
	 */
	public void setActualendtime(java.util.Date actualendtime){
		this.actualendtime = actualendtime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  planid
	 */
	@Column(name ="PLANID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getPlanid(){
		return this.planid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  planid
	 */
	public void setPlanid(java.lang.Integer planid){
		this.planid = planid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  productionorderid
	 */
	@Column(name ="PRODUCTIONORDERID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getProductionorderid(){
		return this.productionorderid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  productionorderid
	 */
	public void setProductionorderid(java.lang.Integer productionorderid){
		this.productionorderid = productionorderid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  orderid
	 */
	@Column(name ="ORDERID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getOrderid(){
		return this.orderid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  orderid
	 */
	public void setOrderid(java.lang.Integer orderid){
		this.orderid = orderid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  releasetime
	 */
	@Column(name ="RELEASETIME",nullable=false)
	public java.util.Date getReleasetime(){
		return this.releasetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  releasetime
	 */
	public void setReleasetime(java.util.Date releasetime){
		this.releasetime = releasetime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  duedate
	 */
	@Column(name ="DUEDATE",nullable=false)
	public java.util.Date getDuedate(){
		return this.duedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  duedate
	 */
	public void setDuedate(java.util.Date duedate){
		this.duedate = duedate;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  priority
	 */
	@Column(name ="PRIORITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getPriority(){
		return this.priority;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  priority
	 */
	public void setPriority(java.lang.Integer priority){
		this.priority = priority;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
