package org.jeecgframework.web.bussi.entity.equipment;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 产线信息
 * @author zhangdaihao
 * @date 2015-07-25 15:50:47
 * @version V1.0   
 *
 */
@Entity
@Table(name = "equipment", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class EquipmentEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**equipmentno*/
	private java.lang.String equipmentno;
	/**equipmentname*/
	private java.lang.String equipmentname;
	/**manager*/
	private java.lang.String manager;
	/**workshop*/
	private java.lang.String workshop;
	/**station*/
	private java.lang.String station;
	/**equipmenttypeid*/
	private java.lang.Integer equipmenttypeid;
	/**state*/
	private java.lang.String state;
	/**maxprod*/
	private java.lang.Float maxprod;
	/**producer*/
	private java.lang.String producer;
	/**price*/
	private java.lang.Integer price;
	/**proddate*/
	private java.util.Date proddate;
	/**purchasedate*/
	private java.util.Date purchasedate;
	/**timeinuse*/
	private java.lang.Float timeinuse;
	/**calendarid*/
	private java.lang.Integer calendarid;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  equipmentno
	 */
	@Column(name ="EQUIPMENTNO",nullable=false,length=100)
	public java.lang.String getEquipmentno(){
		return this.equipmentno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  equipmentno
	 */
	public void setEquipmentno(java.lang.String equipmentno){
		this.equipmentno = equipmentno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  equipmentname
	 */
	@Column(name ="EQUIPMENTNAME",nullable=false,length=100)
	public java.lang.String getEquipmentname(){
		return this.equipmentname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  equipmentname
	 */
	public void setEquipmentname(java.lang.String equipmentname){
		this.equipmentname = equipmentname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  manager
	 */
	@Column(name ="MANAGER",nullable=false,length=50)
	public java.lang.String getManager(){
		return this.manager;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  manager
	 */
	public void setManager(java.lang.String manager){
		this.manager = manager;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  workshop
	 */
	@Column(name ="WORKSHOP",nullable=false,length=100)
	public java.lang.String getWorkshop(){
		return this.workshop;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  workshop
	 */
	public void setWorkshop(java.lang.String workshop){
		this.workshop = workshop;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  station
	 */
	@Column(name ="STATION",nullable=false,length=100)
	public java.lang.String getStation(){
		return this.station;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  station
	 */
	public void setStation(java.lang.String station){
		this.station = station;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  equipmenttypeid
	 */
	@Column(name ="EQUIPMENTTYPEID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getEquipmenttypeid(){
		return this.equipmenttypeid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  equipmenttypeid
	 */
	public void setEquipmenttypeid(java.lang.Integer equipmenttypeid){
		this.equipmenttypeid = equipmenttypeid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  state
	 */
	@Column(name ="STATE",nullable=false,length=500)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  state
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  maxprod
	 */
	@Column(name ="MAXPROD",nullable=false,precision=12)
	public java.lang.Float getMaxprod(){
		return this.maxprod;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  maxprod
	 */
	public void setMaxprod(java.lang.Float maxprod){
		this.maxprod = maxprod;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  producer
	 */
	@Column(name ="PRODUCER",nullable=false,length=500)
	public java.lang.String getProducer(){
		return this.producer;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  producer
	 */
	public void setProducer(java.lang.String producer){
		this.producer = producer;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  price
	 */
	@Column(name ="PRICE",nullable=false,precision=10,scale=0)
	public java.lang.Integer getPrice(){
		return this.price;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  price
	 */
	public void setPrice(java.lang.Integer price){
		this.price = price;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  proddate
	 */
	@Column(name ="PRODDATE",nullable=false)
	public java.util.Date getProddate(){
		return this.proddate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  proddate
	 */
	public void setProddate(java.util.Date proddate){
		this.proddate = proddate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  purchasedate
	 */
	@Column(name ="PURCHASEDATE",nullable=false)
	public java.util.Date getPurchasedate(){
		return this.purchasedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  purchasedate
	 */
	public void setPurchasedate(java.util.Date purchasedate){
		this.purchasedate = purchasedate;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  timeinuse
	 */
	@Column(name ="TIMEINUSE",nullable=false,precision=12)
	public java.lang.Float getTimeinuse(){
		return this.timeinuse;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  timeinuse
	 */
	public void setTimeinuse(java.lang.Float timeinuse){
		this.timeinuse = timeinuse;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  calendarid
	 */
	@Column(name ="CALENDARID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getCalendarid(){
		return this.calendarid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  calendarid
	 */
	public void setCalendarid(java.lang.Integer calendarid){
		this.calendarid = calendarid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
