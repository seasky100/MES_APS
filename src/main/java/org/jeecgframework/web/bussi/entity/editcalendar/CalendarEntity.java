package org.jeecgframework.web.bussi.entity.editcalendar;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 工厂日历创建
 * @author zhangdaihao
 * @date 2015-07-25 17:12:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "calendar", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class CalendarEntity implements java.io.Serializable {
	/**ID*/
	private java.lang.Integer id;
	/**日历编号*/
	private java.lang.String calendarno;
	/**日历名称*/
	private java.lang.String calendarname;
	/**起始日期*/
	private java.util.Date startdate;
	/**结束日期*/
	private java.util.Date enddate;
	/**数据状态*/
	private java.lang.Integer datastatus;
	/**备注*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  ID
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  ID
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  日历编号
	 */
	@Column(name ="CALENDARNO",nullable=false,length=100)
	public java.lang.String getCalendarno(){
		return this.calendarno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  日历编号
	 */
	public void setCalendarno(java.lang.String calendarno){
		this.calendarno = calendarno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  日历名称
	 */
	@Column(name ="CALENDARNAME",nullable=false,length=100)
	public java.lang.String getCalendarname(){
		return this.calendarname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  日历名称
	 */
	public void setCalendarname(java.lang.String calendarname){
		this.calendarname = calendarname;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  起始日期
	 */
	@Column(name ="STARTDATE",nullable=false)
	public java.util.Date getStartdate(){
		return this.startdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  起始日期
	 */
	public void setStartdate(java.util.Date startdate){
		this.startdate = startdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  结束日期
	 */
	@Column(name ="ENDDATE",nullable=false)
	public java.util.Date getEnddate(){
		return this.enddate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  结束日期
	 */
	public void setEnddate(java.util.Date enddate){
		this.enddate = enddate;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  数据状态
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  数据状态
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
