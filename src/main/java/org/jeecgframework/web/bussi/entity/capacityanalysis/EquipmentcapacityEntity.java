package org.jeecgframework.web.bussi.entity.capacityanalysis;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 产能负荷表
 * @author zhangdaihao
 * @date 2015-07-25 20:36:01
 * @version V1.0   
 *
 */
@Entity
@Table(name = "equipmentcapacity", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class EquipmentcapacityEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**resourceid*/
	private java.lang.Integer resourceid;
	/**resourceno*/
	private java.lang.String resourceno;
	/**resourcename*/
	private java.lang.String resourcename;
	/**resourcetype*/
	private java.lang.String resourcetype;
	/**committedjobprocessid*/
	private java.lang.Integer committedjobprocessid;
	/**isavailable*/
	private java.lang.Integer isavailable;
	/**starttime*/
	private java.util.Date starttime;
	/**endtime*/
	private java.util.Date endtime;
	/**availablequantity*/
	private java.lang.String availablequantity;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  resourceid
	 */
	@Column(name ="RESOURCEID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getResourceid(){
		return this.resourceid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  resourceid
	 */
	public void setResourceid(java.lang.Integer resourceid){
		this.resourceid = resourceid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  resourceno
	 */
	@Column(name ="RESOURCENO",nullable=false,length=100)
	public java.lang.String getResourceno(){
		return this.resourceno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  resourceno
	 */
	public void setResourceno(java.lang.String resourceno){
		this.resourceno = resourceno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  resourcename
	 */
	@Column(name ="RESOURCENAME",nullable=false,length=100)
	public java.lang.String getResourcename(){
		return this.resourcename;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  resourcename
	 */
	public void setResourcename(java.lang.String resourcename){
		this.resourcename = resourcename;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  resourcetype
	 */
	@Column(name ="RESOURCETYPE",nullable=false,length=100)
	public java.lang.String getResourcetype(){
		return this.resourcetype;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  resourcetype
	 */
	public void setResourcetype(java.lang.String resourcetype){
		this.resourcetype = resourcetype;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  committedjobprocessid
	 */
	@Column(name ="COMMITTEDJOBPROCESSID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getCommittedjobprocessid(){
		return this.committedjobprocessid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  committedjobprocessid
	 */
	public void setCommittedjobprocessid(java.lang.Integer committedjobprocessid){
		this.committedjobprocessid = committedjobprocessid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  isavailable
	 */
	@Column(name ="ISAVAILABLE",nullable=false,precision=3,scale=0)
	public java.lang.Integer getIsavailable(){
		return this.isavailable;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  isavailable
	 */
	public void setIsavailable(java.lang.Integer isavailable){
		this.isavailable = isavailable;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  starttime
	 */
	@Column(name ="STARTTIME",nullable=false)
	public java.util.Date getStarttime(){
		return this.starttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  starttime
	 */
	public void setStarttime(java.util.Date starttime){
		this.starttime = starttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  endtime
	 */
	@Column(name ="ENDTIME",nullable=false)
	public java.util.Date getEndtime(){
		return this.endtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  endtime
	 */
	public void setEndtime(java.util.Date endtime){
		this.endtime = endtime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  availablequantity
	 */
	@Column(name ="AVAILABLEQUANTITY",nullable=false,length=500)
	public java.lang.String getAvailablequantity(){
		return this.availablequantity;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  availablequantity
	 */
	public void setAvailablequantity(java.lang.String availablequantity){
		this.availablequantity = availablequantity;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=500)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
