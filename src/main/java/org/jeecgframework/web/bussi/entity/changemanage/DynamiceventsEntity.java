package org.jeecgframework.web.bussi.entity.changemanage;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 动态事件
 * @author zhangdaihao
 * @date 2015-08-01 13:25:21
 * @version V1.0   
 *
 */
@Entity
@Table(name = "dynamicevents", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class DynamiceventsEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**dynamiceventsno*/
	private java.lang.String dynamiceventsno;
	/**dynamiceventsname*/
	private java.lang.String dynamiceventsname;
	/**dynamiceventstype*/
	private java.lang.String dynamiceventstype;
	/**objectid*/
	private java.lang.Integer objectid;
	/**starttime*/
	private java.util.Date starttime;
	/**endtime*/
	private java.util.Date endtime;
	/**jobprocessid*/
	private java.lang.Integer jobprocessid;
	/**backlogquantity*/
	private java.lang.Integer backlogquantity;
	/**state*/
	private java.lang.String state;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dynamiceventsno
	 */
	@Column(name ="DYNAMICEVENTSNO",nullable=false,length=50)
	public java.lang.String getDynamiceventsno(){
		return this.dynamiceventsno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dynamiceventsno
	 */
	public void setDynamiceventsno(java.lang.String dynamiceventsno){
		this.dynamiceventsno = dynamiceventsno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dynamiceventsname
	 */
	@Column(name ="DYNAMICEVENTSNAME",nullable=false,length=50)
	public java.lang.String getDynamiceventsname(){
		return this.dynamiceventsname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dynamiceventsname
	 */
	public void setDynamiceventsname(java.lang.String dynamiceventsname){
		this.dynamiceventsname = dynamiceventsname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dynamiceventstype
	 */
	@Column(name ="DYNAMICEVENTSTYPE",nullable=false,length=50)
	public java.lang.String getDynamiceventstype(){
		return this.dynamiceventstype;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dynamiceventstype
	 */
	public void setDynamiceventstype(java.lang.String dynamiceventstype){
		this.dynamiceventstype = dynamiceventstype;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  objectid
	 */
	@Column(name ="OBJECTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getObjectid(){
		return this.objectid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  objectid
	 */
	public void setObjectid(java.lang.Integer objectid){
		this.objectid = objectid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  starttime
	 */
	@Column(name ="STARTTIME",nullable=false)
	public java.util.Date getStarttime(){
		return this.starttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  starttime
	 */
	public void setStarttime(java.util.Date starttime){
		this.starttime = starttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  endtime
	 */
	@Column(name ="ENDTIME",nullable=false)
	public java.util.Date getEndtime(){
		return this.endtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  endtime
	 */
	public void setEndtime(java.util.Date endtime){
		this.endtime = endtime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  jobprocessid
	 */
	@Column(name ="JOBPROCESSID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getJobprocessid(){
		return this.jobprocessid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  jobprocessid
	 */
	public void setJobprocessid(java.lang.Integer jobprocessid){
		this.jobprocessid = jobprocessid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  backlogquantity
	 */
	@Column(name ="BACKLOGQUANTITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getBacklogquantity(){
		return this.backlogquantity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  backlogquantity
	 */
	public void setBacklogquantity(java.lang.Integer backlogquantity){
		this.backlogquantity = backlogquantity;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  state
	 */
	@Column(name ="STATE",nullable=false,length=50)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  state
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
