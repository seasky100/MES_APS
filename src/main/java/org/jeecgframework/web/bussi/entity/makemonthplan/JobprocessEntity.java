package org.jeecgframework.web.bussi.entity.makemonthplan;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 排产结果
 * @author zhangdaihao
 * @date 2015-07-26 16:34:17
 * @version V1.0   
 *
 */
@Entity
@Table(name = "jobprocess", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class JobprocessEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**jobprocessno*/
	private java.lang.String jobprocessno;
	/**jobprocessname*/
	private java.lang.String jobprocessname;
	/**craftid*/
	private java.lang.Integer craftid;
	/**craftprocessid*/
	private java.lang.Integer craftprocessid;
	/**quantity*/
	private java.lang.Integer quantity;
	/**ordernum*/
	private java.lang.Integer ordernum;
	/**plannedstarttime*/
	private java.util.Date plannedstarttime;
	/**plannedendtime*/
	private java.util.Date plannedendtime;
	/**plannedequipmentsetid*/
	private java.lang.Integer plannedequipmentsetid;
	/**plannedequipmentid*/
	private java.lang.Integer plannedequipmentid;
	/**actualstarttime*/
	private java.util.Date actualstarttime;
	/**actualendtime*/
	private java.util.Date actualendtime;
	/**actualequipmentsetid*/
	private java.lang.Integer actualequipmentsetid;
	/**actualequipmentid*/
	private java.lang.Integer actualequipmentid;
	/**orderid*/
	private java.lang.Integer orderid;
	/**productionorderid*/
	private java.lang.Integer productionorderid;
	/**lotid*/
	private java.lang.Integer lotid;
	/**scheduleid*/
	private java.lang.Integer scheduleid;
	/**materialid*/
	private java.lang.Integer materialid;
	/**setuptime*/
	private java.lang.Float setuptime;
	/**turnovertime*/
	private java.lang.Float turnovertime;
	/**isfrozen*/
	private java.lang.Integer isfrozen;
	/**isoutsourcing*/
	private java.lang.Integer isoutsourcing;
	/**state*/
	private java.lang.String state;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  jobprocessno
	 */
	@Column(name ="JOBPROCESSNO",nullable=false,length=50)
	public java.lang.String getJobprocessno(){
		return this.jobprocessno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  jobprocessno
	 */
	public void setJobprocessno(java.lang.String jobprocessno){
		this.jobprocessno = jobprocessno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  jobprocessname
	 */
	@Column(name ="JOBPROCESSNAME",nullable=false,length=50)
	public java.lang.String getJobprocessname(){
		return this.jobprocessname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  jobprocessname
	 */
	public void setJobprocessname(java.lang.String jobprocessname){
		this.jobprocessname = jobprocessname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  craftid
	 */
	@Column(name ="CRAFTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getCraftid(){
		return this.craftid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  craftid
	 */
	public void setCraftid(java.lang.Integer craftid){
		this.craftid = craftid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  craftprocessid
	 */
	@Column(name ="CRAFTPROCESSID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getCraftprocessid(){
		return this.craftprocessid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  craftprocessid
	 */
	public void setCraftprocessid(java.lang.Integer craftprocessid){
		this.craftprocessid = craftprocessid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  quantity
	 */
	@Column(name ="QUANTITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getQuantity(){
		return this.quantity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  quantity
	 */
	public void setQuantity(java.lang.Integer quantity){
		this.quantity = quantity;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  ordernum
	 */
	@Column(name ="ORDERNUM",nullable=false,precision=10,scale=0)
	public java.lang.Integer getOrdernum(){
		return this.ordernum;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  ordernum
	 */
	public void setOrdernum(java.lang.Integer ordernum){
		this.ordernum = ordernum;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  plannedstarttime
	 */
	@Column(name ="PLANNEDSTARTTIME",nullable=false)
	public java.util.Date getPlannedstarttime(){
		return this.plannedstarttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  plannedstarttime
	 */
	public void setPlannedstarttime(java.util.Date plannedstarttime){
		this.plannedstarttime = plannedstarttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  plannedendtime
	 */
	@Column(name ="PLANNEDENDTIME",nullable=false)
	public java.util.Date getPlannedendtime(){
		return this.plannedendtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  plannedendtime
	 */
	public void setPlannedendtime(java.util.Date plannedendtime){
		this.plannedendtime = plannedendtime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  plannedequipmentsetid
	 */
	@Column(name ="PLANNEDEQUIPMENTSETID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getPlannedequipmentsetid(){
		return this.plannedequipmentsetid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  plannedequipmentsetid
	 */
	public void setPlannedequipmentsetid(java.lang.Integer plannedequipmentsetid){
		this.plannedequipmentsetid = plannedequipmentsetid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  plannedequipmentid
	 */
	@Column(name ="PLANNEDEQUIPMENTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getPlannedequipmentid(){
		return this.plannedequipmentid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  plannedequipmentid
	 */
	public void setPlannedequipmentid(java.lang.Integer plannedequipmentid){
		this.plannedequipmentid = plannedequipmentid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  actualstarttime
	 */
	@Column(name ="ACTUALSTARTTIME",nullable=false)
	public java.util.Date getActualstarttime(){
		return this.actualstarttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  actualstarttime
	 */
	public void setActualstarttime(java.util.Date actualstarttime){
		this.actualstarttime = actualstarttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  actualendtime
	 */
	@Column(name ="ACTUALENDTIME",nullable=false)
	public java.util.Date getActualendtime(){
		return this.actualendtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  actualendtime
	 */
	public void setActualendtime(java.util.Date actualendtime){
		this.actualendtime = actualendtime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  actualequipmentsetid
	 */
	@Column(name ="ACTUALEQUIPMENTSETID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getActualequipmentsetid(){
		return this.actualequipmentsetid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  actualequipmentsetid
	 */
	public void setActualequipmentsetid(java.lang.Integer actualequipmentsetid){
		this.actualequipmentsetid = actualequipmentsetid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  actualequipmentid
	 */
	@Column(name ="ACTUALEQUIPMENTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getActualequipmentid(){
		return this.actualequipmentid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  actualequipmentid
	 */
	public void setActualequipmentid(java.lang.Integer actualequipmentid){
		this.actualequipmentid = actualequipmentid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  orderid
	 */
	@Column(name ="ORDERID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getOrderid(){
		return this.orderid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  orderid
	 */
	public void setOrderid(java.lang.Integer orderid){
		this.orderid = orderid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  productionorderid
	 */
	@Column(name ="PRODUCTIONORDERID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getProductionorderid(){
		return this.productionorderid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  productionorderid
	 */
	public void setProductionorderid(java.lang.Integer productionorderid){
		this.productionorderid = productionorderid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  lotid
	 */
	@Column(name ="LOTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getLotid(){
		return this.lotid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  lotid
	 */
	public void setLotid(java.lang.Integer lotid){
		this.lotid = lotid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  scheduleid
	 */
	@Column(name ="SCHEDULEID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getScheduleid(){
		return this.scheduleid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  scheduleid
	 */
	public void setScheduleid(java.lang.Integer scheduleid){
		this.scheduleid = scheduleid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  materialid
	 */
	@Column(name ="MATERIALID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getMaterialid(){
		return this.materialid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  materialid
	 */
	public void setMaterialid(java.lang.Integer materialid){
		this.materialid = materialid;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  setuptime
	 */
	@Column(name ="SETUPTIME",nullable=false,precision=12)
	public java.lang.Float getSetuptime(){
		return this.setuptime;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  setuptime
	 */
	public void setSetuptime(java.lang.Float setuptime){
		this.setuptime = setuptime;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  turnovertime
	 */
	@Column(name ="TURNOVERTIME",nullable=false,precision=12)
	public java.lang.Float getTurnovertime(){
		return this.turnovertime;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  turnovertime
	 */
	public void setTurnovertime(java.lang.Float turnovertime){
		this.turnovertime = turnovertime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  isfrozen
	 */
	@Column(name ="ISFROZEN",nullable=false,precision=3,scale=0)
	public java.lang.Integer getIsfrozen(){
		return this.isfrozen;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  isfrozen
	 */
	public void setIsfrozen(java.lang.Integer isfrozen){
		this.isfrozen = isfrozen;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  isoutsourcing
	 */
	@Column(name ="ISOUTSOURCING",nullable=false,precision=3,scale=0)
	public java.lang.Integer getIsoutsourcing(){
		return this.isoutsourcing;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  isoutsourcing
	 */
	public void setIsoutsourcing(java.lang.Integer isoutsourcing){
		this.isoutsourcing = isoutsourcing;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  state
	 */
	@Column(name ="STATE",nullable=false,length=20)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  state
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
