package org.jeecgframework.web.bussi.entity.product;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 工艺信息
 * @author zhangdaihao
 * @date 2015-06-07 17:26:48
 * @version V1.0   
 *
 */
@Entity
@Table(name = "craft", schema = "")
@SuppressWarnings("serial")
public class CraftEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**craftno*/
	private java.lang.String craftno;
	/**craftname*/
	private java.lang.String craftname;
	/**crafttarget*/
	private java.lang.String crafttarget;
	/**nextcraftid*/
	private java.lang.Integer nextcraftid;
	/**precraftid*/
	private java.lang.Integer precraftid;
	/**materialId*/
	private java.lang.Integer materialId;
	/**cost*/
	private java.lang.Float cost;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  craftno
	 */
	@Column(name ="CRAFTNO",nullable=false,length=200)
	public java.lang.String getCraftno(){
		return this.craftno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  craftno
	 */
	public void setCraftno(java.lang.String craftno){
		this.craftno = craftno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  craftname
	 */
	@Column(name ="CRAFTNAME",nullable=false,length=50)
	public java.lang.String getCraftname(){
		return this.craftname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  craftname
	 */
	public void setCraftname(java.lang.String craftname){
		this.craftname = craftname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  crafttarget
	 */
	@Column(name ="CRAFTTARGET",nullable=false,length=200)
	public java.lang.String getCrafttarget(){
		return this.crafttarget;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  crafttarget
	 */
	public void setCrafttarget(java.lang.String crafttarget){
		this.crafttarget = crafttarget;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  nextcraftid
	 */
	@Column(name ="NEXTCRAFTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getNextcraftid(){
		return this.nextcraftid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  nextcraftid
	 */
	public void setNextcraftid(java.lang.Integer nextcraftid){
		this.nextcraftid = nextcraftid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  precraftid
	 */
	@Column(name ="PRECRAFTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getPrecraftid(){
		return this.precraftid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  precraftid
	 */
	public void setPrecraftid(java.lang.Integer precraftid){
		this.precraftid = precraftid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  materialId
	 */
	@Column(name ="MATERIAL_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getMaterialId(){
		return this.materialId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  materialId
	 */
	public void setMaterialId(java.lang.Integer materialId){
		this.materialId = materialId;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  cost
	 */
	@Column(name ="COST",nullable=false,precision=12)
	public java.lang.Float getCost(){
		return this.cost;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  cost
	 */
	public void setCost(java.lang.Float cost){
		this.cost = cost;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
