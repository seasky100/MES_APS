package org.jeecgframework.web.bussi.entity.workgroup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 班组信息
 * @author zhangdaihao
 * @date 2015-05-31 20:47:29
 * @version V1.0   
 *
 */
@Entity
@Table(name = "workgroup", schema = "")
@SuppressWarnings("serial")
public class WorkgroupEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**workgroupno*/
	private java.lang.String workgroupno;
	/**workgroupname*/
	private java.lang.String workgroupname;
	/**manager*/
	private java.lang.String manager;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  workgroupno
	 */
	@Column(name ="WORKGROUPNO",nullable=false,length=50)
	public java.lang.String getWorkgroupno(){
		return this.workgroupno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  workgroupno
	 */
	public void setWorkgroupno(java.lang.String workgroupno){
		this.workgroupno = workgroupno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  workgroupname
	 */
	@Column(name ="WORKGROUPNAME",nullable=false,length=50)
	public java.lang.String getWorkgroupname(){
		return this.workgroupname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  workgroupname
	 */
	public void setWorkgroupname(java.lang.String workgroupname){
		this.workgroupname = workgroupname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  manager
	 */
	@Column(name ="MANAGER",nullable=false,length=50)
	public java.lang.String getManager(){
		return this.manager;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  manager
	 */
	public void setManager(java.lang.String manager){
		this.manager = manager;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
