package org.jeecgframework.web.bussi.entity.editcalendar;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 班次模式
 * @author zhangdaihao
 * @date 2015-07-25 17:24:49
 * @version V1.0   
 *
 */
@Entity
@Table(name = "shiftpatterntype", schema = "")
@SuppressWarnings("serial")
public class ShiftpatterntypeEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**shiftpatterntypeno*/
	private java.lang.String shiftpatterntypeno;
	/**shiftpatterntypename*/
	private java.lang.String shiftpatterntypename;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  shiftpatterntypeno
	 */
	@Column(name ="SHIFTPATTERNTYPENO",nullable=false,length=50)
	public java.lang.String getShiftpatterntypeno(){
		return this.shiftpatterntypeno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  shiftpatterntypeno
	 */
	public void setShiftpatterntypeno(java.lang.String shiftpatterntypeno){
		this.shiftpatterntypeno = shiftpatterntypeno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  shiftpatterntypename
	 */
	@Column(name ="SHIFTPATTERNTYPENAME",nullable=false,length=50)
	public java.lang.String getShiftpatterntypename(){
		return this.shiftpatterntypename;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  shiftpatterntypename
	 */
	public void setShiftpatterntypename(java.lang.String shiftpatterntypename){
		this.shiftpatterntypename = shiftpatterntypename;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
