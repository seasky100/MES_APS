package org.jeecgframework.web.bussi.entity.makemonthplan;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 月计划表查询
 * @author zhangdaihao
 * @date 2015-07-26 16:31:15
 * @version V1.0   
 *
 */
@Entity
@Table(name = "plan", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class PlanEntity implements java.io.Serializable {
	/**主键*/
	private java.lang.Integer id;
	/**计划编号*/
	private java.lang.String planno;
	/**计划名称*/
	private java.lang.String planname;
	/**计划人*/
	private java.lang.String planner;
	/**保存时间*/
	private java.util.Date savetime;
	/**提示日期*/
	private java.lang.Integer alarmdates;
	/**审核人*/
	private java.lang.String checker;
	/**检查号*/
	private java.lang.Integer numcheck;
	/**检查信息*/
	private java.lang.String checkinfo;
	/**状态*/
	private java.lang.String state;
	/**算法*/
	private java.lang.String algorithminfo;
	/**数据状态*/
	private java.lang.Integer datastatus;
	/**备注*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  主键
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  主键
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  计划编号
	 */
	@Column(name ="PLANNO",nullable=false,length=50)
	public java.lang.String getPlanno(){
		return this.planno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  计划编号
	 */
	public void setPlanno(java.lang.String planno){
		this.planno = planno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  计划名称
	 */
	@Column(name ="PLANNAME",nullable=false,length=50)
	public java.lang.String getPlanname(){
		return this.planname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  计划名称
	 */
	public void setPlanname(java.lang.String planname){
		this.planname = planname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  计划人
	 */
	@Column(name ="PLANNER",nullable=false,length=50)
	public java.lang.String getPlanner(){
		return this.planner;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  计划人
	 */
	public void setPlanner(java.lang.String planner){
		this.planner = planner;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  保存时间
	 */
	@Column(name ="SAVETIME",nullable=false)
	public java.util.Date getSavetime(){
		return this.savetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  保存时间
	 */
	public void setSavetime(java.util.Date savetime){
		this.savetime = savetime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  提示日期
	 */
	@Column(name ="ALARMDATES",nullable=false,precision=10,scale=0)
	public java.lang.Integer getAlarmdates(){
		return this.alarmdates;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  提示日期
	 */
	public void setAlarmdates(java.lang.Integer alarmdates){
		this.alarmdates = alarmdates;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  审核人
	 */
	@Column(name ="CHECKER",nullable=false,length=50)
	public java.lang.String getChecker(){
		return this.checker;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  审核人
	 */
	public void setChecker(java.lang.String checker){
		this.checker = checker;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  检查号
	 */
	@Column(name ="NUMCHECK",nullable=false,precision=10,scale=0)
	public java.lang.Integer getNumcheck(){
		return this.numcheck;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  检查号
	 */
	public void setNumcheck(java.lang.Integer numcheck){
		this.numcheck = numcheck;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  检查信息
	 */
	@Column(name ="CHECKINFO",nullable=false,length=200)
	public java.lang.String getCheckinfo(){
		return this.checkinfo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  检查信息
	 */
	public void setCheckinfo(java.lang.String checkinfo){
		this.checkinfo = checkinfo;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  状态
	 */
	@Column(name ="STATE",nullable=false,length=20)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  状态
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  算法
	 */
	@Column(name ="ALGORITHMINFO",nullable=false,length=200)
	public java.lang.String getAlgorithminfo(){
		return this.algorithminfo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  算法
	 */
	public void setAlgorithminfo(java.lang.String algorithminfo){
		this.algorithminfo = algorithminfo;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  数据状态
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  数据状态
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="MEMO",nullable=false,length=20)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
