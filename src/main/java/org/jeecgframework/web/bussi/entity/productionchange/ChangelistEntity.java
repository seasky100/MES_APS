package org.jeecgframework.web.bussi.entity.productionchange;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 变更单
 * @author zhangdaihao
 * @date 2015-08-01 13:08:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "changelist", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ChangelistEntity implements java.io.Serializable {
	/**ID*/
	private java.lang.Integer id;
	/**变更编号*/
	private java.lang.String changelistno;
	/**变更名称*/
	private java.lang.String changelistname;
	/**ProductionChange_ID*/
	private java.lang.Integer productionchangeId;
	/**变更文档类型*/
	private java.lang.String changedoctype;
	/**变更文档编号*/
	private java.lang.String changedocno;
	/**materialid*/
	private java.lang.Integer materialid;
	/**changedmaterialid*/
	private java.lang.Integer changedmaterialid;
	/**quantity*/
	private java.lang.Integer quantity;
	/**changedquantity*/
	private java.lang.Integer changedquantity;
	/**starttime*/
	private java.util.Date starttime;
	/**changedstarttime*/
	private java.util.Date changedstarttime;
	/**endtime*/
	private java.util.Date endtime;
	/**changedendtime*/
	private java.util.Date changedendtime;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  ID
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  ID
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  变更编号
	 */
	@Column(name ="CHANGELISTNO",nullable=false,length=50)
	public java.lang.String getChangelistno(){
		return this.changelistno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  变更编号
	 */
	public void setChangelistno(java.lang.String changelistno){
		this.changelistno = changelistno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  变更名称
	 */
	@Column(name ="CHANGELISTNAME",nullable=false,length=50)
	public java.lang.String getChangelistname(){
		return this.changelistname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  变更名称
	 */
	public void setChangelistname(java.lang.String changelistname){
		this.changelistname = changelistname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  ProductionChange_ID
	 */
	@Column(name ="PRODUCTIONCHANGE_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getProductionchangeId(){
		return this.productionchangeId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  ProductionChange_ID
	 */
	public void setProductionchangeId(java.lang.Integer productionchangeId){
		this.productionchangeId = productionchangeId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  变更文档类型
	 */
	@Column(name ="CHANGEDOCTYPE",nullable=false,length=50)
	public java.lang.String getChangedoctype(){
		return this.changedoctype;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  变更文档类型
	 */
	public void setChangedoctype(java.lang.String changedoctype){
		this.changedoctype = changedoctype;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  变更文档编号
	 */
	@Column(name ="CHANGEDOCNO",nullable=false,length=100)
	public java.lang.String getChangedocno(){
		return this.changedocno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  变更文档编号
	 */
	public void setChangedocno(java.lang.String changedocno){
		this.changedocno = changedocno;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  materialid
	 */
	@Column(name ="MATERIALID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getMaterialid(){
		return this.materialid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  materialid
	 */
	public void setMaterialid(java.lang.Integer materialid){
		this.materialid = materialid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  changedmaterialid
	 */
	@Column(name ="CHANGEDMATERIALID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getChangedmaterialid(){
		return this.changedmaterialid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  changedmaterialid
	 */
	public void setChangedmaterialid(java.lang.Integer changedmaterialid){
		this.changedmaterialid = changedmaterialid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  quantity
	 */
	@Column(name ="QUANTITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getQuantity(){
		return this.quantity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  quantity
	 */
	public void setQuantity(java.lang.Integer quantity){
		this.quantity = quantity;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  changedquantity
	 */
	@Column(name ="CHANGEDQUANTITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getChangedquantity(){
		return this.changedquantity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  changedquantity
	 */
	public void setChangedquantity(java.lang.Integer changedquantity){
		this.changedquantity = changedquantity;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  starttime
	 */
	@Column(name ="STARTTIME",nullable=false)
	public java.util.Date getStarttime(){
		return this.starttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  starttime
	 */
	public void setStarttime(java.util.Date starttime){
		this.starttime = starttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  changedstarttime
	 */
	@Column(name ="CHANGEDSTARTTIME",nullable=false)
	public java.util.Date getChangedstarttime(){
		return this.changedstarttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  changedstarttime
	 */
	public void setChangedstarttime(java.util.Date changedstarttime){
		this.changedstarttime = changedstarttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  endtime
	 */
	@Column(name ="ENDTIME",nullable=false)
	public java.util.Date getEndtime(){
		return this.endtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  endtime
	 */
	public void setEndtime(java.util.Date endtime){
		this.endtime = endtime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  changedendtime
	 */
	@Column(name ="CHANGEDENDTIME",nullable=false)
	public java.util.Date getChangedendtime(){
		return this.changedendtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  changedendtime
	 */
	public void setChangedendtime(java.util.Date changedendtime){
		this.changedendtime = changedendtime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
