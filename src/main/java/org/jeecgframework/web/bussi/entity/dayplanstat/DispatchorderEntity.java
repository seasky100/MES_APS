package org.jeecgframework.web.bussi.entity.dayplanstat;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 日派工单
 * @author zhangdaihao
 * @date 2015-07-25 20:56:46
 * @version V1.0   
 *
 */
@Entity
@Table(name = "dispatchorder", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class DispatchorderEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**dispatchorderno*/
	private java.lang.String dispatchorderno;
	/**dispatchordername*/
	private java.lang.String dispatchordername;
	/**equipmentsetid*/
	private java.lang.Integer equipmentsetid;
	/**equipmentid*/
	private java.lang.Integer equipmentid;
	/**workgroupmanager*/
	private java.lang.String workgroupmanager;
	/**assistant*/
	private java.lang.String assistant;
	/**quantity*/
	private java.lang.Integer quantity;
	/**craftid*/
	private java.lang.Integer craftid;
	/**jobprocessid*/
	private java.lang.Integer jobprocessid;
	/**ordernum*/
	private java.lang.Integer ordernum;
	/**isfrozen*/
	private java.lang.Integer isfrozen;
	/**lotstate*/
	private java.lang.String lotstate;
	/**materialId*/
	private java.lang.Integer materialId;
	/**processId*/
	private java.lang.Integer processId;
	/**plannedstarttime*/
	private java.util.Date plannedstarttime;
	/**plannedendtime*/
	private java.util.Date plannedendtime;
	/**actualstarttime*/
	private java.util.Date actualstarttime;
	/**actualendtime*/
	private java.util.Date actualendtime;
	/**numfinished*/
	private java.lang.Integer numfinished;
	/**dispatchId*/
	private java.lang.Integer dispatchId;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dispatchorderno
	 */
	@Column(name ="DISPATCHORDERNO",nullable=false,length=255)
	public java.lang.String getDispatchorderno(){
		return this.dispatchorderno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dispatchorderno
	 */
	public void setDispatchorderno(java.lang.String dispatchorderno){
		this.dispatchorderno = dispatchorderno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dispatchordername
	 */
	@Column(name ="DISPATCHORDERNAME",nullable=false,length=255)
	public java.lang.String getDispatchordername(){
		return this.dispatchordername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dispatchordername
	 */
	public void setDispatchordername(java.lang.String dispatchordername){
		this.dispatchordername = dispatchordername;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  equipmentsetid
	 */
	@Column(name ="EQUIPMENTSETID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getEquipmentsetid(){
		return this.equipmentsetid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  equipmentsetid
	 */
	public void setEquipmentsetid(java.lang.Integer equipmentsetid){
		this.equipmentsetid = equipmentsetid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  equipmentid
	 */
	@Column(name ="EQUIPMENTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getEquipmentid(){
		return this.equipmentid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  equipmentid
	 */
	public void setEquipmentid(java.lang.Integer equipmentid){
		this.equipmentid = equipmentid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  workgroupmanager
	 */
	@Column(name ="WORKGROUPMANAGER",nullable=false,length=255)
	public java.lang.String getWorkgroupmanager(){
		return this.workgroupmanager;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  workgroupmanager
	 */
	public void setWorkgroupmanager(java.lang.String workgroupmanager){
		this.workgroupmanager = workgroupmanager;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  assistant
	 */
	@Column(name ="ASSISTANT",nullable=false,length=255)
	public java.lang.String getAssistant(){
		return this.assistant;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  assistant
	 */
	public void setAssistant(java.lang.String assistant){
		this.assistant = assistant;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  quantity
	 */
	@Column(name ="QUANTITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getQuantity(){
		return this.quantity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  quantity
	 */
	public void setQuantity(java.lang.Integer quantity){
		this.quantity = quantity;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  craftid
	 */
	@Column(name ="CRAFTID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getCraftid(){
		return this.craftid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  craftid
	 */
	public void setCraftid(java.lang.Integer craftid){
		this.craftid = craftid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  jobprocessid
	 */
	@Column(name ="JOBPROCESSID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getJobprocessid(){
		return this.jobprocessid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  jobprocessid
	 */
	public void setJobprocessid(java.lang.Integer jobprocessid){
		this.jobprocessid = jobprocessid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  ordernum
	 */
	@Column(name ="ORDERNUM",nullable=false,precision=10,scale=0)
	public java.lang.Integer getOrdernum(){
		return this.ordernum;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  ordernum
	 */
	public void setOrdernum(java.lang.Integer ordernum){
		this.ordernum = ordernum;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  isfrozen
	 */
	@Column(name ="ISFROZEN",nullable=false,precision=3,scale=0)
	public java.lang.Integer getIsfrozen(){
		return this.isfrozen;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  isfrozen
	 */
	public void setIsfrozen(java.lang.Integer isfrozen){
		this.isfrozen = isfrozen;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  lotstate
	 */
	@Column(name ="LOTSTATE",nullable=false,length=255)
	public java.lang.String getLotstate(){
		return this.lotstate;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  lotstate
	 */
	public void setLotstate(java.lang.String lotstate){
		this.lotstate = lotstate;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  materialId
	 */
	@Column(name ="MATERIAL_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getMaterialId(){
		return this.materialId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  materialId
	 */
	public void setMaterialId(java.lang.Integer materialId){
		this.materialId = materialId;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  processId
	 */
	@Column(name ="PROCESS_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getProcessId(){
		return this.processId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  processId
	 */
	public void setProcessId(java.lang.Integer processId){
		this.processId = processId;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  plannedstarttime
	 */
	@Column(name ="PLANNEDSTARTTIME",nullable=false)
	public java.util.Date getPlannedstarttime(){
		return this.plannedstarttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  plannedstarttime
	 */
	public void setPlannedstarttime(java.util.Date plannedstarttime){
		this.plannedstarttime = plannedstarttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  plannedendtime
	 */
	@Column(name ="PLANNEDENDTIME",nullable=false)
	public java.util.Date getPlannedendtime(){
		return this.plannedendtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  plannedendtime
	 */
	public void setPlannedendtime(java.util.Date plannedendtime){
		this.plannedendtime = plannedendtime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  actualstarttime
	 */
	@Column(name ="ACTUALSTARTTIME",nullable=false)
	public java.util.Date getActualstarttime(){
		return this.actualstarttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  actualstarttime
	 */
	public void setActualstarttime(java.util.Date actualstarttime){
		this.actualstarttime = actualstarttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  actualendtime
	 */
	@Column(name ="ACTUALENDTIME",nullable=false)
	public java.util.Date getActualendtime(){
		return this.actualendtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  actualendtime
	 */
	public void setActualendtime(java.util.Date actualendtime){
		this.actualendtime = actualendtime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  numfinished
	 */
	@Column(name ="NUMFINISHED",nullable=false,precision=10,scale=0)
	public java.lang.Integer getNumfinished(){
		return this.numfinished;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  numfinished
	 */
	public void setNumfinished(java.lang.Integer numfinished){
		this.numfinished = numfinished;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  dispatchId
	 */
	@Column(name ="DISPATCH_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getDispatchId(){
		return this.dispatchId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  dispatchId
	 */
	public void setDispatchId(java.lang.Integer dispatchId){
		this.dispatchId = dispatchId;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=255)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
