package org.jeecgframework.web.bussi.entity.equipmentset;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 车间信息
 * @author zhangdaihao
 * @date 2015-07-25 15:50:03
 * @version V1.0   
 *
 */
@Entity
@Table(name = "equipmentset", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class EquipmentsetEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**equipmentsetno*/
	private java.lang.String equipmentsetno;
	/**equipmentsetname*/
	private java.lang.String equipmentsetname;
	/**manager*/
	private java.lang.String manager;
	/**deptname*/
	private java.lang.String deptname;
	/**equipmenttypeid*/
	private java.lang.Integer equipmenttypeid;
	/**state*/
	private java.lang.String state;
	/**maxprod*/
	private java.lang.Float maxprod;
	/**calendarid*/
	private java.lang.Integer calendarid;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  equipmentsetno
	 */
	@Column(name ="EQUIPMENTSETNO",nullable=false,length=100)
	public java.lang.String getEquipmentsetno(){
		return this.equipmentsetno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  equipmentsetno
	 */
	public void setEquipmentsetno(java.lang.String equipmentsetno){
		this.equipmentsetno = equipmentsetno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  equipmentsetname
	 */
	@Column(name ="EQUIPMENTSETNAME",nullable=false,length=100)
	public java.lang.String getEquipmentsetname(){
		return this.equipmentsetname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  equipmentsetname
	 */
	public void setEquipmentsetname(java.lang.String equipmentsetname){
		this.equipmentsetname = equipmentsetname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  manager
	 */
	@Column(name ="MANAGER",nullable=false,length=50)
	public java.lang.String getManager(){
		return this.manager;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  manager
	 */
	public void setManager(java.lang.String manager){
		this.manager = manager;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  deptname
	 */
	@Column(name ="DEPTNAME",nullable=false,length=100)
	public java.lang.String getDeptname(){
		return this.deptname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  deptname
	 */
	public void setDeptname(java.lang.String deptname){
		this.deptname = deptname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  equipmenttypeid
	 */
	@Column(name ="EQUIPMENTTYPEID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getEquipmenttypeid(){
		return this.equipmenttypeid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  equipmenttypeid
	 */
	public void setEquipmenttypeid(java.lang.Integer equipmenttypeid){
		this.equipmenttypeid = equipmenttypeid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  state
	 */
	@Column(name ="STATE",nullable=false,length=500)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  state
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  maxprod
	 */
	@Column(name ="MAXPROD",nullable=false,precision=12)
	public java.lang.Float getMaxprod(){
		return this.maxprod;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  maxprod
	 */
	public void setMaxprod(java.lang.Float maxprod){
		this.maxprod = maxprod;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  calendarid
	 */
	@Column(name ="CALENDARID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getCalendarid(){
		return this.calendarid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  calendarid
	 */
	public void setCalendarid(java.lang.Integer calendarid){
		this.calendarid = calendarid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=500)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
