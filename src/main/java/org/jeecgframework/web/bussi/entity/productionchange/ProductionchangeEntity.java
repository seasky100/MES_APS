package org.jeecgframework.web.bussi.entity.productionchange;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 生产变计划
 * @author zhangdaihao
 * @date 2015-08-01 13:08:27
 * @version V1.0   
 *
 */
@Entity
@Table(name = "productionchange", schema = "")
@SuppressWarnings("serial")
public class ProductionchangeEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**productionchangeno*/
	private java.lang.String productionchangeno;
	/**productionchangename*/
	private java.lang.String productionchangename;
	/**planner*/
	private java.lang.String planner;
	/**dynamicscheduleoperation*/
	private java.lang.String dynamicscheduleoperation;
	/**dynamicscheduleobjective*/
	private java.lang.String dynamicscheduleobjective;
	/**applytime*/
	private java.util.Date applytime;
	/**description*/
	private java.lang.String description;
	/**changetime*/
	private java.util.Date changetime;
	/**state*/
	private java.lang.String state;
	/**confirmer*/
	private java.lang.String confirmer;
	/**numconfirm*/
	private java.lang.Integer numconfirm;
	/**confirmtime*/
	private java.util.Date confirmtime;
	/**confirminfo*/
	private java.lang.String confirminfo;
	/**checker*/
	private java.lang.String checker;
	/**numcheck*/
	private java.lang.Integer numcheck;
	/**checktime*/
	private java.util.Date checktime;
	/**checkinfo*/
	private java.lang.String checkinfo;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  productionchangeno
	 */
	@Column(name ="PRODUCTIONCHANGENO",nullable=false,length=50)
	public java.lang.String getProductionchangeno(){
		return this.productionchangeno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  productionchangeno
	 */
	public void setProductionchangeno(java.lang.String productionchangeno){
		this.productionchangeno = productionchangeno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  productionchangename
	 */
	@Column(name ="PRODUCTIONCHANGENAME",nullable=false,length=50)
	public java.lang.String getProductionchangename(){
		return this.productionchangename;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  productionchangename
	 */
	public void setProductionchangename(java.lang.String productionchangename){
		this.productionchangename = productionchangename;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  planner
	 */
	@Column(name ="PLANNER",nullable=false,length=50)
	public java.lang.String getPlanner(){
		return this.planner;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  planner
	 */
	public void setPlanner(java.lang.String planner){
		this.planner = planner;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dynamicscheduleoperation
	 */
	@Column(name ="DYNAMICSCHEDULEOPERATION",nullable=false,length=100)
	public java.lang.String getDynamicscheduleoperation(){
		return this.dynamicscheduleoperation;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dynamicscheduleoperation
	 */
	public void setDynamicscheduleoperation(java.lang.String dynamicscheduleoperation){
		this.dynamicscheduleoperation = dynamicscheduleoperation;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dynamicscheduleobjective
	 */
	@Column(name ="DYNAMICSCHEDULEOBJECTIVE",nullable=false,length=100)
	public java.lang.String getDynamicscheduleobjective(){
		return this.dynamicscheduleobjective;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dynamicscheduleobjective
	 */
	public void setDynamicscheduleobjective(java.lang.String dynamicscheduleobjective){
		this.dynamicscheduleobjective = dynamicscheduleobjective;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  applytime
	 */
	@Column(name ="APPLYTIME",nullable=false)
	public java.util.Date getApplytime(){
		return this.applytime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  applytime
	 */
	public void setApplytime(java.util.Date applytime){
		this.applytime = applytime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  description
	 */
	@Column(name ="DESCRIPTION",nullable=false,length=200)
	public java.lang.String getDescription(){
		return this.description;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  description
	 */
	public void setDescription(java.lang.String description){
		this.description = description;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  changetime
	 */
	@Column(name ="CHANGETIME",nullable=false)
	public java.util.Date getChangetime(){
		return this.changetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  changetime
	 */
	public void setChangetime(java.util.Date changetime){
		this.changetime = changetime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  state
	 */
	@Column(name ="STATE",nullable=false,length=20)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  state
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  confirmer
	 */
	@Column(name ="CONFIRMER",nullable=true,length=50)
	public java.lang.String getConfirmer(){
		return this.confirmer;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  confirmer
	 */
	public void setConfirmer(java.lang.String confirmer){
		this.confirmer = confirmer;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  numconfirm
	 */
	@Column(name ="NUMCONFIRM",nullable=true,precision=10,scale=0)
	public java.lang.Integer getNumconfirm(){
		return this.numconfirm;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  numconfirm
	 */
	public void setNumconfirm(java.lang.Integer numconfirm){
		this.numconfirm = numconfirm;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  confirmtime
	 */
	@Column(name ="CONFIRMTIME",nullable=true)
	public java.util.Date getConfirmtime(){
		return this.confirmtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  confirmtime
	 */
	public void setConfirmtime(java.util.Date confirmtime){
		this.confirmtime = confirmtime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  confirminfo
	 */
	@Column(name ="CONFIRMINFO",nullable=true,length=200)
	public java.lang.String getConfirminfo(){
		return this.confirminfo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  confirminfo
	 */
	public void setConfirminfo(java.lang.String confirminfo){
		this.confirminfo = confirminfo;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  checker
	 */
	@Column(name ="CHECKER",nullable=false,length=50)
	public java.lang.String getChecker(){
		return this.checker;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  checker
	 */
	public void setChecker(java.lang.String checker){
		this.checker = checker;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  numcheck
	 */
	@Column(name ="NUMCHECK",nullable=false,precision=10,scale=0)
	public java.lang.Integer getNumcheck(){
		return this.numcheck;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  numcheck
	 */
	public void setNumcheck(java.lang.Integer numcheck){
		this.numcheck = numcheck;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  checktime
	 */
	@Column(name ="CHECKTIME",nullable=true)
	public java.util.Date getChecktime(){
		return this.checktime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  checktime
	 */
	public void setChecktime(java.util.Date checktime){
		this.checktime = checktime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  checkinfo
	 */
	@Column(name ="CHECKINFO",nullable=false,length=200)
	public java.lang.String getCheckinfo(){
		return this.checkinfo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  checkinfo
	 */
	public void setCheckinfo(java.lang.String checkinfo){
		this.checkinfo = checkinfo;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
