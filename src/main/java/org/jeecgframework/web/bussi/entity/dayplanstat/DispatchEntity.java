package org.jeecgframework.web.bussi.entity.dayplanstat;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 日生产计划
 * @author zhangdaihao
 * @date 2015-07-25 20:56:48
 * @version V1.0   
 *
 */
@Entity
@Table(name = "dispatch", schema = "")
@SuppressWarnings("serial")
public class DispatchEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**dispatchno*/
	private java.lang.String dispatchno;
	/**dispatchname*/
	private java.lang.String dispatchname;
	/**dispatcher*/
	private java.lang.String dispatcher;
	/**savetime*/
	private java.util.Date savetime;
	/**alarmdates*/
	private java.lang.Integer alarmdates;
	/**confirmer*/
	private java.lang.String confirmer;
	/**numconfirm*/
	private java.lang.Integer numconfirm;
	/**confirmtime*/
	private java.util.Date confirmtime;
	/**confirminfo*/
	private java.lang.String confirminfo;
	/**checker*/
	private java.lang.String checker;
	/**numcheck*/
	private java.lang.Integer numcheck;
	/**checktime*/
	private java.util.Date checktime;
	/**checkinfo*/
	private java.lang.String checkinfo;
	/**state*/
	private java.lang.String state;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dispatchno
	 */
	@Column(name ="DISPATCHNO",nullable=false,length=50)
	public java.lang.String getDispatchno(){
		return this.dispatchno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dispatchno
	 */
	public void setDispatchno(java.lang.String dispatchno){
		this.dispatchno = dispatchno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dispatchname
	 */
	@Column(name ="DISPATCHNAME",nullable=false,length=50)
	public java.lang.String getDispatchname(){
		return this.dispatchname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dispatchname
	 */
	public void setDispatchname(java.lang.String dispatchname){
		this.dispatchname = dispatchname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  dispatcher
	 */
	@Column(name ="DISPATCHER",nullable=false,length=50)
	public java.lang.String getDispatcher(){
		return this.dispatcher;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  dispatcher
	 */
	public void setDispatcher(java.lang.String dispatcher){
		this.dispatcher = dispatcher;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  savetime
	 */
	@Column(name ="SAVETIME",nullable=false)
	public java.util.Date getSavetime(){
		return this.savetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  savetime
	 */
	public void setSavetime(java.util.Date savetime){
		this.savetime = savetime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  alarmdates
	 */
	@Column(name ="ALARMDATES",nullable=false,precision=10,scale=0)
	public java.lang.Integer getAlarmdates(){
		return this.alarmdates;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  alarmdates
	 */
	public void setAlarmdates(java.lang.Integer alarmdates){
		this.alarmdates = alarmdates;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  confirmer
	 */
	@Column(name ="CONFIRMER",nullable=false,length=50)
	public java.lang.String getConfirmer(){
		return this.confirmer;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  confirmer
	 */
	public void setConfirmer(java.lang.String confirmer){
		this.confirmer = confirmer;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  numconfirm
	 */
	@Column(name ="NUMCONFIRM",nullable=false,precision=10,scale=0)
	public java.lang.Integer getNumconfirm(){
		return this.numconfirm;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  numconfirm
	 */
	public void setNumconfirm(java.lang.Integer numconfirm){
		this.numconfirm = numconfirm;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  confirmtime
	 */
	@Column(name ="CONFIRMTIME",nullable=false)
	public java.util.Date getConfirmtime(){
		return this.confirmtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  confirmtime
	 */
	public void setConfirmtime(java.util.Date confirmtime){
		this.confirmtime = confirmtime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  confirminfo
	 */
	@Column(name ="CONFIRMINFO",nullable=false,length=200)
	public java.lang.String getConfirminfo(){
		return this.confirminfo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  confirminfo
	 */
	public void setConfirminfo(java.lang.String confirminfo){
		this.confirminfo = confirminfo;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  checker
	 */
	@Column(name ="CHECKER",nullable=false,length=50)
	public java.lang.String getChecker(){
		return this.checker;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  checker
	 */
	public void setChecker(java.lang.String checker){
		this.checker = checker;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  numcheck
	 */
	@Column(name ="NUMCHECK",nullable=false,precision=10,scale=0)
	public java.lang.Integer getNumcheck(){
		return this.numcheck;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  numcheck
	 */
	public void setNumcheck(java.lang.Integer numcheck){
		this.numcheck = numcheck;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  checktime
	 */
	@Column(name ="CHECKTIME",nullable=false)
	public java.util.Date getChecktime(){
		return this.checktime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  checktime
	 */
	public void setChecktime(java.util.Date checktime){
		this.checktime = checktime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  checkinfo
	 */
	@Column(name ="CHECKINFO",nullable=false,length=200)
	public java.lang.String getCheckinfo(){
		return this.checkinfo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  checkinfo
	 */
	public void setCheckinfo(java.lang.String checkinfo){
		this.checkinfo = checkinfo;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  state
	 */
	@Column(name ="STATE",nullable=false,length=20)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  state
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
