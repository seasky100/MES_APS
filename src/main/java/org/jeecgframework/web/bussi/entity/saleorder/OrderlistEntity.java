package org.jeecgframework.web.bussi.entity.saleorder;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 销售订单录入转换
 * @author zhangdaihao
 * @date 2015-07-25 17:37:19
 * @version V1.0   
 *
 */
@Entity
@Table(name = "orderlist", schema = "")
@SuppressWarnings("serial")
public class OrderlistEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**orderno*/
	private java.lang.String orderno;
	/**ordername*/
	private java.lang.String ordername;
	/**orderprice*/
	private java.lang.Float orderprice;
	/**systemuserId*/
	private java.lang.Integer systemuserId;
	/**tardinesscost*/
	private java.lang.Float tardinesscost;
	/**inventorycost*/
	private java.lang.Float inventorycost;
	/**materialid*/
	private java.lang.Integer materialid;
	/**quantity*/
	private java.lang.Integer quantity;
	/**quantityu*/
	private java.lang.Integer quantityu;
	/**quantityf*/
	private java.lang.Integer quantityf;
	/**orderdate*/
	private java.util.Date orderdate;
	/**releasedate*/
	private java.util.Date releasedate;
	/**duedate*/
	private java.util.Date duedate;
	/**actualddate*/
	private java.util.Date actualddate;
	/**priority*/
	private java.lang.Integer priority;
	/**color*/
	private java.lang.String color;
	/**isplan*/
	private java.lang.Integer isplan;
	/**state*/
	private java.lang.String state;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  orderno
	 */
	@Column(name ="ORDERNO",nullable=false,length=50)
	public java.lang.String getOrderno(){
		return this.orderno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  orderno
	 */
	public void setOrderno(java.lang.String orderno){
		this.orderno = orderno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  ordername
	 */
	@Column(name ="ORDERNAME",nullable=false,length=50)
	public java.lang.String getOrdername(){
		return this.ordername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  ordername
	 */
	public void setOrdername(java.lang.String ordername){
		this.ordername = ordername;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  orderprice
	 */
	@Column(name ="ORDERPRICE",nullable=false,precision=12)
	public java.lang.Float getOrderprice(){
		return this.orderprice;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  orderprice
	 */
	public void setOrderprice(java.lang.Float orderprice){
		this.orderprice = orderprice;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  systemuserId
	 */
	@Column(name ="SYSTEMUSER_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getSystemuserId(){
		return this.systemuserId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  systemuserId
	 */
	public void setSystemuserId(java.lang.Integer systemuserId){
		this.systemuserId = systemuserId;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  tardinesscost
	 */
	@Column(name ="TARDINESSCOST",nullable=false,precision=12)
	public java.lang.Float getTardinesscost(){
		return this.tardinesscost;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  tardinesscost
	 */
	public void setTardinesscost(java.lang.Float tardinesscost){
		this.tardinesscost = tardinesscost;
	}
	/**
	 *方法: 取得java.lang.Float
	 *@return: java.lang.Float  inventorycost
	 */
	@Column(name ="INVENTORYCOST",nullable=false,precision=12)
	public java.lang.Float getInventorycost(){
		return this.inventorycost;
	}

	/**
	 *方法: 设置java.lang.Float
	 *@param: java.lang.Float  inventorycost
	 */
	public void setInventorycost(java.lang.Float inventorycost){
		this.inventorycost = inventorycost;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  materialid
	 */
	@Column(name ="MATERIALID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getMaterialid(){
		return this.materialid;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  materialid
	 */
	public void setMaterialid(java.lang.Integer materialid){
		this.materialid = materialid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  quantity
	 */
	@Column(name ="QUANTITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getQuantity(){
		return this.quantity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  quantity
	 */
	public void setQuantity(java.lang.Integer quantity){
		this.quantity = quantity;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  quantityu
	 */
	@Column(name ="QUANTITYU",nullable=false,precision=10,scale=0)
	public java.lang.Integer getQuantityu(){
		return this.quantityu;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  quantityu
	 */
	public void setQuantityu(java.lang.Integer quantityu){
		this.quantityu = quantityu;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  quantityf
	 */
	@Column(name ="QUANTITYF",nullable=false,precision=10,scale=0)
	public java.lang.Integer getQuantityf(){
		return this.quantityf;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  quantityf
	 */
	public void setQuantityf(java.lang.Integer quantityf){
		this.quantityf = quantityf;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  orderdate
	 */
	@Column(name ="ORDERDATE",nullable=false)
	public java.util.Date getOrderdate(){
		return this.orderdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  orderdate
	 */
	public void setOrderdate(java.util.Date orderdate){
		this.orderdate = orderdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  releasedate
	 */
	@Column(name ="RELEASEDATE",nullable=false)
	public java.util.Date getReleasedate(){
		return this.releasedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  releasedate
	 */
	public void setReleasedate(java.util.Date releasedate){
		this.releasedate = releasedate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  duedate
	 */
	@Column(name ="DUEDATE",nullable=false)
	public java.util.Date getDuedate(){
		return this.duedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  duedate
	 */
	public void setDuedate(java.util.Date duedate){
		this.duedate = duedate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  actualddate
	 */
	@Column(name ="ACTUALDDATE",nullable=false)
	public java.util.Date getActualddate(){
		return this.actualddate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  actualddate
	 */
	public void setActualddate(java.util.Date actualddate){
		this.actualddate = actualddate;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  priority
	 */
	@Column(name ="PRIORITY",nullable=false,precision=10,scale=0)
	public java.lang.Integer getPriority(){
		return this.priority;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  priority
	 */
	public void setPriority(java.lang.Integer priority){
		this.priority = priority;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  color
	 */
	@Column(name ="COLOR",nullable=false,length=50)
	public java.lang.String getColor(){
		return this.color;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  color
	 */
	public void setColor(java.lang.String color){
		this.color = color;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  isplan
	 */
	@Column(name ="ISPLAN",nullable=false,precision=3,scale=0)
	public java.lang.Integer getIsplan(){
		return this.isplan;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  isplan
	 */
	public void setIsplan(java.lang.Integer isplan){
		this.isplan = isplan;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  state
	 */
	@Column(name ="STATE",nullable=false,length=20)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  state
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
