package org.jeecgframework.web.bussi.entity.editcalendar;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 班次时间
 * @author zhangdaihao
 * @date 2015-07-25 17:24:47
 * @version V1.0   
 *
 */
@Entity
@Table(name = "shift", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ShiftEntity implements java.io.Serializable {
	/**id*/
	private java.lang.Integer id;
	/**shiftno*/
	private java.lang.String shiftno;
	/**shiftname*/
	private java.lang.String shiftname;
	/**shiftpatterntypeId*/
	private java.lang.Integer shiftpatterntypeId;
	/**starttime*/
	private java.util.Date starttime;
	/**endtime*/
	private java.util.Date endtime;
	/**datastatus*/
	private java.lang.Integer datastatus;
	/**memo*/
	private java.lang.String memo;
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  id
	 */
	public void setId(java.lang.Integer id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  shiftno
	 */
	@Column(name ="SHIFTNO",nullable=false,length=50)
	public java.lang.String getShiftno(){
		return this.shiftno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  shiftno
	 */
	public void setShiftno(java.lang.String shiftno){
		this.shiftno = shiftno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  shiftname
	 */
	@Column(name ="SHIFTNAME",nullable=false,length=50)
	public java.lang.String getShiftname(){
		return this.shiftname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  shiftname
	 */
	public void setShiftname(java.lang.String shiftname){
		this.shiftname = shiftname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  shiftpatterntypeId
	 */
	@Column(name ="SHIFTPATTERNTYPE_ID",nullable=false,precision=10,scale=0)
	public java.lang.Integer getShiftpatterntypeId(){
		return this.shiftpatterntypeId;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  shiftpatterntypeId
	 */
	public void setShiftpatterntypeId(java.lang.Integer shiftpatterntypeId){
		this.shiftpatterntypeId = shiftpatterntypeId;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  starttime
	 */
	@Column(name ="STARTTIME",nullable=false)
	public java.util.Date getStarttime(){
		return this.starttime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  starttime
	 */
	public void setStarttime(java.util.Date starttime){
		this.starttime = starttime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  endtime
	 */
	@Column(name ="ENDTIME",nullable=false)
	public java.util.Date getEndtime(){
		return this.endtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  endtime
	 */
	public void setEndtime(java.util.Date endtime){
		this.endtime = endtime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  datastatus
	 */
	@Column(name ="DATASTATUS",nullable=false,precision=3,scale=0)
	public java.lang.Integer getDatastatus(){
		return this.datastatus;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  datastatus
	 */
	public void setDatastatus(java.lang.Integer datastatus){
		this.datastatus = datastatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  memo
	 */
	@Column(name ="MEMO",nullable=false,length=200)
	public java.lang.String getMemo(){
		return this.memo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  memo
	 */
	public void setMemo(java.lang.String memo){
		this.memo = memo;
	}
}
